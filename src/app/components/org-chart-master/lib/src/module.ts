import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OrgChartComponent } from './org-chart/org-chart.component';
import { NodeComponent } from './node/node.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [OrgChartComponent, NodeComponent],
	exports: [OrgChartComponent]
})
export class OrgChartModule { }
