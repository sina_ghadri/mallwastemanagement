import { Component, Input } from '@angular/core';
import { INode } from '../node';

@Component({
	selector: 'oc-node',
	templateUrl: './node.component.html',
	styleUrls: ['./node.component.scss']
})
export class NodeComponent {

	@Input() node: INode;
	@Input() hasManager = false;
}
