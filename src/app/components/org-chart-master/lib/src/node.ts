export interface INode {
	name: string;
	designation?: string;
	color?:string;
  icon?:string;
  data?:any;
	childrens: INode[];
}

export class Node implements INode {
	name: string;
	designation?: string;
  color?:string;
  icon?:string;
  data?:any;
	childrens: Node[];
	manager?: Node;
	constructor(orgStructure: string[], manager?: Node) {
		this.manager = manager;
		const [name, ...reports] = orgStructure;
		this.name = name.split('(')[0].trim();
		const desigMatch = name.match(/\(([^)]+)\)/);
		this.designation = desigMatch && desigMatch[1].trim();

		this.childrens = reports.map(r => r.substring(1))
			.reduce((previous, current) => {
				if (!current.startsWith(' ')) {
					previous.push([]);
				}

				previous[previous.length - 1].push(current);

				return previous;
			}, <string[][]>[])
			.map(r => new Node(r, this));
	}
}
