import { Component, Input } from '@angular/core';
import { INode } from '../node';

@Component({
	selector: 'ss-org-chart',
	templateUrl: './org-chart.component.html',
	styleUrls: ['./org-chart.component.css']
})
export class OrgChartComponent {
	@Input() topNode: INode;
	@Input() hasManager = false;
}
