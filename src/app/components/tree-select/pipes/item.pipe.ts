import { Pipe, PipeTransform } from '@angular/core';
import { SelectableItem } from '../models/selectable-item';

@Pipe({ name: 'itemPipe' })
export class ItemPipe implements PipeTransform {
  public transform(value: SelectableItem[], isChild: boolean = false) {
    // ES6 array destructuring
    let result = value.filter((item) => item.matchFilter);
    if(result.length == 0 && isChild) result = value;
    return result;
  }
}
