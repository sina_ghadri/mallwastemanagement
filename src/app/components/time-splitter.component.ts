import { Component, OnInit, Input, ElementRef, HostListener } from "@angular/core";
import { TimeSplit } from "./time-split";

@Component({
  selector: 'app-time-splitter',
  templateUrl: './time-splitter.component.html',
  styleUrls: ['./time-splitter.component.scss']
})
export class TimeSplitterComponent implements OnInit {
  @Input() items: TimeSplit[] = [];
  @Input() defaultModel: TimeSplit;
  @Input() startTime: string = '05:00';
  @Input() endTime: string = '23:00';

  @HostListener('mouseup', ['$event'])
  onmouseup(e: MouseEvent) {
    this.selectedItem = null;
  }
  @HostListener('mousemove', ['$event'])
  onmouseMove(e: MouseEvent) {
    if (this.selectedItem) {

      const point = this.getPoint(e, this.element.nativeElement);
      const width = this.element.nativeElement.offsetWidth;
      const time = ((width - point.x) * (this.parseTime(this.endTime)-this.parseTime(this.startTime)) / width) + this.parseTime(this.startTime);
      if(time > this.parseTime(this.startTime) && time <= this.parseTime(this.endTime))
        this.selectedItem.time = this.toTime(time);
    }
  }

  selectedItem: TimeSplit = null;


  constructor(private element: ElementRef) {
    this.items.push({ text: 'Section 1', time: '10:00', color: 'red' });
    this.items.push({ text: 'Section 2', time: '15:00', color: 'green' });
    this.items.push({ text: 'Section 3', time: '18:00', color: 'blue' });
    //this.items.push({ text: 'Section 4', time: '24:00', color: 'yellow' });
  }
  ngOnInit(): void {

  }
  getPoint(e: MouseEvent, parent: HTMLElement) {
    debugger
    let rect = parent.getBoundingClientRect();
    const x = (e.clientX - rect.left) + parent.scrollLeft;
    const y = (e.clientY - rect.top) + parent.scrollTop;
    return { x, y };
  }
  parseTime(time: string): number {
    let splite = time.split(':');
    if (splite.length == 1) return parseInt(splite[0]);
    if (splite.length == 2) {
      const hour = parseInt(splite[0]);
      const min = parseInt(splite[1]);
      return hour + (min / 60);
    }
  }
  toTime(time: number): string {
    const hour = Math.floor(time);
    const min = Math.floor((time - hour) * 60);
    return (hour > 9 ? '' : '0') + hour + ':' + (min > 9 ? '' : '0') + min;
  }
  getPercent(index: number): number {
    const value = this.parseTime(this.items[index].time) - (index > 0 ? this.parseTime(this.items[index - 1].time) : this.parseTime(this.startTime));
    return (value * 100 / (this.parseTime(this.endTime) - this.parseTime(this.startTime)));
  }
  getItems() {
    return this.items = this.items.sort((x1: TimeSplit, x2: TimeSplit) => {
      const time1 = this.parseTime(x1.time);
      const time2 = this.parseTime(x2.time);
      if (time1 > time2) return 1;
      if (time1 < time2) return -1;
      return 0;
    })
  }

  onmounseDown(e: MouseEvent, obj: TimeSplit) {

    this.selectedItem = obj;
  }
  addSplit(e: MouseEvent, parent:HTMLElement, item: TimeSplit)
  {

    const point = this.getPoint(e, parent);
    const index = this.items.indexOf(item);
    const time = this.parseTime(item.time);
    const pretime = (index > 0 ? this.parseTime(this.items[index - 1].time) : this.parseTime(this.startTime));
    const dist = time - pretime;
    const width = parent.offsetWidth;
    const newNode = new TimeSplit;
    newNode.time = this.toTime(pretime + ((width - point.x) * dist / width));
    newNode.color = this.defaultModel.color;
    newNode.text = this.defaultModel.text;
    this.items.splice(index, 0 , newNode);
  }

  addSplitInFree(e: MouseEvent, item: TimeSplit)
  {

    const point = this.getPoint(e, this.element.nativeElement);
    const index = this.items.indexOf(item);
    const time = this.parseTime(item.time);
    const pretime = (index > 0 ? this.parseTime(this.items[index - 1].time) : this.parseTime(this.startTime));
    const dist = time - pretime;
    const width = this.element.nativeElement.offsetWidth;
    const newNode = new TimeSplit;
    newNode.time = this.toTime(pretime + ((width - point.x) * dist / width));
    newNode.color = this.defaultModel.color;
    newNode.text = this.defaultModel.text;
    this.items.splice(index, 0 , newNode);
  }
}
