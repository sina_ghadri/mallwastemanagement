import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import { ConfigurationService } from '../lib/sigmasoft-ng/misc';
import { Place } from '../models/place';
import { OperationResult } from '../lib/sigmasoft-ts/models';
import { Observable } from 'rxjs';
import { ODataPagination } from '../models/oDataPagination';
import { Cordination } from '../models/cordination';

@Injectable({
  providedIn: 'root'
})
export class PlaceService {

  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<Place[]>> {
    return this.http.get<OperationResult<Place[]>>(this.config.api.Place);
  }
  getWithPagination(oDataPagination: ODataPagination): Observable<OperationResult<Place[]>> {
    let params = '?';
    if (oDataPagination.filter) { params = params + `$filter=${oDataPagination.filter}&`; }
    if (oDataPagination.orderby) { params = params + `$orderby=${oDataPagination.orderby}&`; }
    if (oDataPagination.top) { params = params + `$top=${oDataPagination.top}&`; }
    if (oDataPagination.skip) { params = params + `$skip=${oDataPagination.skip}&`; }
    return this.http.get<OperationResult<Place[]>>(this.config.api.Place + params);
  }
  getChildren(parentId: number): Observable<OperationResult<Place[]>> {
    return this.http.get<OperationResult<Place[]>>(this.config.api.Place + `${parentId}/Children`);
  }
  getCount(): Observable<any> {
    return this.http.get<any>(this.config.api.Place + 'Count');
  }
  getById(id: number): Observable<OperationResult<Place>> {
    return this.http.get<OperationResult<Place>>(this.config.api.Place + id);
  }
  insert(obj: Place): Observable<OperationResult<Place>> {
    return this.http.post<OperationResult<Place>>(this.config.api.Place, obj);
  }
  update(obj: Place): Observable<OperationResult<Place>> {
    return this.http.patch<OperationResult<Place>>(this.config.api.Place, obj);
  }
  deleteById(id: number): Observable<OperationResult<any>> {
    return this.http.delete<OperationResult<any>>(this.config.api.Place + id);
  }
  setCordination(id: number, obj: Cordination): Observable<OperationResult<Place>> {
    obj.X = Math.round(obj.X);
    obj.Y = Math.round(obj.Y);
    obj.Width = Math.round(obj.Width);
    obj.Height = Math.round(obj.Height);
    return this.http.post<OperationResult<Place>>(this.config.api.Place + `${id}/SetCordination`, obj);
  }
  removeCordination(id:number): Observable<OperationResult<Place>> {
    return this.http.delete<OperationResult<Place>>(this.config.api.Place + `${id}/RemoveCordination`);
  }
  uploadImage(id: number, file: File, onprogress: (don: number, total: number, progress: number) => void = null): Observable<OperationResult<any>> {
    return new Observable<OperationResult<any>>(ob => {
      let data = new FormData;
      data.append('image', file);
      var xhr = new XMLHttpRequest();
      xhr.addEventListener('progress', function (e: any) {
        var done = e.position || e.loaded, total = e.totalSize || e.total;
        if (onprogress) onprogress(done, total, Math.floor(done / total * 1000) / 10)
      }, false);
      if (xhr.upload) {
        xhr.upload.onprogress = function (e: any) {
          var done = e.position || e.loaded, total = e.totalSize || e.total;
          if (onprogress) onprogress(done, total, Math.floor(done / total * 1000) / 10)
        };
      }
      xhr.onreadystatechange = function (e) {
        if (this.readyState == 4) {
          if (this.status == 200) ob.next(JSON.parse(this.responseText))
          else ob.error(this.responseText);
          ob.complete();
        }
      };
      xhr.open('POST', this.config.api.Place + id + '/Upload', true);
      xhr.send(data);
    });
  }
}
