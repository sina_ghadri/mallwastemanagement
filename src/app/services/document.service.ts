import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {Document} from '../models/document';
import {ContainerType} from '../models/ContainerType';
import {OperationResult} from '../lib/sigmasoft-ts/models';




@Injectable({
  providedIn: 'root'
})
export class DocumentService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<Document[]>> {
    return this.http.get<OperationResult<Document[]>>(this.config.api.Document);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<Document[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Document[]>>(this.config.api.Document+params);
  }

  getById(id: number): Observable<OperationResult<Document>> {
    return this.http.get<OperationResult<Document>>(this.config.api.Document + id);
  }
  insert(obj: Document): Observable<OperationResult<Document>> {
    return this.http.post<OperationResult<Document>>(this.config.api.Document , obj);
  }
  update(obj: Document): Observable<OperationResult<Document>>  {
    return this.http.patch<OperationResult<Document>>(this.config.api.Document , obj);
  }
  deleteById(id: number): Observable <OperationResult<Document>>  {
    return this.http.delete<OperationResult<Document>>(this.config.api.Document + id);
  }

  getByUserId(oDataPagination:ODataPagination,id):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<Document[]>(this.config.api.User+`${id}/`+this.config.api.Document+params);
  }

}
