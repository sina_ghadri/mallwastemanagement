import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {Request} from '../models/request';




@Injectable({
  providedIn: 'root'
})
export class RequestService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<Request[]> {
    return this.http.get<Request[]>(this.config.api.Request);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<Request[]>(this.config.api.Request+params);
  }

  getById(id: number): Observable<Request> {
    return this.http.get<Request>(this.config.api.Request + id);
  }
  insert(obj: Request): Observable<Request>  {
    return this.http.post<any>(this.config.api.Request , obj);
  }
  update(obj: Request): Observable<Request>  {
    return this.http.patch<any>(this.config.api.Request , obj);
  }
  deleteById(id: number): Observable <any>  {
    return this.http.delete<any>(this.config.api.Request + id);
  }

  getByStationId(oDataPagination:ODataPagination,id):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<Request[]>(this.config.api.Station+`${id}/`+this.config.api.Request+params);
  }

}
