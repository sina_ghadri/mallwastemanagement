import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {DayType} from '../models/dayType';
import {OperationResult} from '../lib/sigmasoft-ts/models';




@Injectable({
  providedIn: 'root'
})
export class DayTypeService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<DayType[]>> {
    return this.http.get<OperationResult<DayType[]>>(this.config.api.DayType);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<DayType[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<DayType[]>>(this.config.api.DayType+params);
  }

  getById(id: number): Observable<OperationResult<DayType>> {
    return this.http.get<OperationResult<DayType>>(this.config.api.DayType + id);
  }
  insert(obj: DayType): Observable<OperationResult<DayType>>  {
    return this.http.post<OperationResult<DayType>>(this.config.api.DayType , obj);
  }
  update(obj: DayType): Observable<OperationResult<DayType>>  {
    return this.http.patch<OperationResult<DayType>>(this.config.api.DayType , obj);
  }
  deleteById(id: number): Observable <OperationResult<DayType>>  {
    return this.http.delete<OperationResult<DayType>>(this.config.api.DayType + id);
  }



}
