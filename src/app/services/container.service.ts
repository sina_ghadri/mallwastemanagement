import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';
import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {Container} from '../models/container';
import {Station} from '../models/station';
import {OperationResult} from '../lib/sigmasoft-ts/models';




@Injectable({
  providedIn: 'root'
})
export class ContainerService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<Container[]>> {
    return this.http.get<OperationResult<Container[]>>(this.config.api.Container);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<Container[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Container[]>>(this.config.api.Container+params);
  }

  getById(id: number): Observable<OperationResult<Container>> {
    return this.http.get<OperationResult<Container>>(this.config.api.Container + id);
  }
  insert(obj: Container): Observable<OperationResult<Container>>  {
    return this.http.post<OperationResult<Container>>(this.config.api.Container , obj);
  }
  update(obj: Container): Observable<OperationResult<Container>>  {
    return this.http.patch<OperationResult<Container>>(this.config.api.Container , obj);
  }
  deleteById(id: number): Observable <OperationResult<Container>>  {
    return this.http.delete<OperationResult<Container>>(this.config.api.Container + id);
  }

  getByContainerTypeId(oDataPagination:ODataPagination,id):Observable<OperationResult<Container[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Container[]>>(this.config.api.ContainerType+`${id}/`+this.config.api.Container+params);
  }

}
