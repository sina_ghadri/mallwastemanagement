import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {OperationResult} from '../lib/sigmasoft-ts/models';
import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {UseType} from '../models/useType';
import {DocumentItem} from '../models/documentItem';
import {Container} from '../models/container';
import { Index } from '../models';
import { UseTypeIndex } from '../models/usetype_index';

@Injectable({
  providedIn: 'root'
})
export class UseTypeService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<UseType[]>> {
    return this.http.get<OperationResult<UseType[]>>(this.config.api.UseType);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<UseType[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<UseType[]>>(this.config.api.UseType+params);
  }
  getById(id: number): Observable<OperationResult<UseType>> {
    return this.http.get<OperationResult<UseType>>(this.config.api.UseType + id);
  }
  insert(obj: UseType): Observable<OperationResult<UseType>> {
    return this.http.post<OperationResult<UseType>>(this.config.api.UseType , obj);
  }
  update(obj: UseType): Observable<OperationResult<UseType>>  {
    return this.http.patch<OperationResult<UseType>>(this.config.api.UseType , obj);
  }
  deleteById(id: number): Observable <OperationResult<any>>  {
    return this.http.delete<OperationResult<any>>(this.config.api.UseType + id);
  }

  getByIndexId(id):Observable<OperationResult<UseType[]>> {
    return this.http.get<OperationResult<UseType[]>>(this.config.api.Index+`${id}/`+'UseTypes');
  }

  getIndexes(id: number): Observable<OperationResult<UseTypeIndex[]>> {
    return this.http.get<OperationResult<UseTypeIndex[]>>(this.config.api.UseType + id + '/Indexes');
  }
  addIndexe(id: number, indexId: number): Observable<UseTypeIndex[]> {
    return this.http.post<UseTypeIndex[]>(this.config.api.UseType + id + '/Indexes', { UseTypeId: id, IndexId: indexId });
  }
  removeIndexes(id: number, indexId: number): Observable<UseTypeIndex[]> {
    return this.http.delete<UseTypeIndex[]>(this.config.api.UseType + id + '/Indexes/' + indexId);
  }
}
