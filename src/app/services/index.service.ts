import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {OperationResult} from '../lib/sigmasoft-ts/models';
import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {Index} from '../models';
import {Station} from '../models/station';
import {Inspection} from '../models/inspection';

@Injectable({
  providedIn: 'root'
})
export class IndexService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<Index[]>> {
    return this.http.get<OperationResult<Index[]>>(this.config.api.Index);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<Index[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Index[]>>(this.config.api.Index+params);
  }


  getById(id: number): Observable<OperationResult<Index>> {
    return this.http.get<OperationResult<Index>>(this.config.api.Index + id);
  }
  insert(obj: Index): Observable<OperationResult<Index>> {
    return this.http.post<OperationResult<Index>>(this.config.api.Index , obj);
  }
  update(obj: Index): Observable<OperationResult<Index>>  {
    return this.http.patch<OperationResult<Index>>(this.config.api.Index , obj);
  }
  deleteById(id: number): Observable <OperationResult<any>>  {
    return this.http.delete<OperationResult<any>>(this.config.api.Index + id);
  }

  getByUseTypeId(oDataPagination:ODataPagination,id):Observable<OperationResult<Index[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Index[]>>(this.config.api.UseType+`${id}/Indexes`+params);
  }
  getByInspectionId(oDataPagination:ODataPagination,id):Observable<OperationResult<Index[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Index[]>>(this.config.api.Inspection+`${id}/Indexes`+params);
  }

}
