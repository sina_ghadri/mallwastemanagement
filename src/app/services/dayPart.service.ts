import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {OperationResult} from '../lib/sigmasoft-ts/models';
import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {DayPart} from '../models/DayPart';
import {DocumentItem} from '../models/documentItem';



@Injectable({
  providedIn: 'root'
})
export class DayPartService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<DayPart[]> {
    return this.http.get<DayPart[]>(this.config.api.DayPart);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<DayPart[]>(this.config.api.DayPart+params);
  }

  getById(id: number): Observable<DayPart> {
    return this.http.get<DayPart>(this.config.api.DayPart + id);
  }
  insert(obj: DayPart): Observable<DayPart>  {
    return this.http.post<any>(this.config.api.DayPart , obj);
  }
  update(obj: DayPart): Observable<DayPart>  {
    return this.http.patch<any>(this.config.api.DayPart , obj);
  }
  deleteById(id: number): Observable <any>  {
    return this.http.delete<any>(this.config.api.DayPart + id);
  }


  getBySlaId(oDataPagination:ODataPagination,id):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<DayPart[]>(this.config.api.Sla+`${id}/`+this.config.api.DayPart+params);
  }
}
