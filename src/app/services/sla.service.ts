import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {Sla} from '../models/sla';
import {OperationResult} from '../lib/sigmasoft-ts/models';




@Injectable({
  providedIn: 'root'
})
export class SlaService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<Sla[]>> {
    return this.http.get<OperationResult<Sla[]>>(this.config.api.Sla);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<Sla[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Sla[]>>(this.config.api.Sla+params);
  }

  getById(id: number): Observable<OperationResult<Sla>> {
    return this.http.get<OperationResult<Sla>>(this.config.api.Sla + id);
  }
  insert(obj: Sla): Observable<OperationResult<Sla>>  {
    return this.http.post<OperationResult<Sla>>(this.config.api.Sla , obj);
  }
  update(obj: Sla): Observable<OperationResult<Sla>>  {
    return this.http.patch<OperationResult<Sla>>(this.config.api.Sla , obj);
  }
  deleteById(id: number): Observable <OperationResult<Sla>>  {
    return this.http.delete<OperationResult<Sla>>(this.config.api.Sla + id);
  }



}
