import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {User} from '../models/user';
import {OperationResult} from '../lib/sigmasoft-ts/models';




@Injectable({
  providedIn: 'root'
})
export class UserService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<User[]>> {
    return this.http.get<OperationResult<User[]>>(this.config.api.User);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<User[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<User[]>>(this.config.api.User+params);
  }

  getById(id: number): Observable<OperationResult<User>> {
    return this.http.get<OperationResult<User>>(this.config.api.User + id);
  }
  insert(obj: User): Observable<OperationResult<User>>  {
    return this.http.post<OperationResult<User>>(this.config.api.User , obj);
  }
  update(obj: User): Observable<OperationResult<User>>  {
    return this.http.patch<OperationResult<User>>(this.config.api.User , obj);
  }
  deleteById(id: number): Observable <OperationResult<User>>  {
    return this.http.delete<OperationResult<User>>(this.config.api.User + id);
  }



}
