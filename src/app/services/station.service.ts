import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {OperationResult} from '../lib/sigmasoft-ts/models';
import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';
import {Station} from '../models/station';
import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {Document} from '../models/document';


@Injectable({
  providedIn: 'root'
})
export class StationService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<Station[]>> {
    return this.http.get<OperationResult<Station[]>>(this.config.api.Station);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<Station[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Station[]>>(this.config.api.Station+params);
  }
  getById(id: number): Observable<OperationResult<Station>> {

    return this.http.get<OperationResult<Station>>(this.config.api.Station + id);
  }


  insert(obj: Station): Observable<OperationResult<Station>> {
    return this.http.post<OperationResult<Station>>(this.config.api.Station , obj);
  }
  update(obj: Station): Observable<OperationResult<Station>>  {
    return this.http.patch<OperationResult<Station>>(this.config.api.Station , obj);
  }
  setCordination(id: number, obj: { x: number, y: number }): Observable<OperationResult<Station>>  {
    return this.http.put<OperationResult<Station>>(this.config.api.Station + id + '/SetCordination' , obj);
  }
  removeCordination(id: number): Observable<OperationResult<Station>>  {
    return this.http.delete<OperationResult<Station>>(this.config.api.Station + id + '/RemoveCordination' );
  }
  deleteById(id: number): Observable <OperationResult<any>>  {
    return this.http.delete<OperationResult<any>>(this.config.api.Station + id);
  }
  getByUseTypeId(oDataPagination:ODataPagination,id):Observable<OperationResult<Station[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Station[]>>(this.config.api.UseType+`${id}/Stations`+params);
  }
  getByContainerTypeId(oDataPagination:ODataPagination,id):Observable<OperationResult<Station[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Station[]>>(this.config.api.ContainerType+`${id}/`+this.config.api.Station+params);
  }
  getByPlaceId(oDataPagination:ODataPagination,id):Observable<OperationResult<Station[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Station[]>>(this.config.api.Place+`${id}/Stations`+params);
  }
  getByUserId(oDataPagination:ODataPagination,id):Observable<OperationResult<Station[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Station[]>>(this.config.api.User+`${id}/`+this.config.api.Station+params);
  }
}
