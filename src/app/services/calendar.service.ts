import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';


import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {Calendar} from '../models/calendar';
import {OperationResult} from '../lib/sigmasoft-ts/models';





@Injectable({
  providedIn: 'root'
})
export class CalendarService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<Calendar[]>> {
    return this.http.get<OperationResult<Calendar[]>>(this.config.api.Calendar);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<Calendar[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Calendar[]>>(this.config.api.Calendar+params);
  }

  getById(id: number): Observable<OperationResult<Calendar>> {
    return this.http.get<OperationResult<Calendar>>(this.config.api.Calendar + id);
  }
  insert(obj: Calendar): Observable<OperationResult<Calendar>>  {
    return this.http.post<OperationResult<Calendar>>(this.config.api.Calendar , obj);
  }
  update(obj: Calendar): Observable<OperationResult<Calendar>>  {
    return this.http.patch<OperationResult<Calendar>>(this.config.api.Calendar , obj);
  }
  deleteById(id: number): Observable <OperationResult<Calendar>>  {
    return this.http.delete<OperationResult<Calendar>>(this.config.api.Calendar + id);
  }


  getByDayTypeId(oDataPagination:ODataPagination,id):Observable<OperationResult<Calendar[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Calendar[]>>(this.config.api.DayType+`${id}/`+this.config.api.Calendar+params);
  }
}
