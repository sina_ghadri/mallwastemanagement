import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {WasteType} from '../models/wasteType';
import {OperationResult} from '../lib/sigmasoft-ts/models';




@Injectable({
  providedIn: 'root'
})
export class WasteTypeService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<WasteType[]>> {
    return this.http.get<OperationResult<WasteType[]>>(this.config.api.WasteType);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<WasteType[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<WasteType[]>>(this.config.api.WasteType+params);
  }

  getById(id: number): Observable<OperationResult<WasteType>> {
    return this.http.get<OperationResult<WasteType>>(this.config.api.WasteType + id);
  }
  insert(obj: WasteType): Observable<OperationResult<WasteType>>  {
    return this.http.post<any>(this.config.api.WasteType , obj);
  }
  update(obj: WasteType): Observable<OperationResult<WasteType>>  {
    return this.http.patch<any>(this.config.api.WasteType , obj);
  }
  deleteById(id: number): Observable <any>  {
    return this.http.delete<any>(this.config.api.WasteType + id);
  }



}
