import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {OperationResult} from '../lib/sigmasoft-ts/models';
import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {Inspection} from '../models/inspection';
import {Index} from '../models';
import {Document} from '../models/document';


@Injectable({
  providedIn: 'root'
})
export class InspectionService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<Inspection[]>> {
    return this.http.get<OperationResult<Inspection[]>>(this.config.api.Inspection);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<Inspection[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Inspection[]>>(this.config.api.Inspection+params);
  }

  getById(id: number): Observable<OperationResult<Inspection>> {
    return this.http.get<OperationResult<Inspection>>(this.config.api.Inspection + id);
  }
  insert(obj: Inspection): Observable<OperationResult<Inspection>>  {
    return this.http.post<any>(this.config.api.Inspection , {StationId:obj.StationId,UserId:1,Time:obj.Time,Indexes:obj.Indexes});
  }
  update(obj: Inspection): Observable<OperationResult<Inspection>>  {
    return this.http.patch<any>(this.config.api.Inspection , {Id:obj.Id,StationId:obj.StationId,UserId:1,Time:obj.Time,Indexes:obj.Indexes});
  }
  deleteById(id: number): Observable <OperationResult<Inspection>>  {
    return this.http.delete<OperationResult<Inspection>>(this.config.api.Inspection + id);
  }

  getByCalendarId(oDataPagination:ODataPagination,id):Observable<OperationResult<Inspection[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Inspection[]>>(this.config.api.Calendar+`${id}/`+this.config.api.Inspection+params);
  }
  getByIndexId(oDataPagination:ODataPagination,id):Observable<OperationResult<Inspection[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Inspection[]>>(this.config.api.Index+`${id}/`+this.config.api.Inspection+params);
  }
  getByStationId(oDataPagination:ODataPagination,id):Observable<OperationResult<Inspection[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Inspection[]>>(this.config.api.Station+`${id}/`+this.config.api.Inspection+params);
  }

  getByUserId(oDataPagination:ODataPagination,id):Observable<OperationResult<Inspection[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<Inspection[]>>(this.config.api.User+`${id}/`+this.config.api.Inspection+params);
  }
}
