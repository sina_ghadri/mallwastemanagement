import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {DocumentItem} from '../models/documentItem';
import {Calendar} from '../models/calendar';
import {WasteType} from '../models/wasteType';




@Injectable({
  providedIn: 'root'
})
export class DocumentItemService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<DocumentItem[]> {
    return this.http.get<DocumentItem[]>(this.config.api.DocumentItem);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<DocumentItem[]>(this.config.api.DocumentItem+params);
  }

  getById(id: number): Observable<DocumentItem> {
    return this.http.get<DocumentItem>(this.config.api.DocumentItem + id);
  }
  insert(obj: DocumentItem): Observable<DocumentItem>  {
    return this.http.post<any>(this.config.api.DocumentItem , obj);
  }
  update(obj: DocumentItem): Observable<DocumentItem>  {
    return this.http.patch<any>(this.config.api.DocumentItem , obj);
  }
  deleteById(id: number): Observable <any>  {
    return this.http.delete<any>(this.config.api.DocumentItem + id);
  }

  getByDocumentId(oDataPagination:ODataPagination,id):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<DocumentItem[]>(this.config.api.Document+`${id}/Items`+params);
  }
  getByRequestId(oDataPagination:ODataPagination,id):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<DocumentItem[]>(this.config.api.Request+`${id}/`+this.config.api.DocumentItem+params);
  }
  getByStationId(oDataPagination:ODataPagination,id):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<DocumentItem[]>(this.config.api.Station+`${id}/`+this.config.api.DocumentItem+params);
  }
  getByWasteTyped(oDataPagination:ODataPagination,id):Observable<any[]> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<DocumentItem[]>(this.config.api.WasteType+`${id}/`+this.config.api.DocumentItem+params);
  }
}
