import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

import {Observable} from 'rxjs';
import {ODataPagination} from '../models/oDataPagination';

import {ConfigurationService} from '../lib/sigmasoft-ng/misc';
import {ContainerType} from '../models/ContainerType';
import {DayPart} from '../models/dayPart';
import {WasteType} from '../models/wasteType';
import {OperationResult} from '../lib/sigmasoft-ts/models';



@Injectable({
  providedIn: 'root'
})
export class ContainerTypeService {



  private get config() { return this.configService.configuration; }
  constructor(private http: HttpClient, private configService: ConfigurationService) { }

  getAll(): Observable<OperationResult<ContainerType[]>> {
    return this.http.get<OperationResult<ContainerType[]>>(this.config.api.ContainerType);
  }
  getWithPagination(oDataPagination:ODataPagination):Observable<OperationResult<ContainerType[]>> {
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<ContainerType[]>>(this.config.api.ContainerType+params);
  }

  getById(id: number): Observable<OperationResult<ContainerType>> {
    return this.http.get<OperationResult<ContainerType>>(this.config.api.ContainerType + id);
  }
  insert(obj: ContainerType): Observable<OperationResult<ContainerType>>  {
    return this.http.post<OperationResult<ContainerType>>(this.config.api.ContainerType , obj);
  }
  update(obj: ContainerType): Observable<any>  {
    return this.http.patch<any>(this.config.api.ContainerType , obj);
  }
  deleteById(id: number): Observable <OperationResult<ContainerType>>  {
    return this.http.delete<OperationResult<ContainerType>>(this.config.api.ContainerType + id);
  }

  getByWasteTypeId(oDataPagination:ODataPagination,id):Observable<OperationResult<ContainerType[]>>{
    let params='?';
    if(oDataPagination.filter){ params=params+`$filter=${oDataPagination.filter}&`;}
    if(oDataPagination.orderby){ params=params+`$orderby=${oDataPagination.orderby}&`;}
    if(oDataPagination.top){ params=params+`$top=${oDataPagination.top}&`;}
    if(oDataPagination.skip){ params=params+`$skip=${oDataPagination.skip}&`;}
    return this.http.get<OperationResult<ContainerType[]>>(this.config.api.WasteType+`${id}/ContainTypes`+params);
  }

}
