import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';




const routes: Route[] = [
  { path: '', loadChildren: './pages/auth/auth.module#AuthModule' },
  { path: 'Auth', loadChildren: './pages/auth/auth.module#AuthModule' },
  { path: 'Pages', loadChildren: './pages/pages.module#PagesModule' },
  { path: '**', redirectTo:''},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
