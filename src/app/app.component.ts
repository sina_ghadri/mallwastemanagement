import { Component, OnInit } from '@angular/core';



import {SwUpdate} from '@angular/service-worker';
import {ConfigurationService, HttpConfigService} from './lib/sigmasoft-ng/misc';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  exportAs: 'app-root',
})
export class AppComponent implements OnInit {
  update: boolean = false;


  public constructor(private service: ConfigurationService, private httpConfig: HttpConfigService ,updates: SwUpdate) {

    updates.available.subscribe(event => {
      console.log("update");
        this.update=true;
      updates.activateUpdate().then(() => document.location.reload());
    })
  }
  ngOnInit(): void {






    let style = (<any>document.createElement('LINK'));
    style.href = 'assets/css/' +  'rtl' + '.css';
    style.rel = 'stylesheet';
    document.head.appendChild(style);

    // if(data.rtl)
    // {
    //   let RTLStyle = (<any>document.createElement('LINK'));
    //   RTLStyle.href = 'assets/css/bootstrap.rtl.min.css';
    //   RTLStyle.rel = 'stylesheet';
    //   document.head.appendChild(RTLStyle);
    // }

    document.body.classList.add('rtl' );
    document.body.classList.add('lang-' + 'fa');






  }
}
