import {Pipe, PipeTransform} from '@angular/core';


@Pipe({name: 'truefalse'})
export class TruefalsePipe implements PipeTransform {

  transform(input:any):string {

    if(input==true)
    {
      return "فعال"
    }
    else{
      return "غیر فعال"
    }

  }
}
