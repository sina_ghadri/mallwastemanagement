

export class ODataPagination {
  public filter: string;
  public orderby: string;
  public skip: number;
  public top:number;
}

export class FilterSpecification {
  filterOperation: any;
  filterValue: any;
  propertyName: string;
}




