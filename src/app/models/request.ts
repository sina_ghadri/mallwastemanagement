import {Station} from './station';

export  class Request {
  Id: number;
  Date: string;
  Num: number;
  StationId: number;
  Type: number;
  Station: Station;
}

