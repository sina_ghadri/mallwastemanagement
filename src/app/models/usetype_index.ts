import { UseType } from "./useType";
import { Index } from ".";

export class UseTypeIndex {
    Id: number;
    UseTypeId: number;
    UseType: UseType;
    IndexId: number;
    Index: Index;
}