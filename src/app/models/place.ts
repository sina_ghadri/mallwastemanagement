import {Parent} from './inspection';
import { Cordination } from './cordination';

export class Place {
  Id: number;
  Name: string;
  Cordination: Cordination;
  IsActive: boolean=true;
  ParentId: number;
  Parent: Place;
  HasImage:any;
}
