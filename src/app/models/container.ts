import {ContainerType} from './ContainerType';
import {DateTime, JDate} from '../lib/sigmasoft-ts/datetime';


export class Container {
  Id: number;
  Code: number;


  StartDate: string=DateTime.now.format('yyyy/MM/dd');
  get StartJDate(): string { return this.StartDate ? JDate.toJDate(JDate.parseDate(this.StartDate)).format('yyyy/MM/dd') : null; }
  set StartJDate(value: string) { this.StartDate = JDate.format(JDate.parseJDate(value).toDate()); }
  EndDate: string=DateTime.now.format('yyyy/MM/dd');
  get EndJDate(): string { return this.EndDate ? JDate.toJDate(JDate.parseDate(this.EndDate)).format('yyyy/MM/dd') : null; }
  set EndJDate(value: string) { this.EndDate = JDate.format(JDate.parseJDate(value).toDate()); }
  ContainerTypeId: number;
  ContainerType: ContainerType;
}
