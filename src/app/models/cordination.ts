export class Cordination {
    public X: number = 0;
    public Y: number = 0;

    public Width: number = 0;
    public Height: number = 0;

    reset() {
        this.X = 0;
        this.Y = 0;
        this.Width = 0;
        this.Height = 0;
    }

    scale(value) {
        this.X *= value;
        this.Y *= value;
        this.Width *= value;
        this.Height *= value;
    }
}