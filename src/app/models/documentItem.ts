
import {Station} from './station';
import {Document} from './document';
import {Request} from './request';
import {WasteType} from './wasteType';


export class DocumentItem {
  Id: number;
  DocumentId: number;
  Volume: number;
  Weight: number;
  WasteTypeId: number;
  StationId: number;
  RequestId: number;
  Document: Document;
  WasteType: WasteType;
  WasteTypeObj: any;
  Station: Station;
  Request: Request;
}
