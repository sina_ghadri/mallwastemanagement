import {DayType} from './dayType';
import {Sla} from './sla';

export class DayPart {
  Id: number;
  StartTime: string;
  EndTime: string;
  SlaId: number;
  DayTypeId: number;
  Sla: Sla;
  DayType: DayType;
}
