import {User} from './user';
import {UseType} from './useType';
import {Place} from './place';

export  class Station {
  Id: number;
  Name: string;
  UseTypeId: number;
  PlaceId: number;
  Cordination: any;
  Tag: string;
  Space: string;
  ConnectiveUserId: number;
  IsActive: boolean=true;
  UseType: UseType;
  Place: Place;
  ConnectiveUser: User;
}
