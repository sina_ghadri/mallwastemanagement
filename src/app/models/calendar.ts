import {DayType} from './dayType';
import {DateTime, JDate} from '../lib/sigmasoft-ts/datetime';

export class Calendar {
  Id: number;

  Date: string=DateTime.now.format('yyyy-MM-dd');
  get JDate(): string { return this.Date ? JDate.toJDate(JDate.parseDate(this.Date)).format('yyyy-MM-dd') : null; }
  set JDate(value: string) { this.Date = JDate.format(JDate.parseJDate(value).toDate()); }
  DayTypeId: number;
  DayType: DayType;

}
