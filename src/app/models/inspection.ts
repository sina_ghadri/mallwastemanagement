
import {Station} from './station';
import {User} from './user';
import {DateTime, JDate} from '../lib/sigmasoft-ts/datetime';
import {Index} from './index';
import {Calendar} from './calendar';

export class Inspection {
  Id: number;

  Time: string=DateTime.now.format('yyyy-MM-dd hh:mm:ss');
  get JTime(): string { return this.Time ? JDate.toJDate(JDate.parseDate(this.Time)).format('yyyy-MM-dd hh:mm:ss') : null; }
  set JTime(value: string) { this.Time = JDate.format(JDate.parseJDate(value).toDate()); }

  Calendar: Calendar;
  CalendarId: number;

  User: User;
  UserId: number=1;

  Station: Station;
  StationId: number;

  Indexes: Index[]=[];


}





export class  Parent {
}


