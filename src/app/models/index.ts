import {Inspection, Parent} from './inspection';

export  class Index {
  Id?: number;
  Name?: string;
  ParentId?: number;
  Parent?: Index;


  Children?:any[]

  InspectionId?: number;
  IndexId: number;
  Grade: number;
  CreationTime?: string;
  Inspection?: Inspection;
  Index?: Index;
}

