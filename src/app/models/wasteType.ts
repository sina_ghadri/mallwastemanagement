export class  WasteType {
  Id: number;
  Name: string;
  ParentId: number;
  Parent: WasteType;
}
