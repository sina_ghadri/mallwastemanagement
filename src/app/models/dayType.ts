export class DayType {
  Id?: number;
  Name: string;
  Color: string;
  StartTime?: string;
  EndTime?: string;
  IsActive?: boolean=true;

  Date?:any;
}
