import {WasteType} from './wasteType';


export class ContainerType {
  Id: number;
  Name: string;
  Color: string;
  Capacity: number;
  WasteTypeId: number;
  LongLife: number;
  WasteType: WasteType;
}


