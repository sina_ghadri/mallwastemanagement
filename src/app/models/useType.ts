import {Parent} from './inspection';
import { UseTypeIndex } from './usetype_index';

export  class UseType {
  Id: number;
  Name: string;
  ParentId: number;
  Parent: Parent;

  indexes: UseTypeIndex[];
}

