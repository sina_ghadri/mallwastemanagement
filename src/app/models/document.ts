import {User} from './user';
import {DocumentItem} from './documentItem';
import {DateTime, JDate} from '../lib/sigmasoft-ts/datetime';


export class Document {
  Id: number;
  Num: number;
  Time: string;
  Type: number;
  UserId: number;
  Volume: number;
  Weight: number;
  User: User = new User;
  Items: DocumentItem[]=[];

  get JTime(): string { return this.Time ? JDate.toJDate(JDate.parseDate(this.Time)).format('yyyy-MM-dd hh:mm:ss') : null; }
  set JTime(value: string) { this.Time = JDate.format(JDate.parseJDate(value).toDate()); }

}
