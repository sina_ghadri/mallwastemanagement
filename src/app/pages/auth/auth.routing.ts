import { NgModule } from '@angular/core';
import { RouterModule, Route} from '@angular/router';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';


const routes: Route[] = [
  { path: '', component:LoginComponent},
  { path: 'Login', component:LoginComponent},
  { path: 'Register', component:RegisterComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRouting { }
