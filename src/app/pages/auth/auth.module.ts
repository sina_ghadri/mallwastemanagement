import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AuthRouting } from './auth.routing';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import {SigmaSoftModule} from '../../lib/sigmasoft-ng';

@NgModule({
  imports: [
    CommonModule,
    AuthRouting,
    SigmaSoftModule,
  ],
  declarations: [LoginComponent, RegisterComponent]
})
export class AuthModule { }
