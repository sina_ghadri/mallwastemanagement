import { Component, OnInit, Input } from "@angular/core";
import { Router } from "@angular/router";

import {PageLogin} from '../../../lib/sigmasoft-ng/panel/admin/models';
import {OperationResult} from '../../../lib/sigmasoft-ts/models';


@Component({
  selector: "admin-login",
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  exportAs: "admin-login",
  providers: []
})
export class LoginComponent implements OnInit {
  @Input() login: PageLogin;

  public constructor(
    private router: Router,

    // private authService: AuthService,
    // private managerService: AdminPanelMangerService
  ) {
    // if(managerService.login) this.login = managerService.login;
  }

  ngOnInit(): void {
    if(!this.login)
    {
      this.login = new PageLogin();
      this.login.background = "assets/images/admin/login/Recycling.png";
      this.login.logo = "assets/images/admin/login/mall.jpg";
      this.login.description = "سامانه پسماند  مال";
    }
  }
  onsuccess(op) {

    // this.authService.setToken(op.data.token);
    // localStorage.setItem("data",JSON.stringify(op));
    // localStorage.setItem("token",op.access_token);
    this.router.navigate(['Pages/Dashboard']);

  }
}
