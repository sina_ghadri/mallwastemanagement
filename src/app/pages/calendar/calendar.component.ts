import { Component, OnInit } from '@angular/core';
import {Calendar} from '../../models/calendar';
import {CalendarService} from '../../services/calendar.service';
import {DayTypeService} from '../../services/dayType.service';
import {DayType} from '../../models/dayType';
import {DateTime, JDate} from '../../lib/sigmasoft-ts/datetime';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  obj:Calendar=new Calendar();
  touched:boolean=false;
  progressForm:boolean=false;
  dayTypeItems:DayType[]=[];
  items:DayType[]=[];
  constructor(private service:CalendarService,private dayTypeService:DayTypeService) { }

  ngOnInit() {
    this.load();
    this.loadDayType();


  }
  setDayType(Date){
    debugger
    if(this.items.find(item=> JDate.toJDate(JDate.parseDate(item.Date)).format('yyyy-MM-dd')==Date))
    {
        let Color=this.items.find(item=>JDate.toJDate(JDate.parseDate(item.Date)).format('yyyy-MM-dd')==Date).Color
         this.obj.DayType=this.dayTypeItems.find(i=>i.Color==Color)
        this.obj.DayTypeId= this.dayTypeItems.find(i=>i.Color==Color).Id
    }
    else {
      this.obj.DayType=null;
      this.obj.DayTypeId= null
    }
  }
  save(form){
    this.touched = true;


    if (form.valid) {
      this.progressForm = true;
      debugger

      this.obj.DayType= this.dayTypeItems.find(i=>i.Id==this.obj.DayTypeId);

      if(this.items.find(i=> JDate.toJDate(JDate.parseDate(i.Date)).format('yyyy-MM-dd')==this.obj.JDate))
      {
        this.obj.Id=this.items.find(i=> JDate.toJDate(JDate.parseDate(i.Date)).format('yyyy-MM-dd')==this.obj.JDate).Id;
        this.service.update(this.obj).subscribe(res=>{
          this.items[this.items.findIndex(i=> JDate.toJDate(JDate.parseDate(i.Date)).format('yyyy-MM-dd')==this.obj.JDate)]={Date:this.obj.Date,Color:this.obj.DayType.Color,Name:this.obj.DayType.Name}

          this.progressForm = false;
          this.touched = false;
        },error1 => {
          this.progressForm = false;
          this.touched = false;
        })
      }
      else {
        this.service.insert(this.obj).subscribe(res=>{
          this.items.push({Date:this.obj.Date,Color:this.obj.DayType.Color,Name:this.obj.DayType.Name});
          this.progressForm = false;
          this.touched = false;
        },error1 => {
          this.progressForm = false;
          this.touched = false;
        })
      }

    }

  }
  load(){
    this.service.getAll().subscribe(res=>{
      this.items=[];
      for(const item of res.Data)
      {
        if(item.DayType)
        {
          this.items.push({Id:item.Id,Date:item.Date,Color:item.DayType.Color,Name:item.DayType.Name});
          debugger
        }

      }
      this.dayTypeService.getAll().subscribe(res=>{
        this.dayTypeItems=res.Data
        this.setDayType(DateTime.toJDate(new Date()).format('yyyy-MM-dd'))
      })

    })
  }
  loadDayType(){
    this.dayTypeService.getAll().subscribe(res=>{
      this.dayTypeItems=res.Data

    })
  }
}
