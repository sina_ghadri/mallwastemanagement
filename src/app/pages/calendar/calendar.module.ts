import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarComponent } from './calendar.component';
import {SigmaSoftModule} from '../../lib/sigmasoft-ng';
import {CalendarService} from '../../services/calendar.service';
import {DayTypeService} from '../../services/dayType.service';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    FormsModule,
    CalendarRoutingModule
  ],
  providers:[CalendarService,DayTypeService],
  declarations: [CalendarComponent]
})
export class CalendarModule { }
