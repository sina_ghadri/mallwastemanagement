import {Component, Input, OnInit} from '@angular/core';
import {User} from './../../lib/sigmasoft-ng/panel/admin/models/user';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  @Input() user: User = new User();
  change: any = { old: "", new: "", repeat: "" };

  progressContact: boolean;
  progressPassword: boolean;
  progressSocial:boolean;

  messageContact: string;
  messagePassword: string;
  messageSocial: string;

  messages: any[];
  logins: any[];
  public constructor() {}

  ngOnInit(): void {}

}
