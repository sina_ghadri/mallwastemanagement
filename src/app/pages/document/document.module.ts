import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DocumentRoutingModule } from './document-routing.module';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { IndexComponent } from './index/index.component';

import { FormsModule } from '@angular/forms';
import { SigmaSoftModule } from '../../lib/sigmasoft-ng';

import { NgxTreeSelectModule } from '../../components/tree-select';
import { UserService } from '../../services/user.service';
import { WasteTypeService } from '../../services/wasteType.service';
import { StationService } from '../../services/station.service';


@NgModule({
  imports: [
    CommonModule,
    NgxTreeSelectModule,
    DocumentRoutingModule,
    FormsModule,
    SigmaSoftModule,
    

  ],
  providers: [UserService, WasteTypeService, StationService],
  declarations: [CreateComponent, EditComponent, IndexComponent]
})
export class DocumentModule { }
