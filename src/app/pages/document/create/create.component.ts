import { Component, OnInit, ElementRef, EventEmitter, Output } from '@angular/core';
import { ConfigurationService } from '../../../lib/sigmasoft-ng/misc';
import { NotificationService } from '../../../lib/sigmasoft-ng/message/notification/notification.service';
import { NotificationItem, NotificationType } from '../../../lib/sigmasoft-ng/message/notification/models';
import { Document} from '../../../models/document';
import { DocumentService } from '../../../services/document.service';
import { DocumentItem } from '../../../models/documentItem';
import { Global } from '../../../lib/sigmasoft-ts/models';
import { WasteTypeService } from '../../../services/wasteType.service';
import { Station } from '../../../models/station';
import { StationService } from '../../../services/station.service';
import { arrayToTree } from '../../../models/arrayToTree';
import { UserService } from '../../../services/user.service';


@Component({
  selector: 'document-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  public reasontextField='text';
  public AllowParentSelection = false;
  public ShowFilter = true;
  public Disabled = false;
  public FilterPlaceholder = 'جستجو کنید...';
  public MaxDisplayed = 2;
  public wasteobj:any;

  obj: Document = new Document;
  touched:boolean=false;
  sendStatus:boolean;
  progressForm:boolean;
  global = Global;
  wasteTypeItems: any[] = [];
  stationItems: Station[] = [];


  @Output() oncomplete: EventEmitter<Document> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();

  constructor(private element: ElementRef, public configService: ConfigurationService, private service: DocumentService, private userService: UserService, private wasteTypeService: WasteTypeService, private stationService: StationService, private notificationService:NotificationService) { }

  ngOnInit() {
    this.getWasteType();
    this.getStation();
  }

  getWasteType() {
    this.wasteTypeService.getAll().subscribe(res=>{
      let changedData = arrayToTree(res.Data as any[], { id: 'Id', parentId: 'ParentId' });
      this.wasteTypeItems=changedData
    })
  }

  getStation() {
    this.stationService.getAll().subscribe(res=>{
      this.stationItems = res.Data;
    })
  }
  getUser(){
    this.userService.getById(this.obj.UserId).subscribe(res=>{
      this.obj.User = res.Data;
    })
  }
  AddItems(){
     this.obj.Items.push(new DocumentItem());
  }
  delteItem(index){
    this.obj.Items.splice(index, 1);
  }

  CreateModel(){
    this.focus()
    this.sendStatus=false;
    this.touched = false;
    this.obj=new Document();
  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }

  Save(form){
    this.touched = true;
    this.sendStatus=false;
    if (form.valid) {
      this.progressForm=true;

      if(this.obj.Id){
        this.service.update(this.obj).subscribe(res=>{

          this.SendNoti('Success','پیام','رسید با موفقیت ویرایش شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.progressForm=false;
        },error => {
          this.progressForm=false;
        });
      }
      else {
        this.service.insert(this.obj).subscribe(res=>{this.obj.Id=res.Data.Id;

          this.SendNoti('Success','پیام','رسید با موفقیت افزوده شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.sendStatus=true;
          this.progressForm=false;
        },error => {
          this.progressForm=false;
        });
      }

    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
