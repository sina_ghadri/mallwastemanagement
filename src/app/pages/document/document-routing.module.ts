import { NgModule } from '@angular/core';
import {Routes, RouterModule, Route} from '@angular/router';

import {IndexComponent} from './index/index.component';
import {CreateComponent} from './create/create.component';
import {EditComponent} from './edit/edit.component';

const routes: Route[] = [
  { path: '', component:IndexComponent},
  { path: 'Create', component:CreateComponent},
  { path: 'Update', component:EditComponent},
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRoutingModule { }
