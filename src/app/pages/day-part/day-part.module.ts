import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DayPartRoutingModule } from './day-part-routing.module';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { IndexComponent } from './index/index.component';

@NgModule({
  imports: [
    CommonModule,
    DayPartRoutingModule
  ],
  declarations: [CreateComponent, EditComponent, IndexComponent]
})
export class DayPartModule { }
