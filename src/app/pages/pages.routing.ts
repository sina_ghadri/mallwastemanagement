import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {PagesComponent} from './pages.component';
import {AccountComponent} from './account/account.component';





const routes: Route[] = [
  { path: '', component: PagesComponent,
    children: [
      { path: 'Account', component: AccountComponent},
      { path: 'Basic-Information',  loadChildren: './basic-information/basic-information.module#BasicInformationModule'},
      { path: 'Calendar',  loadChildren: './calendar/calendar.module#CalendarModule'},
      { path: 'Dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
      { path: 'Day-Part',  loadChildren: './day-part/day-part.module#DayPartModule'},
      { path: 'Document',  loadChildren: './document/document.module#DocumentModule'},
      { path: 'Monitoring',  loadChildren: './monitoring/monitoring.module#MonitoringModule'},

      { path: 'Inspection',  loadChildren: './inspection/inspection.module#InspectionModule'},
   

    ]  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRouting { }
