import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MonitoringRoutingModule } from './monitoring-routing.module';
import { MonitoringComponent } from './monitoring.component';

import { ProgressBarModule } from '../../lib/sigmasoft-ng/progress/bar/bar.module';


@NgModule({
  imports: [
    CommonModule,
    MonitoringRoutingModule,
    ProgressBarModule
  ],
  declarations: [MonitoringComponent]
})
export class MonitoringModule { }
