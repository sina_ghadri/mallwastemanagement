import { Component, OnInit } from '@angular/core';
import {WasteTypeService} from '../../../services/wasteType.service';
import {TreeViewItem} from '../../../lib/sigmasoft-ng/data/treeview/models';
import {WasteType} from '../../../models/wasteType';
enum Modes {
  Detail = 1,
  Edit = 2,
  Create = 3,
}
@Component({
  selector: 'app-waste-type',
  templateUrl: './waste-type.component.html',
  styleUrls: ['./waste-type.component.css']
})
export class WasteTypeComponent implements OnInit {
  id: number;
  mode: Modes = 1;
  obj: WasteType=new WasteType();

  sla: TreeViewItem[] = [];
  wasteTypes: TreeViewItem[] = [];



  constructor(private service:WasteTypeService) { }

  ngOnInit() {
    this.loadWasteTypes();
  }


  loadWasteTypes() {
    this.service.getAll().subscribe(data => {
      for (let i = 0; i < data.Count; i++) {
        const item = data.Data[i];
        item.Parent = data.Data.find(x => x.Id == item.ParentId);
      }
      this.wasteTypes = [];
      this.sla = this.loadWasteTypeItems(data.Data, this.wasteTypes, this.sla);
    });
  }
  loadWasteTypeItems(data: WasteType[], nodes: TreeViewItem[], sla: TreeViewItem[], parent?: TreeViewItem): TreeViewItem[] {
    let items = data.filter(x => (parent && x.ParentId == parent.id) || (!parent && !x.ParentId));
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      let node = new TreeViewItem();
      node.id = item.Id;
      node.icon = 'mdi mdi-lumx color-red';
      node.text = item.Name ;
      node.value=item;
      let oldNode = sla.find(x => x.id == item.Id);
      if (oldNode) {
        node.isOpen = oldNode.isOpen;
        node.isSelected = oldNode.isSelected;
      }
      this.loadWasteTypeItems(data, node.items, sla, node);
      nodes.push(node);
      sla.push(node);
    }
    return sla;
  }
  selectedChange(model:TreeViewItem) {
    this.obj=model.value;
    this.id = model.id;
  }

  edit() {
    this.mode = Modes.Edit;
  }
  editComplete(result: WasteType) {

    if (result) this.id = result.Id;
    this.mode = Modes.Detail;
    this.loadWasteTypes();
  }
  delete() {

    if (!confirm("آیا از حذف این ردیف اطمینان دارید ؟")) return;
    this.service.deleteById(this.id).subscribe(op => {
      if (op.Success)  this.loadWasteTypes();
      else alert(op.Message || 'خطا در انجام عملیات');
    })
  }
  create() {
    this.mode = Modes.Create;
  }
  createComplete(result: WasteType) {
    if (result) this.id = result.Id;
    this.mode = Modes.Detail;
    this.loadWasteTypes();
  }



}
