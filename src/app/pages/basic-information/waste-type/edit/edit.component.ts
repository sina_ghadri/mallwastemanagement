import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {WasteType} from '../../../../models/wasteType';
import {WasteTypeService} from '../../../../services/wasteType.service';

@Component({
  selector: 'waste-type-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Input() id: number;
  @Input() data: WasteType = new WasteType;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter;
  obj: WasteType = new WasteType;

  progress: boolean = false;
  message: string;

  constructor(private service: WasteTypeService) { }

  ngOnInit() {
    this.obj=Object.assign({},this.data);
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.obj=Object.assign({},this.data);
    // this.obj=this.obj;
    // if(this.id) this.service.getById(this.id).subscribe(data => this.obj = data);
  }
  submit(e: Event) {
    e.preventDefault();
    this.progress = true;
    this.service.update(this.obj).subscribe(op => {
      if(op) this.oncomplete.emit(op);
    }, e => this.progress = false, () => this.progress = false);
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
