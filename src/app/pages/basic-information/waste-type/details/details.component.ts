import {Component, Input, OnInit, SimpleChanges} from '@angular/core';
import {WasteTypeService} from '../../../../services/wasteType.service';
import {WasteType} from '../../../../models/wasteType';

@Component({
  selector: 'waste-type-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  @Input() id: number;
  @Input() data: WasteType = new WasteType;
  obj: WasteType = new WasteType;
  message: string;

  constructor(private service: WasteTypeService) { }

  ngOnInit() {
    this.obj=Object.assign({},this.data);
  }
  ngOnChanges(changes: SimpleChanges): void {

    this.obj=Object.assign({},this.data);
    // this.obj=this.obj;
    // if(this.id) this.service.getById(this.id).subscribe(data => this.obj = data);
  }

}
