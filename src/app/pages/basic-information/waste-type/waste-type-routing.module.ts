import { NgModule } from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {WasteTypeComponent} from './waste-type.component';
const routes: Route[] = [
  { path: '', component:WasteTypeComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class WasteTypeRoutingModule { }
