import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WasteTypeRoutingModule } from './waste-type-routing.module';
import { WasteTypeComponent } from './waste-type.component';
import {WasteTypeService} from '../../../services/wasteType.service';
import {SigmaSoftModule} from '../../../lib/sigmasoft-ng';
import {FormsModule} from '@angular/forms';
import { DetailsComponent } from './details/details.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { DeleteComponent } from './delete/delete.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    WasteTypeRoutingModule,
    SigmaSoftModule
  ],
  providers:[WasteTypeService],
  declarations: [WasteTypeComponent, DetailsComponent, CreateComponent, EditComponent, DeleteComponent]
})
export class WasteTypeModule { }
