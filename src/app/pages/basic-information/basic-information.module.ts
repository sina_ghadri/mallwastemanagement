import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasicInformationRoutingModule } from './basic-information-routing.module';

@NgModule({
  imports: [
    CommonModule,
    BasicInformationRoutingModule
  ],
  declarations: []
})
export class BasicInformationModule { }
