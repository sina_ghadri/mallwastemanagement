import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';

import {Global} from '../../../../lib/sigmasoft-ts/models';
import {Station} from '../../../../models/station';
import {StationService} from '../../../../services/station.service';
import {PlaceService} from '../../../../services/place.service';
import {UseTypeService} from '../../../../services/useType.service';
import {Place} from '../../../../models/place';
import {UseType} from '../../../../models/useType';
import {ODataPagination} from '../../../../models/oDataPagination';
import {arrayToTree} from '../../../../models/arrayToTree';
import {UserService} from '../../../../services/user.service';
import {User} from '../../../../models/user';


@Component({
  selector: 'station-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public reasontextField='text';
  public AllowParentSelection = false;
  public ShowFilter = true;
  public Disabled = false;
  public FilterPlaceholder = 'جستجو کنید...';
  public MaxDisplayed = 2;
  public useTypeobj:any;

  progress: boolean;
  message: string;
  obj: Station = new Station;
  touched:boolean=false;
  progressForm:boolean;
  buildingItems:Place[]=[];
  gradeItems:Place[]=[];
  placeItems:Place[]=[];
  buildingObj:Place=new Place();
  gradeObj:Place=new Place();
  useTypeItems:any[]=[];
  userItems:User[]=[];
  ShowPlaceChooser:boolean=false;
  @Output() oncomplete: EventEmitter<Station> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();
  sendStatus:boolean;
  global = Global;
  NameError:string;
  oDataPagination:ODataPagination=new ODataPagination();
  constructor(public configService: ConfigurationService,private useTypeService:UseTypeService,private placeService:PlaceService,private element: ElementRef,private service: StationService,private userService: UserService,private  notificationService:NotificationService) {
  }

  ngOnInit() {
    if(this.configService.checkPermission('ROLE_StationAdd'))
    {
      setTimeout(() => this.focus(), 100);
      this.loadStation();
      this.loadUsers();
      this.getBuildingData(this.oDataPagination);
    }
    else {
      this.onShowList.emit()
    }


  }
  checkName(){
   if(this.obj.Name){
     let oDataPagination:ODataPagination=new ODataPagination();

     oDataPagination.filter=`Name eq '${this.obj.Name}'`;
     this.service.getWithPagination(oDataPagination).subscribe(res=>{
       if(res.Data.length){
         this.NameError='ایستگاه با همچین نامی وجود دارد!'
       }
       else {
         this.NameError=null;
       }
     })
   }
   else {
     this.NameError=null;
   }

  }
  mapEditorComplete(result) {

    if(result.placeId){
      debugger
      this.obj.Place=this.placeItems.find(i=>i.Id==result.placeId);
      this.ShowPlaceChooser=false;
      this.obj.PlaceId=result.placeId

    }
    else {
      this.gradeObj=new Place()
    }
    // this.mode = Modes.Detail;
  }
  loadUsers(){
    this.userService.getAll().subscribe(res=>{
      this.userItems=[];
      for(const item of res.Data)
      {
        item.Fullname=item.Firstname+ ' ' + item.Lastname;
        this.userItems.push(item)
      }

    })
  }
  loadStation(){
    this.useTypeService.getAll().subscribe(res=>{
      let changedData = arrayToTree(res.Data as any[], { id: 'Id', parentId: 'ParentId' });
      this.useTypeItems=changedData
    })
  }
  getBuildingData(oDataPagination:ODataPagination){
    this.buildingItems=[];
    oDataPagination.filter=`ParentId eq ${null}`;
    this.placeService.getWithPagination(oDataPagination).subscribe(res=>{
      this.buildingItems=res.Data;
    })
  }
  getGradeData(oDataPagination:ODataPagination,ParentId){
    this.gradeItems=[];
    oDataPagination.filter=`ParentId eq ${ParentId}`;
    this.placeService.getWithPagination(oDataPagination).subscribe(res=>{
      this.gradeItems=res.Data;
    })
  }
  getPlaceData(oDataPagination:ODataPagination,ParentId){
    oDataPagination.filter=`ParentId eq ${ParentId}`;
    this.placeService.getWithPagination(oDataPagination).subscribe(res=>{

      this.placeItems=res.Data;

    })
  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  CreateModel(){
    this.focus()
    this.sendStatus=false;
    this.touched = false;
    this.obj.Name='';
    this.obj.Id=null;
    // this.buildingObj=new Place();
    // this.gradeObj=new Place();
    // this.useTypeobj=null;
  }

  Save(form){
    this.touched = true;
    this.sendStatus=false;
    if (form.valid) {
      this.progressForm=true;

      if(this.obj.Id){
        this.service.update(this.obj).subscribe(res=>{

          this.SendNoti('Success','پیام','ایستگاه با موفقیت ویرایش شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }
      else {
        this.service.insert(this.obj).subscribe(res=>{
          this.obj.Id=res.Data.Id;

          this.SendNoti('Success','پیام','ایستگاه با موفقیت افزوده شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.sendStatus=true;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }



    }
  }

  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
