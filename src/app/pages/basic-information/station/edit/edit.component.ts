import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTableRow} from '../../../../lib/sigmasoft-ng/data/table/models';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';
import {Global} from '../../../../lib/sigmasoft-ts/models';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {UseType} from '../../../../models/useType';
import {Place} from '../../../../models/place';
import {UseTypeService} from '../../../../services/useType.service';
import {PlaceService} from '../../../../services/place.service';
import {Station} from '../../../../models/station';
import {StationService} from '../../../../services/station.service';
import {ODataPagination} from '../../../../models/oDataPagination';
import {arrayToTree, TreeItemModel} from '../../../../models/arrayToTree';
import {UserService} from '../../../../services/user.service';
import {User} from '../../../../models/user';



@Component({
  selector: 'station-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  public reasontextField='text';
  public AllowParentSelection = false;
  public ShowFilter = true;
  public Disabled = false;
  public FilterPlaceholder = 'جستجو کنید...';
  public MaxDisplayed = 2;
  public useTypeobj:TreeItemModel=new TreeItemModel();

  @Input() data:DataTableRow;
  progress: boolean;
  message: string;
  obj: Station = new Station;
  touched:boolean=false;
  progressForm:boolean;
  buildingItems:Place[]=[];
  gradeItems:Place[]=[];
  placeItems:Place[]=[];
  buildingObj:Place=new Place();
  gradeObj:Place=new Place();
  useTypeItems:any[]=[];
  userItems:User[]=[];
  ShowPlaceChooser:boolean=false;
  @Output() oncomplete: EventEmitter<Station> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();

  global = Global;
  oDataPagination:ODataPagination=new ODataPagination();
  oDataPagination1:ODataPagination=new ODataPagination();
  oDataPagination2:ODataPagination=new ODataPagination();
  constructor(public configService: ConfigurationService,private userService: UserService,private useTypeService:UseTypeService,private placeService:PlaceService,private element: ElementRef,private service: StationService,private  notificationService:NotificationService) {
  }

  ngOnInit() {
    this.obj=Object.assign({},this.data.value);
    // this.placeService.getById(this.obj.PlaceId).subscribe(res=>{
    //
    //   this.obj.Place=res;
    //   this.buildingObj=this.obj.Place.Parent;
    //   this.gradeObj=this.obj.Place.Parent.Parent;
    // });

    this.useTypeobj.value=this.obj.UseType;
    this.useTypeobj.id=this.useTypeobj.value.Id;
    this.useTypeobj.text=this.useTypeobj.value.Name;
    this.useTypeobj.checked=null;
    this.useTypeobj.children=[];


    this.buildingObj=this.obj.Place.Parent;
    this.gradeObj=this.obj.Place.Parent.Parent;
    this.loadStation();

    this.getBuildingData(this.oDataPagination);
    this.getGradeData(this.oDataPagination1,this.obj.Place.Parent.ParentId);
    this.getPlaceData(this.oDataPagination2,this.obj.Place.ParentId);
    this.loadUsers();
    setTimeout(() => this.focus(), 100);

  }
  mapEditorComplete(result) {

    if(result.placeId){
      debugger
      this.obj.Place=this.placeItems.find(i=>i.Id==result.placeId);
      this.ShowPlaceChooser=false;
      this.obj.PlaceId=result.placeId

    }
    else {
      this.gradeObj=new Place()
    }
    // this.mode = Modes.Detail;
  }
  loadUsers(){
    this.userService.getAll().subscribe(res=>{
      this.userItems=[];
      for(const item of res.Data)
      {
        item.Fullname=item.Firstname+ ' ' + item.Lastname;
        this.userItems.push(item)
      }

    })
  }
  loadStation(){
    this.useTypeService.getAll().subscribe(res=>{
      let changedData = arrayToTree(res.Data as any[], { id: 'Id', parentId: 'ParentId' });
      this.useTypeItems=changedData
    })
  }
  getBuildingData(oDataPagination:ODataPagination){
    this.buildingItems=[];
    oDataPagination.filter=`ParentId eq ${null}`;
    this.placeService.getWithPagination(oDataPagination).subscribe(res=>{
      this.buildingItems=res.Data;
      this.buildingObj=this.obj.Place.Parent.Parent;

    })
  }
  getGradeData(oDataPagination:ODataPagination,ParentId){
    this.gradeItems=[];
    oDataPagination.filter=`ParentId eq ${ParentId}`;
    this.placeService.getWithPagination(oDataPagination).subscribe(res=>{
      this.gradeItems=res.Data;
      this.gradeObj=this.obj.Place.Parent;
    })
  }
  getPlaceData(oDataPagination:ODataPagination,ParentId){
    oDataPagination.filter=`ParentId eq ${ParentId}`;
    this.placeService.getWithPagination(oDataPagination).subscribe(res=>{

      this.placeItems=res.Data;

    })
  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  Save(form){
    this.touched = true;
    if (form.valid) {
      this.progressForm=true;
      this.service.update(this.obj).subscribe(res=>{

        this.SendNoti('Success','پیام','ایستگاه با موفقیت ویرایش شد.');
        this.oncomplete.emit(this.obj);
        this.touched = false;
        this.progressForm=false;
        // this.SendNoti('Primary');

        // this.SendNoti('Warning');
        // this.SendNoti('Error');
      },error => {
        this.progressForm=false;
      });


    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
