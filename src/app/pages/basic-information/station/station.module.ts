import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StationRoutingModule } from './station-routing.module';
import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { HistoryComponent } from './history/history.component';
import {FormsModule} from '@angular/forms';
import {SigmaSoftModule} from '../../../lib/sigmasoft-ng';
import {UseTypeService} from '../../../services/useType.service';
import {StationService} from '../../../services/station.service';
import {PlaceService} from '../../../services/place.service';
import {NgxTreeSelectModule} from '../../../components/tree-select';
import {PlaceModule} from '../place/place.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    NgxTreeSelectModule,
    StationRoutingModule,
    PlaceModule
  ],
  providers:[UseTypeService,StationService,PlaceService],
  declarations: [IndexComponent, CreateComponent, EditComponent, HistoryComponent]
})
export class StationModule { }
