import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Modal} from '../../../../lib/sigmasoft-ng/overlay/modal/models';
import {
  DataTable,
  DataTableRow,
  DataTableColumn,
  DataTableButton,
  DataTableMenu,
  DataTableMenuItem
} from '../../../../lib/sigmasoft-ng';

import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';

import {ODataPagination} from '../../../../models/oDataPagination';
import {StationService} from '../../../../services/station.service';
import {Station} from '../../../../models/station';





@Component({
  selector: 'station-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  @ViewChild('datepickerTemplate') datepickerTemplate: TemplateRef<any>;
  @ViewChild('phoneTemplate') phoneTemplate: TemplateRef<any>;
  @ViewChild('columnTrueFalseType') columnTrueFalseType: TemplateRef<any>;

  datatable: DataTable = new DataTable({ language: 'fa' });

  paginationSearch:ODataPagination=new ODataPagination();

  row:  DataTableRow;
  insertModal:boolean;
  detailModal: boolean;
  updateModal: boolean;
  deleteModal: Modal = new Modal;
  listModal: boolean;



  constructor(public configService: ConfigurationService,private service: StationService) { }
  ngOnInit(): void {


    this.datatable.lazyloading = true;
    this.datatable.onrefresh.subscribe(() => this.load());
    this.datatable.onstateChange.subscribe((state) => {
      console.log('onstateChange', state);
      this.paginationSearch=new ODataPagination();
      this.paginationSearch.skip=(state.page-1)*10;
      this.paginationSearch.top=state.limit;

      if(state.searchBy.length>0 || state.sortBy){
        if(state.searchBy.length>0){
          let i=0;
          for(const item of  state.searchBy)
          {

            if(i>0 ||  this.paginationSearch.filter)
            {this.paginationSearch.filter= this.paginationSearch.filter+' and '+` indexof(${item.name}, '${item.search.value}') gt -1`}
            else {this.paginationSearch.filter= `indexof(${item.name}, '${item.search.value}') gt -1`}
            i++;
          }
        }
        if(state.sortBy){
          if(state.sortBy.title!='وضعیت')
          {
            if(state.ascending) {this.paginationSearch.orderby=state.sortBy.name+" asc";}
            else {this.paginationSearch.orderby=state.sortBy.name+" desc";}
          }
        }
      }
      this.loadData();
    });
    this.insertModal=true;

    this.datatable.columns.push(new DataTableColumn('نام', 'Name'));
    this.datatable.columns.push(new DataTableColumn('نوع کاربری', row => {return row.value.UseType.Name}).setName('UseType/Name'));
    this.datatable.columns.push(new DataTableColumn('ساختمان', row => {return row.value.Place.Parent.Parent.Name}).setName('Place/Parent/Parent/Name'));
    this.datatable.columns.push(new DataTableColumn('طبقه', row => {return row.value.Place.Parent.Name}).setName('Place/Parent/Name'));
    this.datatable.columns.push(new DataTableColumn('مکان', row => {return  row.value.Place.Name}).setName('Place/Name'));
    this.datatable.columns.push(new DataTableColumn('پلاک', row => {return  row.value.Tag}).setName('Tag'));
    this.datatable.columns.push(new DataTableColumn('مساحت', row => {return  row.value.Space}).setName('Space'));
    this.datatable.columns.push(new DataTableColumn('کاربر واسط', row => {


      if(row.value.ConnectiveUser)
      {
        return  `${ row.value.ConnectiveUser.Firstname+' '+row.value.ConnectiveUser.Lastname}`
      }
      else {
        return ''
      }
     }).setName('User.Firstname'));
    this.datatable.columns.push(new DataTableColumn('وضعیت', row => `<i class="mdi ${row.value.IsActive ? 'mdi-check color-green' : 'mdi-close color-red'}"></i>`).setWidth('60px').setClass('text-center').setName('IsActive'));


    //SetPermission
    if(this.configService.checkPermission('ROLE_StationAdd'))
    {
      this.datatable.buttons.push(new DataTableButton('افزودن', () => this.insert(),'mdi  mdi-plus','btn-blue-light'));
    }
    let menu = new DataTableMenu;
    if(this.configService.checkPermission('ROLE_StationUpdate'))
    {
      menu.items.push(new DataTableMenuItem('ویرایش', row => this.update(row), 'mdi mdi-pencil', 'color-amber'));
    }
    if(this.configService.checkPermission('ROLE_StationUpdate'))
    {
      menu.items.push(new DataTableMenuItem('حذف', row => this.delete(row), 'mdi mdi-delete', 'color-red'));
    }
    if(this.configService.checkPermission('ROLE_StationExcel'))
    {
      this.datatable.options.exportOptions.excel=true;
    }
    if(this.configService.checkPermission('ROLE_StationPrint'))
    {
      this.datatable.options.exportOptions.print=true;
    }

    this.datatable.menus.push(menu);

    this.load();
  }
  loadData() {
    this.datatable.progress = true;
    this.service.getWithPagination(this.paginationSearch).subscribe(op => {
      this.datatable.dataSource = op.Data;
      this.datatable.count=op.Count;




    }, () => this.datatable.progress = false, () => this.datatable.progress = false);
  }
  load() {
    this.datatable.progress = true;
    this.paginationSearch=new ODataPagination();
    this.paginationSearch.top=10;
    this.paginationSearch.skip=0;

    this.datatable.firstPage();
    this.service.getWithPagination(this.paginationSearch).subscribe(op => {this.datatable.dataSource = op.Data; this.datatable.count=op.Count}, () => this.datatable.progress = false, () => this.datatable.progress = false);
  }
  loadById(id: number) {
    this.datatable.progress = true;
    this.service.getById(id).subscribe(op => this.datatable.replaceRow(row => row.value.id == id, op.Count), () => this.datatable.progress = false, () => this.datatable.progress = false);
  }
  insert() { this.listModal=false;this.insertModal=true;}
  insertComplete(result: Station) {
    if(result && !result.Id) this.datatable.addRow(result);     if(result && result.Id) this.datatable.addRow(result);
    // if(result) this.load();
  }
  detail(row: DataTableRow) { this.row = row; this.detailModal=true; }
  update(row: DataTableRow) { this.row = row;
    this.updateModal=true;this.insertModal=false;this.listModal=false ;
  }
  updateComplete(result: Station) {
    // if(result) this.loadById(this.row.value.id);
    if(result) this.datatable.replaceRow(row => row.value.Id == this.row.value.Id, result);
    this.updateModal=false;
    this.listModal=true;
  }
  delete(row: DataTableRow) { this.row = row; this.deleteModal.open(); }
  deleteComplete(result: number) {
    this.service.deleteById(this.row.value.Id).subscribe(res=>{
      this.datatable.removeRow(row => row.value.Id == this.row.value.Id);

      if(this.datatable.rows.length==0){
        this.load();
      }
      else {
        this.loadData();
      }
      this.deleteModal.close();
    },error=>{
      this.deleteModal.close();
    })

  }
}
