import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {Container} from '../../../../models/container';
import {ContainerService} from '../../../../services/container.service';
import {ContainerTypeService} from '../../../../services/containerType.service';
import {ContainerType} from '../../../../models/ContainerType';
import {Global} from '../../../../lib/sigmasoft-ts/models';
import {JDate} from '../../../../lib/sigmasoft-ts/datetime';


@Component({
  selector: 'container-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  progress: boolean;
  message: string;
  obj: Container = new Container;
  touched:boolean=false;
  progressForm:boolean;
  containerTypeItems:ContainerType[]=[];
  @Output() oncomplete: EventEmitter<Container> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();
  sendStatus:boolean;
  global = Global;
  constructor(public configService: ConfigurationService,private containerTypeService:ContainerTypeService,private element: ElementRef,private service: ContainerService,private  notificationService:NotificationService) {
  }
  setEndDate(){
    this.obj.EndJDate= JDate.parseJDate(this.obj.StartJDate).addYear(this.obj.ContainerType.LongLife).format('yyyy/MM/dd');
    this.obj.EndDate= JDate.parseJDate(this.obj.StartDate).addYear(this.obj.ContainerType.LongLife).format('yyyy/MM/dd');
  }

  ngOnInit() {
    if(this.configService.checkPermission('ROLE_ContainerAdd'))
    {
      setTimeout(() => this.focus(), 100);
      this.loadContainerType();
    }
    else {
      this.onShowList.emit()
    }


  }

  loadContainerType(){
    this.containerTypeService.getAll().subscribe(res=>{
      this.containerTypeItems=res.Data;
    })
  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  CreateModel(){
    this.focus()
    this.sendStatus=false;
    this.touched = false;
    this.obj=new Container();
  }

  Save(form){
    this.touched = true;
    this.sendStatus=false;
    if (form.valid) {
      this.progressForm=true;

      if(this.obj.Id){
        this.service.update(this.obj).subscribe(res=>{

          this.SendNoti('Success','پیام','ظرف با موفقیت ویرایش شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }
      else {

        this.service.insert(this.obj).subscribe(res=>{this.obj.Id=res.Data.Id;

          this.SendNoti('Success','پیام','ظرف با موفقیت افزوده شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.sendStatus=true;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }



    }
  }

  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
