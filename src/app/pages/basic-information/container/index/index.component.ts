import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {Modal} from '../../../../lib/sigmasoft-ng/overlay/modal/models';
import {
  DataTable,
  DataTableRow,
  DataTableColumn,
  DataTableButton,
  DataTableMenu,
  DataTableMenuItem
} from '../../../../lib/sigmasoft-ng';

import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';

import {ODataPagination} from '../../../../models/oDataPagination';
import {ContainerService} from '../../../../services/container.service';
import {Container} from '../../../../models/container';
import {JDate} from '../../../../lib/sigmasoft-ts/datetime';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  @ViewChild('datepickerTemplate') datepickerTemplate: TemplateRef<any>;
  @ViewChild('phoneTemplate') phoneTemplate: TemplateRef<any>;
  @ViewChild('columnTrueFalseType') columnTrueFalseType: TemplateRef<any>;

  datatable: DataTable = new DataTable({ language: 'fa' });

  paginationSearch:ODataPagination=new ODataPagination();

  row:  DataTableRow;
  insertModal:boolean;
  detailModal: boolean;
  updateModal: boolean;
  deleteModal: Modal = new Modal;
  listModal: boolean;



  constructor(public configService: ConfigurationService,private service: ContainerService) { }
  ngOnInit(): void {


    this.datatable.lazyloading = true;
    this.datatable.onrefresh.subscribe(() => this.load());
    this.datatable.onstateChange.subscribe((state) => {
      console.log('onstateChange', state);
      this.paginationSearch=new ODataPagination();
      this.paginationSearch.skip=(state.page-1)*10;
      this.paginationSearch.top=state.limit;

      if(state.searchBy.length>0 || state.sortBy){
        if(state.searchBy.length>0){
          let i=0;
          for(const item of  state.searchBy)
          {

            if(i>0 ||  this.paginationSearch.filter)
            {this.paginationSearch.filter= this.paginationSearch.filter+' and '+` indexof(${item.name}, '${item.search.value}') gt -1`}
            else {this.paginationSearch.filter= `indexof(${item.name}, '${item.search.value}') gt -1`}
            i++;
          }
        }
        if(state.sortBy){
          if(state.sortBy.title!='وضعیت')
          {
            if(state.ascending) {this.paginationSearch.orderby=state.sortBy.name+" asc";}
            else {this.paginationSearch.orderby=state.sortBy.name+" desc";}
          }
        }
      }
      this.loadData();
    });
    this.insertModal=true;

    this.datatable.columns.push(new DataTableColumn('کد', 'Code'));
    this.datatable.columns.push(new DataTableColumn('تاریخ شروع', row => {return JDate.toJDate(JDate.parseDate(row.value.StartDate)).format('yyyy/MM/dd'); }).setName('StartDate'));
    this.datatable.columns.push(new DataTableColumn('تاریخ پایان', row => {return JDate.toJDate(JDate.parseDate(row.value.EndDate)).format('yyyy/MM/dd'); }).setName('EndDate'));
    this.datatable.columns.push(new DataTableColumn('نوع ظرف', row => {return row.value.ContainerType.Name}).setName('ContainerType/Name'));
    // this.datatable.columns.push(new DataTableColumn('وضعیت', row => `<i class="mdi ${row.value.IsActive ? 'mdi-check color-green' : 'mdi-close color-red'}"></i>`).setWidth('60px').setClass('text-center'));
    //

    //SetPermission
    if(this.configService.checkPermission('ROLE_ContainerAdd'))
    {
      this.datatable.buttons.push(new DataTableButton('افزودن', () => this.insert(),'mdi  mdi-plus','btn-blue-light'));
    }
    let menu = new DataTableMenu;
    if(this.configService.checkPermission('ROLE_ContainerUpdate'))
    {
      menu.items.push(new DataTableMenuItem('ویرایش', row => this.update(row), 'mdi mdi-pencil', 'color-amber'));
    }
    if(this.configService.checkPermission('ROLE_ContainerUpdate'))
    {
      menu.items.push(new DataTableMenuItem('حذف', row => this.delete(row), 'mdi mdi-delete', 'color-red'));
    }
    if(this.configService.checkPermission('ROLE_ContainerExcel'))
    {
      this.datatable.options.exportOptions.excel=true;
    }
    if(this.configService.checkPermission('ROLE_ContainerPrint'))
    {
      this.datatable.options.exportOptions.print=true;
    }

    this.datatable.menus.push(menu);

    this.load();
  }
  loadData() {
    this.datatable.progress = true;
    this.service.getWithPagination(this.paginationSearch).subscribe(op => {
      this.datatable.dataSource = op.Data;
      this.datatable.count=op.Count;
    }, () => this.datatable.progress = false, () => this.datatable.progress = false);
  }
  load() {
    this.datatable.progress = true;
    this.paginationSearch=new ODataPagination();
    this.paginationSearch.top=10;
    this.paginationSearch.skip=0;

    this.datatable.firstPage();
    this.service.getWithPagination(this.paginationSearch).subscribe(op => {this.datatable.dataSource = op.Data; this.datatable.count=op.Count}, () => this.datatable.progress = false, () => this.datatable.progress = false);
  }
  loadById(id: number) {
    this.datatable.progress = true;
    this.service.getById(id).subscribe(op => this.datatable.replaceRow(row => row.value.id == id, op.Data), () => this.datatable.progress = false, () => this.datatable.progress = false);
  }
  insert() { this.listModal=false;this.insertModal=true;}
  insertComplete(result: Container) {
    if(result && !result.Id) this.datatable.addRow(result);     if(result && result.Id) this.datatable.addRow(result);
    // if(result) this.load();
  }
  detail(row: DataTableRow) { this.row = row; this.detailModal=true; }
  update(row: DataTableRow) { this.row = row;
    this.updateModal=true;this.insertModal=false;this.listModal=false ;
  }
  updateComplete(result: Container) {
    // if(result) this.loadById(this.row.value.id);
    if(result) this.datatable.replaceRow(row => row.value.Id == this.row.value.Id, result);
    this.updateModal=false;
    this.listModal=true;
  }
  delete(row: DataTableRow) { this.row = row; this.deleteModal.open(); }
  deleteComplete(result: number) {
    this.service.deleteById(this.row.value.Id).subscribe(res=>{
      this.datatable.removeRow(row => row.value.Id == this.row.value.Id);

      if(this.datatable.rows.length==0){
        this.load();
      }
      else {
        this.loadData();
      }
      this.deleteModal.close();
    },error=>{
      this.deleteModal.close();
    })

  }
}
