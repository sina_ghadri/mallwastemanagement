import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTableRow} from '../../../../lib/sigmasoft-ng/data/table/models';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';
import {Container} from '../../../../models/container';
import {ContainerService} from '../../../../services/container.service';
import {Global} from '../../../../lib/sigmasoft-ts/models';
import {ContainerTypeService} from '../../../../services/containerType.service';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {ContainerType} from '../../../../models/ContainerType';
import {JDate} from '../../../../lib/sigmasoft-ts/datetime';



@Component({
  selector: 'container-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Input() data:DataTableRow;
  progress: boolean;
  message: string;
  obj: Container = new Container;
  containerTypeItems:ContainerType[]=[];
  touched:boolean=false;
  global = Global;
  progressForm:boolean;
  @Output() oncomplete: EventEmitter<Container> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();

  constructor(public configService: ConfigurationService,private containerTypeService:ContainerTypeService,private element: ElementRef,private service: ContainerService,private  notificationService:NotificationService) {
  }
  ngOnInit() {
    this.obj=Object.assign({},this.data.value);
    this.obj.StartJDate=JDate.toJDate(JDate.parseDate(this.obj.StartDate)).format('yyyy/MM/dd');
    this.obj.EndJDate=JDate.toJDate(JDate.parseDate(this.obj.EndDate)).format('yyyy/MM/dd');
    setTimeout(() => this.focus(), 100);
    this.loadContainerType();
  }
  loadContainerType(){
    this.containerTypeService.getAll().subscribe(res=>{
      this.containerTypeItems=res.Data;
    })
  }
  setEndDate(){
    this.obj.EndJDate= JDate.parseJDate(this.obj.StartJDate).addYear(this.obj.ContainerType.LongLife).format('yyyy/MM/dd');
    this.obj.EndDate= JDate.parseJDate(this.obj.StartDate).addYear(this.obj.ContainerType.LongLife).format('yyyy/MM/dd');
  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  Save(form){
    this.touched = true;
    if (form.valid) {
      this.progressForm=true;
      this.service.update(this.obj).subscribe(res=>{

        this.SendNoti('Success','پیام','ظرف با موفقیت ویرایش شد.');
        this.oncomplete.emit(this.obj);
        this.touched = false;
        this.progressForm=false;
        // this.SendNoti('Primary');

        // this.SendNoti('Warning');
        // this.SendNoti('Error');
      },error => {
        this.progressForm=false;
      });


    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
