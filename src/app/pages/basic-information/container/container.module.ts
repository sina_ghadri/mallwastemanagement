import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContainerRoutingModule } from './container-routing.module';
import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import {SigmaSoftModule} from '../../../lib/sigmasoft-ng';
import {FormsModule} from '@angular/forms';
import {ContainerService} from '../../../services/container.service';
import {ContainerTypeService} from '../../../services/containerType.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    ContainerRoutingModule
  ],
  providers:[ContainerService,ContainerTypeService],
  declarations: [IndexComponent, CreateComponent, EditComponent]
})
export class ContainerModule { }
