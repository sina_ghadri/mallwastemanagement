import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTableRow} from '../../../../lib/sigmasoft-ng/data/table/models';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';

import {Global} from '../../../../lib/sigmasoft-ts/models';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {DayType} from '../../../../models/dayType';
import {DayTypeService} from '../../../../services/dayType.service';
import {DayPartService} from '../../../../services/dayPart.service';
import {SlaService} from '../../../../services/sla.service';
import {Sla} from '../../../../models/sla';




@Component({
  selector: 'day-type-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Input() data:DataTableRow;
  progress: boolean;
  message: string;
  obj: DayType = new DayType;


  slaItems:Sla[]=[];
  touched:boolean=false;
  global = Global;
  colorItems:any[]=[{Id:'red',Name:'قرمز'},{Id:'blue',Name:'آبی'},{Id:'yellow',Name:'زرد'},{Id:'green',Name:'سبز'},{Id:'pink',Name:'صورتی'}];
  progressForm:boolean;
  @Output() oncomplete: EventEmitter<DayType> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();

  constructor(public configService: ConfigurationService,private dayPartService: DayPartService,private slaService: SlaService,private element: ElementRef,private service: DayTypeService,private  notificationService:NotificationService) {
  }
  ngOnInit() {
    this.obj=Object.assign({},this.data.value);
    this.loadSla();
    setTimeout(() => this.focus(), 100);

  }
  loadSla(){
    this.slaService.getAll().subscribe(res=>{
      this.slaItems=res.Data
    })
  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  Save(form){
    this.touched = true;
    if (form.valid) {
      this.progressForm=true;
      this.service.update(this.obj).subscribe(res=>{

        this.SendNoti('Success','پیام','نوع روز با موفقیت ویرایش شد.');
        this.oncomplete.emit(this.obj);
        this.touched = false;
        this.progressForm=false;
        // this.SendNoti('Primary');

        // this.SendNoti('Warning');
        // this.SendNoti('Error');
      },error => {
        this.progressForm=false;
      });


    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
