import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTableRow} from '../../../../lib/sigmasoft-ng/data/table/models';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';

import {Global} from '../../../../lib/sigmasoft-ts/models';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {DayType} from '../../../../models/dayType';
import {DayTypeService} from '../../../../services/dayType.service';
import {Container} from '../../../../models/container';
import {SlaService} from '../../../../services/sla.service';
import {Sla} from '../../../../models/sla';
import {DayPartService} from '../../../../services/dayPart.service';
import {TimeSplit} from '../../../../components/time-split';




@Component({
  selector: 'day-type-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  progress: boolean;
  message: string;
  obj: DayType = new DayType;

  touched:boolean=false;

  progressForm:boolean;
  global = Global;
  sendStatus:boolean;
  slaItems:Sla[]=[];
  colorItems:any[]=[{Id:'red',Name:'قرمز'},{Id:'blue',Name:'آبی'},{Id:'yellow',Name:'زرد'},{Id:'green',Name:'سبز'},{Id:'pink',Name:'صورتی'}];
  @Output() oncomplete: EventEmitter<DayType> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();
  items:TimeSplit[]=[];
  constructor(public configService: ConfigurationService,private element: ElementRef,private service: DayTypeService,private dayPartService: DayPartService,private slaService: SlaService,private  notificationService:NotificationService) {
  }
  ngOnInit() {

    let item=new TimeSplit;
    item.time="22:22";
    item.text="قرمز";
    item.color="red";
    this.items.push(item);

    let item3=new TimeSplit;
    item3.time="15:22";
    item3.text="آبی";
    item3.color="blue";
    this.items.push(item3);

    let item2=new TimeSplit;
    item2.time="18:22";
    item2.text="زرد";
    item2.color="yellow";
    this.items.push(item2);

    setTimeout(() => this.focus(), 100);

  }

  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  CreateModel(){
    this.focus();
    this.loadSla();
    this.sendStatus=false;
    this.touched = false;
    this.obj=new DayType();
  }
  loadSla(){
    this.slaService.getAll().subscribe(res=>{
      this.slaItems=res.Data
    })
  }
  Save(form){
    this.touched = true;
    this.sendStatus=false;
    if (form.valid) {
      this.progressForm=true;

      if(this.obj.Id){
        this.service.update(this.obj).subscribe(res=>{

          this.SendNoti('Success','پیام','نوع روز با موفقیت ویرایش شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }
      else {
        this.service.insert(this.obj).subscribe(res=>{this.obj.Id=res.Data.Id;

          this.SendNoti('Success','پیام','نوع روز با موفقیت افزوده شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.sendStatus=true;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }



    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
