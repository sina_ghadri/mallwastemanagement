import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DayTypeRoutingModule } from './day-type-routing.module';
import { IndexComponent } from './index/index.component';

import { EditComponent } from './edit/edit.component';
import { HistoryComponent } from './history/history.component';
import {DayTypeService} from '../../../services/dayType.service';
import {FormsModule} from '@angular/forms';
import {SigmaSoftModule} from '../../../lib/sigmasoft-ng';
import {CreateComponent} from './create/create.component';
import {DayPartService} from '../../../services/dayPart.service';
import {SlaService} from '../../../services/sla.service';
import {TimeSplitterModule} from '../../../components/time-splitter.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    DayTypeRoutingModule,
    TimeSplitterModule
  ],
  providers:[DayTypeService,DayPartService,SlaService],
  declarations: [IndexComponent,CreateComponent, EditComponent, HistoryComponent],

})
export class DayTypeModule { }
