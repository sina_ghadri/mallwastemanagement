import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';

import {Global} from '../../../../lib/sigmasoft-ts/models';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {Sla} from '../../../../models/sla';
import {SlaService} from '../../../../services/sla.service';





@Component({
  selector: 'day-type-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  progress: boolean;
  message: string;
  obj: Sla = new Sla;

  touched:boolean=false;

  progressForm:boolean;
  global = Global;
  sendStatus:boolean;
  slaItems:Sla[]=[];
  colorItems:any[]=[{Id:'red',Name:'قرمز'},{Id:'blue',Name:'آبی'},{Id:'yellow',Name:'زرد'},{Id:'green',Name:'سبز'},{Id:'pink',Name:'صورتی'}];
  @Output() oncomplete: EventEmitter<Sla> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();

  constructor(private slaService:SlaService,public configService: ConfigurationService,private element: ElementRef,private service: SlaService,private  notificationService:NotificationService) {
  }
  ngOnInit() {


    setTimeout(() => this.focus(), 100);

  }

  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  CreateModel(){
    this.focus();
    this.loadSla();
    this.sendStatus=false;
    this.touched = false;
    this.obj=new Sla();
  }
  loadSla(){
    this.slaService.getAll().subscribe(res=>{
      this.slaItems=res.Data
    })
  }
  Save(form){
    this.touched = true;
    this.sendStatus=false;
    if (form.valid) {
      this.progressForm=true;

      if(this.obj.Id){
        this.service.update(this.obj).subscribe(res=>{

          this.SendNoti('Success','پیام',' مقطع  با موفقیت ویرایش شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }
      else {
        this.service.insert(this.obj).subscribe(res=>{this.obj.Id=res.Data.Id;

          this.SendNoti('Success','پیام',' مقطع با موفقیت افزوده شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.sendStatus=true;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }



    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
