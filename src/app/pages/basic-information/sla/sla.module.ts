import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { IndexComponent } from './index/index.component';

import { EditComponent } from './edit/edit.component';

import {FormsModule} from '@angular/forms';
import {SigmaSoftModule} from '../../../lib/sigmasoft-ng';
import {CreateComponent} from './create/create.component';
import {SlaService} from '../../../services/sla.service';
import {SlaRoutingModule} from './sla-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    SlaRoutingModule
  ],
  providers:[SlaService],
  declarations: [IndexComponent,CreateComponent, EditComponent],

})
export class SlaModule { }
