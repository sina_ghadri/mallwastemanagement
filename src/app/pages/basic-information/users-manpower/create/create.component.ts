import {Component, ElementRef, EventEmitter, OnInit, Output} from '@angular/core';
import {User} from '../../../../models/user';
import {Global} from '../../../../lib/sigmasoft-ts/models';
import {UserService} from '../../../../services/user.service';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';


@Component({
  selector: 'user-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  obj: User = new User;
  progress: boolean;
  message: string;
  touched:boolean=false;
  progressForm:boolean;
  global = Global;
  sendStatus:boolean;

  @Output() oncomplete: EventEmitter<User> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();


  constructor(private element: ElementRef, public configService: ConfigurationService, private service: UserService, private  notificationService:NotificationService) { }

  ngOnInit() {
  }

  CreateModel(){
    this.focus()
    this.sendStatus=false;
    this.touched = false;
    this.obj=new User();
  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }


  Save(form){
    this.touched = true;
    this.sendStatus=false;
    if (form.valid) {
      this.progressForm=true;

      if(this.obj.Id){
        this.service.update(this.obj).subscribe(res=>{

          this.SendNoti('Success','پیام','کاربر با موفقیت ویرایش شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }
      else {
        this.service.insert(this.obj).subscribe(res=>{this.obj.Id=res.Data.Id;

          this.SendNoti('Success','پیام','کاربر با موفقیت افزوده شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.sendStatus=true;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }



    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}

