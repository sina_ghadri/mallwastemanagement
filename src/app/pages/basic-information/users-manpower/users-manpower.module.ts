import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersManpowerRoutingModule } from './users-manpower-routing.module';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { IndexComponent } from './index/index.component';
import { FormsModule } from '@angular/forms';
import { SigmaSoftModule } from '../../../lib/sigmasoft-ng';


@NgModule({
  imports: [
    CommonModule,
    UsersManpowerRoutingModule,
    FormsModule,
    SigmaSoftModule,
  ],
  declarations: [CreateComponent, EditComponent, IndexComponent]
})
export class UsersManpowerModule { }
