import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Place } from '../../../../models/place';
import { PlaceService } from '../../../../services/place.service';



@Component({
  selector: 'place-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Input() id: number;
  @Input() data: Place = new Place;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter;
  obj: Place = new Place;

  progress: boolean = false;
  message: string;
  title: string;
  map: string;
  mapFile: any;

  constructor(private service: PlaceService) { }

  ngOnInit() {
    this.obj = Object.assign({}, this.data);
    if (this.obj.Parent) {
      if (this.obj.Parent.Parent) {
        this.title = "زون"
      }
      else {
        this.title = "طبقه"
      }
    }
    else {
      this.title = "ساختمان"
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.obj = Object.assign({}, this.data);
    if (this.obj.Parent) {
      if (this.obj.Parent.Parent) {
        this.title = "زون"
      }
      else {
        this.title = "طبقه"
      }
    }
    else {
      this.title = "ساختمان"
    }
    // this.obj=this.obj;
    // if(this.id) this.service.getById(this.id).subscribe(data => this.obj = data);
  }
  submit(e: Event) {
    e.preventDefault();
    this.progress = true;
    this.service.update(this.obj).subscribe(op => {
      if (op) this.oncomplete.emit(op);
    }, e => this.progress = false, () => this.progress = false);
  }
  cancel() {
    this.oncomplete.emit(0);
  }
  mapChange(files) {
    if (files && files[0]) {
      this.readURL(this.mapFile = files[0]);
    }
  }
  readURL(file) {
    var reader = new FileReader();
    reader.onload = (e: any) => { this.map = e.target.result; }
    reader.readAsDataURL(file);
  }
  uploadImage()
  {

  }
}
