import { Component, OnInit } from '@angular/core';

import { TreeViewItem } from '../../../lib/sigmasoft-ng/data/treeview/models';
import { Place } from '../../../models/place';
import { PlaceService } from '../../../services/place.service';



enum Modes {
  Detail = 1,
  Edit = 2,
  Create = 3,
  MapEditor = 4
}
@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.css']
})
export class PlaceComponent implements OnInit {
  id: number;
  mode: Modes = 1;
  obj: Place = new Place();

  sla: TreeViewItem[] = [];
  places: TreeViewItem[] = [];

  constructor(private service: PlaceService) { }

  ngOnInit() {
    this.loadplaces();
  }
  createDisable() {
    if (this.obj.Parent) {
      if (this.obj.Parent.Parent) {
        return true
      }
      else {
        return false
      }
    }
    else {
      return false
    }

  }

  loadplaces() {
    this.service.getAll().subscribe(data => {
      for (let i = 0; i < data.Data.length; i++) {
        const item = data.Data[i];
        item.Parent = data.Data.find(x => x.Id == item.ParentId);
      }
      this.places = [];
      this.sla = this.loadPlaceItems(data.Data, this.places, this.sla);
    });
  }
  loadPlaceItems(data: Place[], nodes: TreeViewItem[], sla: TreeViewItem[], parent?: TreeViewItem): TreeViewItem[] {
    let items = data.filter(x => (parent && x.ParentId == parent.id) || (!parent && !x.ParentId));
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      let node = new TreeViewItem();
      node.id = item.Id;
      node.icon = 'mdi mdi-lumx color-red';
      node.text = item.Name;
      node.value = item;
      let oldNode = sla.find(x => x.id == item.Id);
      if (oldNode) {
        node.isOpen = oldNode.isOpen;
        node.isSelected = oldNode.isSelected;
      }
      this.loadPlaceItems(data, node.items, sla, node);
      nodes.push(node);
      sla.push(node);
    }
    return sla;
  }
  selectedChange(model: TreeViewItem) {
    this.obj = model.value;
    this.id = model.id;
  }

  edit() {
    this.mode = Modes.Edit;
  }
  editComplete(result: Place) {
    if (result) this.id = result.Id;
    this.mode = Modes.Detail;
    this.loadplaces();
  }
  delete() {
    if (!confirm("آیا از حذف این ردیف اطمینان دارید ؟")) return;
    this.service.deleteById(this.id).subscribe(op => {
      if (op.Success) this.loadplaces();
      else alert(op.Message || 'خطا در انجام عملیات');
    })
  }
  create() {
    this.mode = Modes.Create;
  }
  createComplete(result: Place) {
    if (result) this.id = result.Id;
    this.mode = Modes.Detail;
    this.loadplaces();
  }
  mapComplete(result) {
    if (result) this.id = result.Id;
    this.loadplaces();
  }
  mapEditor()
  {
    this.mode = Modes.MapEditor;
  }
  mapEditorComplete(result) {
    this.mode = Modes.Detail;
  }
}
