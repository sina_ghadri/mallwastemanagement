import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaceRoutingModule } from './place-routing.module';
import { PlaceComponent } from './place.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { DetailsComponent } from './details/details.component';
import { DeleteComponent } from './delete/delete.component';
import { FormsModule } from '@angular/forms';
import { SigmaSoftModule } from '../../../lib/sigmasoft-ng';
import { UseTypeService } from '../../../services/useType.service';
import { MapComponent } from './map/map.component';
import { MapEditorComponent } from './map-editor/map-editor.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PlaceRoutingModule,
    SigmaSoftModule,

  ],
  exports:[MapEditorComponent],
  providers: [UseTypeService],
  declarations: [PlaceComponent, CreateComponent, EditComponent, DetailsComponent, DeleteComponent, MapComponent, MapEditorComponent]
})
export class PlaceModule { }
