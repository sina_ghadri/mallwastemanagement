import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {PlaceService} from '../../../../services/place.service';
import {Place} from '../../../../models/place';



@Component({
  selector: 'place-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  @Input() data: Place = new Place;
  obj: Place = new Place;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter;


  progress: boolean = false;
  message: string;
  title: string;
  constructor(private service: PlaceService) { }
  createDisable(){
    if(this.data.Parent)
    {
      if(this.data.Parent.Parent)
      {
        return true
      }
      else {
        return false
      }
    }
    else {
      return false
    }

  }
  ngOnInit() {

    this.obj.ParentId=Object.assign({},this.data).Id;

    if (this.data.ParentId){
      this.title="زون"
    }
    else {
      this.title="طبقه"
    }
  }
  ngOnChanges(changes: SimpleChanges): void {

    this.obj.ParentId=Object.assign({},this.data).Id;
    if (this.data.ParentId){
      this.title="زون"
    }
    else {
      this.title="طبقه"
    }
    // this.obj=this.obj;
    // if(this.id) this.service.getById(this.id).subscribe(data => this.obj = data);
  }

  submit(e: Event) {

    e.preventDefault();
    this.progress = true;
    this.obj.ParentId=this.data.Id;
    this.service.insert(this.obj).subscribe(op => {
      if(op) this.oncomplete.emit(op.Data);
    }, e => this.progress = false, () => this.progress = false);
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
