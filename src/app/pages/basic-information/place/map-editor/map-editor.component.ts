import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  SimpleChanges,
  ElementRef,
  HostBinding
} from "@angular/core";
import { Place } from "../../../../models/place";
import { PlaceService } from "../../../../services/place.service";
import { ConfigurationService, Modal } from "../../../../lib/sigmasoft-ng";
import { Cordination } from "../../../../models/cordination";
import { Global } from "../../../../lib/sigmasoft-ts";
import { HotkeysService, Hotkey } from "angular2-hotkeys";
import { Station } from "src/app/models/station";
import { StationService } from "src/app/services/station.service";
import { ODataPagination } from "src/app/models/oDataPagination";

class Point {
  public x: number;
  public y: number;

  reset() {
    this.x = 0;
    this.y = 0;
  }
}
enum Tools {
  Cursor = 0,
  MakeBox = 1,
  Station = 2,
  Eraser = 3,
  Resize = 4,
  Move = 5,
  ZoomIn = 6,
  ZoomOut = 7
}

enum PlaceChooseMethod {
  Choose = 1,
  New = 2
}

@Component({
  selector: "place-map-editor",
  templateUrl: "./map-editor.component.html",
  styleUrls: ["./map-editor.component.scss"]
})
export class MapEditorComponent implements OnInit {
  @Input() data: Place = new Place();
  @Input() justShow: boolean = false;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter();
  obj: Place = new Place();
  global: any = Global;
  progress: boolean;
  map: string;
  mapBox: HTMLElement;
  selectedTools: Tools = Tools.Cursor;
  selectedBox: HTMLElement;
  dragging: boolean = false;

  places: Place[] = [];
  selectablePlaces: Place[] = [];
  showablePlaces: Place[] = [];

  stations: Station[];
  selectableStations: Station[] = [];
  showableStations: Station[] = [];
  queue: Place[] = [];

  childPlace: Place;
  station: Station;

  startPoint: Point = new Point();
  endPoint: Point = new Point();
  cordination: Cordination = new Cordination();

  placeModal: Modal = new Modal();
  stationModal: Modal = new Modal();

  choosePlaceMethod: PlaceChooseMethod = PlaceChooseMethod.Choose;
  zoom: number = 1;

  constructor(
    private service: PlaceService,
    private element: ElementRef,
    private hotkeysService: HotkeysService,
    private stationService: StationService,
    private configurationService: ConfigurationService
  ) {}

  ngOnInit() {
    this.obj = Object.assign({}, this.data);

    this.mapBox = this.element.nativeElement.querySelector(".map");
    this.selectedBox = this.element.nativeElement.querySelector(".select-box");

    this.hotkeysService.add(
      new Hotkey("v", (e: KeyboardEvent): boolean => {
        this.selectedTools = Tools.Cursor;
        return false;
      })
    );
    this.hotkeysService.add(
      new Hotkey("b", (e: KeyboardEvent): boolean => {
        this.selectedTools = Tools.MakeBox;
        return false;
      })
    );
    this.hotkeysService.add(
      new Hotkey("s", (e: KeyboardEvent): boolean => {
        this.selectedTools = Tools.Station;
        return false;
      })
    );
    this.hotkeysService.add(
      new Hotkey("e", (e: KeyboardEvent): boolean => {
        this.selectedTools = Tools.Eraser;
        return false;
      })
    );
    this.hotkeysService.add(
      new Hotkey("r", (e: KeyboardEvent): boolean => {
        this.selectedTools = Tools.Resize;
        return false;
      })
    );
    this.hotkeysService.add(
      new Hotkey("m", (e: KeyboardEvent): boolean => {
        this.selectedTools = Tools.Move;
        return false;
      })
    );
    this.hotkeysService.add(
      new Hotkey("+", (e: KeyboardEvent): boolean => {
        this.zoomIn();
        return false;
      })
    );
    this.hotkeysService.add(
      new Hotkey("-", (e: KeyboardEvent): boolean => {
        this.zoomOut();
        return false;
      })
    );

    this.loadMap();
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.obj = Object.assign({}, this.data);
    this.loadMap();
    this.loadStations();
  }
  loadMap() {
    this.map =
      this.configurationService.configuration.upload.maps +
      this.obj.Id +
      ".jpg?" +
      new Date().getDate();
    this.loadPlaces();
  }
  loadPlaces() {
    this.service.getChildren(this.obj.Id).subscribe(op => {
      this.places = op.Data;
      this.selectablePlaces = this.places.filter(x => !x.Cordination);
      this.showablePlaces = this.places.filter(x => x.Cordination);
    });
  }
  loadStations() {
    this.stationService
      .getByPlaceId(new ODataPagination(), this.obj.Id)
      .subscribe(op => {
        this.stations = op.Data;
        this.selectableStations = this.stations.filter(x => !x.Cordination);
        this.showableStations = this.stations.filter(x => x.Cordination);
      });
  }
  submit(e: Event) {}

  createComplete(result: Place) {
    if (result) {
      this.childPlace = result;
      this.savePlace();
    }
  }
  //================================== Drawing
  mapOnClick(e: MouseEvent) {

    if (this.selectedTools == Tools.Station) {
      const point = this.getPoint(e);
      this.cordination.X = point.x;
      this.cordination.Y = point.y;
      this.stationModal.open();
    }
  }

  getPoint(e: MouseEvent) {
    let rect = this.mapBox.getBoundingClientRect();
    const x = e.clientX / this.zoom - rect.left + this.mapBox.scrollLeft;
    const y = e.clientY / this.zoom - rect.top + this.mapBox.scrollTop;
    return { x, y };
  }
  mapOnMouseDown(e: MouseEvent) {
    if (this.dragging) return this.mapOnMouseUp(e);
    if (this.selectedTools == Tools.MakeBox) {
      debugger
      let point = this.getPoint(e);
      this.setCordinationPosition(point.x, point.y);
    }
    this.dragging = true;
  }
  mapOnMouseUp(e: MouseEvent) {
    if (this.selectedTools == Tools.MakeBox) {
      let point = this.getPoint(e);
      this.setCordinationSize(point.x, point.y);
      this.renderCordination();
      this.selectedTools = Tools.Cursor;
      this.placeModal.open();
    }
    this.dragging = false;
  }
  mapOnMouseMove(e: MouseEvent) {
    if (this.selectedTools == Tools.Cursor) {
      if (this.dragging) {
        this.mapBox.scrollLeft += -e.movementX;
        this.mapBox.scrollTop += -e.movementY;
      }
    }
    if (this.selectedTools == Tools.MakeBox && this.dragging) {
      let point = this.getPoint(e);
      this.setCordinationSize(point.x, point.y);
      this.renderCordination();
    }
  }
  mapOnScroll(e: WheelEvent) {}
  setCordinationPosition(x: number, y: number) {
    this.startPoint.x = x;
    this.startPoint.y = y;
  }
  setCordinationSize(x: number, y: number) {
    this.endPoint.x = x;
    this.endPoint.y = y;
  }
  renderCordination() {
    this.cordination.X =
      this.startPoint.x < this.endPoint.x ? this.startPoint.x : this.endPoint.x;
    this.cordination.Y =
      this.startPoint.y < this.endPoint.y ? this.startPoint.y : this.endPoint.y;
    this.cordination.Width = Math.abs(this.startPoint.x - this.endPoint.x);
    this.cordination.Height = Math.abs(this.startPoint.y - this.endPoint.y);

    // this.cordination.scale(this.zoom);

    this.selectedBox.style.top = this.cordination.Y + "px";
    this.selectedBox.style.left = this.cordination.X + "px";
    this.selectedBox.style.width = this.cordination.Width + "px";
    this.selectedBox.style.height = this.cordination.Height + "px";
  }
  //================================== Zoom
  zoomIn() {
    if (this.zoom < 4) {
      this.zoom += 0.1;
    }
  }
  zoomOut() {
    if (this.zoom > 0.2) this.zoom -= 0.1;
  }
  //================================= Save Place
  savePlace() {
    this.progress = true;
    this.service
      .setCordination(this.childPlace.Id, this.cordination)
      .subscribe(op => {
        if (op.Success) {

          this.savePlaceDone();
        }
        this.progress=false;
      });
  }
  savePlaceDone() {
    this.loadPlaces();
    this.placeModal.close();
    this.savePlaceComplete();
  }
  savePlaceCancel() {
    this.savePlaceComplete();
  }
  savePlaceComplete() {
    this.childPlace = null;
    this.startPoint.reset();
    this.endPoint.reset();
    this.renderCordination();
  }
  //=============================== Place Operation
  placeOnClick(place: Place) {
    if (this.selectedTools == Tools.Eraser) {
      if (confirm(`آیا از حذف بخش ${place.Name} اطمینان دارید ؟`)) {
        this.service.removeCordination(place.Id).subscribe(op => {
          if (op.Success) {
            this.loadPlaces();
          }
        });
      }
    }
  }
  placeOnMouseDown(place: Place) {}
  placeOnMouseUp(place: Place) {}
  placeOnMouseMove(place: Place) {
    if (this.selectedTools == Tools.Move) {
      if (this.dragging) {
      }
    }
  }
  //====================================== Navigator
  goInside(place: Place) {
    if (this.justShow) {
      this.oncomplete.emit({ placeId: place.Id });
    } else {
      this.queue.push(this.obj);
      this.obj = place;
      this.loadMap();
    }
  }
  goOutside() {
    if (this.queue.length > 0) {
      this.obj = this.queue.pop();
      this.loadMap();
    } else {
      this.oncomplete.emit(0);
    }
  }
  //======================================= Station
  stationOnClick(station: Station) {
    if (this.selectedTools == Tools.Eraser) {
      if (confirm(`آیا از حذف ایستگاه ${station.Name} اطمینان دارید ؟`)) {
        this.stationService.removeCordination(station.Id).subscribe(op => {
          if (op.Success) {
            this.loadStations();
          }
        });
      }
    }
  }
  saveStation() {
    this.stationService
      .setCordination(this.station.Id, {
        x: this.cordination.X,
        y: this.cordination.Y
      })
      .subscribe(op => {
        if (op.Success) {

          this.stationModal.close();
          this.loadStations();
        }
        this.progress=false;
      });
  }
  saveStationCancel(){
    this.childPlace = null;
    this.startPoint.reset();
    this.endPoint.reset();
    this.renderCordination();
  }
}
