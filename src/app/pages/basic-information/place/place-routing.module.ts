import { NgModule } from '@angular/core';
import {Routes, RouterModule, Route} from '@angular/router';
import {PlaceComponent} from './place.component';


const routes: Route[] = [
  { path: 'All', component:PlaceComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaceRoutingModule { }
