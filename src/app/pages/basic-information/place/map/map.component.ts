import { Component, EventEmitter, Input, OnInit, Output, SimpleChanges } from '@angular/core';
import { Place } from '../../../../models/place';
import { PlaceService } from '../../../../services/place.service';
import { ConfigurationService } from 'src/app/lib/sigmasoft-ng';



@Component({
  selector: 'place-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss']
})
export class MapComponent implements OnInit {
  @Input() id: number;
  @Input() data: Place = new Place;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter;
  obj: Place = new Place;

  progress: boolean = false;
  loaded: boolean = false;
  message: string;
  title: string;
  map: string;
  mapFile: any;
  uploadProgress: string;

  constructor(private service: PlaceService, private configurationService: ConfigurationService) { }

  ngOnInit() {
    this.obj = Object.assign({}, this.data);
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.obj = Object.assign({}, this.data);
    if(changes.id)
    {
      this.map = null;
      this.mapFile = null;
      this.uploadProgress = '0';

      if(this.obj.HasImage) this.map = this.configurationService.configuration.upload.maps + this.id + '.jpg?' + new Date().getDate();
    }
  }
  submit(e: Event) {
    e.preventDefault();
    this.progress = true;
    this.service.uploadImage(this.id, this.mapFile, this.onProgress.bind(this)).subscribe(op => {
      if (op) this.oncomplete.emit(op);
    }, e => this.progress = false, () => this.progress = false);
  }
  cancel() {
    this.oncomplete.emit(0);
  }
  onProgress(done: number, total: number, progress: number) {
    this.uploadProgress = progress + '%';
  }
  mapChange(files) {
    if (files && files[0]) {
      this.readURL(this.mapFile = files[0]);
    }
  }
  readURL(file) {
    var reader = new FileReader();
    reader.onload = (e: any) => { this.map = e.target.result; }
    reader.readAsDataURL(file);
  }
}
