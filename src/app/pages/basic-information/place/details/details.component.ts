import {Component, Input, OnInit, SimpleChanges} from '@angular/core';
import {Place} from '../../../../models/place';
import {PlaceService} from '../../../../services/place.service';


@Component({
  selector: 'place-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  @Input() id: number;
  @Input() data: Place = new Place;
  obj: Place = new Place;
  message: string;
  title: string;
  constructor(private service: PlaceService) { }

  ngOnInit() {
    this.obj=Object.assign({},this.data);
    if (this.obj.Parent){
      if (this.obj.Parent.Parent){
        this.title="زون"
      }
      else {
        this.title="طبقه"
      }
    }
    else {
      this.title="ساختمان"
    }
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.obj=Object.assign({},this.data);
    if (this.obj.Parent){
      if (this.obj.Parent.Parent){
        this.title="زون"
      }
      else {
        this.title="طبقه"
      }
    }
    else {
      this.title="ساختمان"
    }
    // this.obj=this.obj;
    // if(this.id) this.service.getById(this.id).subscribe(data => this.obj = data);
  }

}
