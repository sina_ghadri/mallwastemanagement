import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UseTypeComponent } from './use-type.component';

describe('UseTypeComponent', () => {
  let component: UseTypeComponent;
  let fixture: ComponentFixture<UseTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UseTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UseTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
