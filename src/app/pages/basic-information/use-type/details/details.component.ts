import {Component, Input, OnInit, SimpleChanges} from '@angular/core';
import {UseType} from '../../../../models/useType';
import {UseTypeService} from '../../../../services/useType.service';


@Component({
  selector: 'use-type-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  @Input() id: number;
  @Input() data: UseType = new UseType;
  obj: UseType = new UseType;
  message: string;

  constructor(private service: UseTypeService) { }

  ngOnInit() {
    this.obj=Object.assign({},this.data);
  }
  ngOnChanges(changes: SimpleChanges): void {

    this.obj=Object.assign({},this.data);
    // this.obj=this.obj;
    // if(this.id) this.service.getById(this.id).subscribe(data => this.obj = data);
  }

}
