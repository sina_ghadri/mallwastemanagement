import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {UseType} from '../../../../models/useType';
import {UseTypeService} from '../../../../services/useType.service';


@Component({
  selector: 'use-type-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  @Input() data: UseType = new UseType;
  obj: UseType = new UseType;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter;


  progress: boolean = false;
  message: string;

  constructor(private service: UseTypeService) { }

  ngOnInit() {

    this.obj.ParentId=Object.assign({},this.data).Id;
  }
  ngOnChanges(changes: SimpleChanges): void {

    this.obj.ParentId=Object.assign({},this.data).Id;
    // this.obj=this.obj;
    // if(this.id) this.service.getById(this.id).subscribe(data => this.obj = data);
  }

  submit(e: Event) {

    e.preventDefault();
    this.progress = true;

    this.obj.ParentId=this.data.Id;

    this.service.insert(this.obj).subscribe(op => {
      if(op) this.oncomplete.emit(op);
    }, e => this.progress = false, () => this.progress = false);
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
