import { NgModule } from '@angular/core';
import {Routes, RouterModule, Route} from '@angular/router';
import {UseTypeComponent} from './use-type.component';


const routes: Route[] = [
  { path: '', component:UseTypeComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UseTypeRoutingModule { }
