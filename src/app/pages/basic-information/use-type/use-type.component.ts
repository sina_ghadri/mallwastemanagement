import {Component, OnInit, OnChanges, Input, SimpleChanges, Injectable, ViewChild} from '@angular/core';
import { isNil, remove, reverse } from 'lodash';
import {TreeViewItem} from '../../../lib/sigmasoft-ng/data/treeview/models';

import { UseType } from '../../../models/useType';
import { UseTypeService } from '../../../services/useType.service';
import { Index } from '../../../models';
import { IndexService } from '../../../services/index.service';


import {
  DownlineTreeviewItem,
  DropdownTreeviewComponent,
  OrderDownlineTreeviewEventParser,
  TreeviewComponent,
  TreeviewConfig,
  TreeviewEventParser,
  TreeviewItem
} from '../../../components/tree';
import {DropdownTreeviewSelectI18n} from '../../../components/tree/file/dropdown-treeview-select-i18n';
import {arrayToTree} from '../../../models/arrayToTree';
enum Modes {
  Detail = 1,
  Edit = 2,
  Create = 3,
}

@Injectable()
export class ProductTreeviewConfig extends TreeviewConfig {
  hasAllCheckBox = false;
  hasFilter = true;
  hasCollapseExpand = true;
  maxHeight = 400;
}
@Component({
  selector: 'app-use-type',
  templateUrl: './use-type.component.html',
  styleUrls: ['./use-type.component.css'],
  providers:[
    { provide: TreeviewEventParser, useClass: OrderDownlineTreeviewEventParser },
    { provide: TreeviewConfig, useClass: ProductTreeviewConfig }
  ]
})
export class UseTypeComponent implements OnInit {

  id: number;
  mode: Modes = 1;
  obj: UseType = new UseType();

  items: TreeviewItem[]=[];

  sla: TreeViewItem[] = [];
  useTypes: TreeViewItem[] = [];
  indexes: Index[] = [];


  @ViewChild(TreeviewComponent) treeviewComponent: TreeviewComponent;
  @ViewChild(DropdownTreeviewComponent) dropdownTreeviewComponent: DropdownTreeviewComponent;
  private dropdownTreeviewSelectI18n: DropdownTreeviewSelectI18n;
  constructor(private service: UseTypeService, private indexService: IndexService) { }

  ngOnInit() {
    this.loaduseTypes();
    this.indexService.getAll().subscribe(op => {
      let indexes = op.Data;
      let parents = indexes.filter(x => !x.ParentId);
      for (let i = 0; i < parents.length; i++) {
        const item = parents[i];
        parents[i].Children = indexes.filter(x => x.ParentId == item.Id);
      }
      this.indexes = parents;

      let changedData = arrayToTree(op.Data as any[], { id: 'Id', parentId: 'ParentId' });

      this.items=[];
      for(const item of changedData )
      {
        item.collapsed=false;
        for(const child of item.children )
        {
          child.collapsed=false;
        }
        this.items.push(new TreeviewItem(item))

      }
    });
  }
  loadUseTypeIndexes() {
    this.service.getIndexes(this.id).subscribe(op => {
      this.obj.indexes = op.Data;


    });
  }
  hasIndex(indexId: number): boolean {
    if(!this.obj.indexes) return false;
    return !!this.obj.indexes.find(x => x.IndexId == indexId);
  }
  changeIndex(indexId: number,item:Index) {
    debugger

    let ui = this.obj.indexes.find(x => x.IndexId == indexId);

    if(item.Children)
    {
    let GB=item.Children;
      for(const item of  GB){

        if(ui) {
          let uiui = this.obj.indexes.find(x => x.IndexId == item.Id);

          if(uiui) {
            this.removeIndex(item.Id);
          }
        } else {

          let uiuiui = this.obj.indexes.find(x => x.IndexId == item.Id);

          if(uiuiui) {

          }else {
            this.addIndex(item.Id)
          }

        }
      }
    }


    if(ui) {
      this.removeIndex(indexId);
    } else {
      this.addIndex(indexId);
    }

  }
  addIndex(indexId: number) {
    this.service.addIndexe(this.id, indexId).subscribe(op => {
      this.loadUseTypeIndexes();
    });
  }
  removeIndex(indexId: number) {
    this.service.removeIndexes(this.id, indexId).subscribe(op => {
      this.loadUseTypeIndexes();
    });
  }

  loaduseTypes() {
    this.service.getAll().subscribe(data => {
      for (let i = 0; i < data.Count; i++) {
        const item = data.Data[i];
        item.Parent = data.Data.find(x => x.Id == item.ParentId);
      }
      this.useTypes = [];
      this.sla = this.loadUseTypeItems(data.Data, this.useTypes, this.sla);
    });
  }
  loadUseTypeItems(data: UseType[], nodes: TreeViewItem[], sla: TreeViewItem[], parent?: TreeViewItem): TreeViewItem[] {
    let items = data.filter(x => (parent && x.ParentId == parent.id) || (!parent && !x.ParentId));
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      let node = new TreeViewItem();
      node.id = item.Id;
      node.icon = 'mdi mdi-lumx color-red';
      node.text = item.Name;
      node.value = item;
      let oldNode = sla.find(x => x.id == item.Id);
      if (oldNode) {
        node.isOpen = oldNode.isOpen;
        node.isSelected = oldNode.isSelected;
      }
      this.loadUseTypeItems(data, node.items, sla, node);
      nodes.push(node);
      sla.push(node);
    }
    return sla;
  }
  selectedChange(model: TreeViewItem) {
    this.obj = model.value;
    this.id = model.id;
    this.loadUseTypeIndexes();
  }

  edit() {
    this.mode = Modes.Edit;
  }
  editComplete(result: UseType) {
    debugger
    if (result) this.id = result.Id;
    this.mode = Modes.Detail;
    this.loaduseTypes();
  }
  delete() {
    debugger
    if (!confirm("آیا از حذف این ردیف اطمینان دارید ؟")) return;
    this.service.deleteById(this.id).subscribe(op => {
      if (op.Success) {

        this.mode = Modes.Detail;
        debugger
        this.id = null;
        this.loaduseTypes();
        this.obj = new UseType();
      }
      else alert(op.Message || 'خطا در انجام عملیات');
    })
  }
  create() {
    this.mode = Modes.Create;
  }
  createA() {
    this.obj = new UseType();
  }
  createComplete(result: UseType) {
    if (result) this.id = result.Id;
    this.mode = Modes.Detail;
    this.loaduseTypes();
  }



}
