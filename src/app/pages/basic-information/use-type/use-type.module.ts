import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UseTypeRoutingModule } from './use-type-routing.module';
import { UseTypeComponent } from './use-type.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { DetailsComponent } from './details/details.component';
import { DeleteComponent } from './delete/delete.component';
import {FormsModule} from '@angular/forms';
import {SigmaSoftModule} from '../../../lib/sigmasoft-ng';
import {UseTypeService} from '../../../services/useType.service';
import {I18n} from '../../../components/tree/file/i18n';
import {TreeviewModule} from '../../../components/tree';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UseTypeRoutingModule,
    TreeviewModule.forRoot(),
    SigmaSoftModule
  ],
  providers:[I18n,UseTypeService],
  declarations: [UseTypeComponent, CreateComponent, EditComponent, DetailsComponent, DeleteComponent]
})
export class UseTypeModule { }
