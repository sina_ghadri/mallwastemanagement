import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTableRow} from '../../../../lib/sigmasoft-ng/data/table/models';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';
import {PlaceService} from '../../../../services/place.service';
import {Place} from '../../../../models/place';


@Component({
  selector: 'building-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Input() data:DataTableRow;
  progress: boolean;
  message: string;
  obj: Place = new Place;
  touched:boolean=false;

  progressForm:boolean;
  @Output() oncomplete: EventEmitter<Place> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();

  constructor(private element: ElementRef,private service: PlaceService,private  notificationService:NotificationService) { }

  ngOnInit() {
    this.obj=Object.assign({},this.data.value);
    setTimeout(() => this.focus(), 100);
  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  Save(form){
    this.touched = true;
    if (form.valid) {
      this.progressForm=true;
      this.service.update(this.obj).subscribe(res=>{

        this.SendNoti('Success','پیام','ساختمان با موفقیت ویرایش شد.');
        this.oncomplete.emit(this.obj);
        this.touched = false;
        this.progressForm=false;
        // this.SendNoti('Primary');

        // this.SendNoti('Warning');
        // this.SendNoti('Error');
      },error => {
        this.progressForm=false;
      });


    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
