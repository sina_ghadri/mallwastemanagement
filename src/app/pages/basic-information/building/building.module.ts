import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuildingRoutingModule } from './building-routing.module';
import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import {FormsModule} from '@angular/forms';
import {SigmaSoftModule} from '../../../lib/sigmasoft-ng';
import {PlaceService} from '../../../services/place.service';

@NgModule({
  imports: [
    CommonModule,
    SigmaSoftModule,
    FormsModule,
    BuildingRoutingModule
  ],
  declarations: [IndexComponent, CreateComponent, EditComponent],
  providers:[PlaceService]
})
export class BuildingModule { }
