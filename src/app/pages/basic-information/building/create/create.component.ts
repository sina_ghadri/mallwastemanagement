import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {PlaceService} from '../../../../services/place.service';
import {Place} from '../../../../models/place';

@Component({
  selector: 'building-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

  progress: boolean;
  message: string;
  obj: Place = new Place;
  touched:boolean=false;
  progressForm:boolean;
  @Output() oncomplete: EventEmitter<Place> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();
  sendStatus:boolean;
  constructor(public configService: ConfigurationService,private element: ElementRef,private service: PlaceService,private  notificationService:NotificationService) {
  }

  ngOnInit() {
    if(this.configService.checkPermission('ROLE_PlacesAdd'))
    {
      setTimeout(() => this.focus(), 100);
    }
    else {
      this.onShowList.emit()
    }


  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  CreateModel(){
    this.focus()
    this.sendStatus=false;
    this.touched = false;
    this.obj=new Place();
  }

  Save(form){
    this.touched = true;
    this.sendStatus=false;
    if (form.valid) {
      this.progressForm=true;

      if(this.obj.Id){
        this.service.update(this.obj).subscribe(res=>{

          this.SendNoti('Success','پیام','ساختمان با موفقیت ویرایش شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }
      else {
        this.service.insert(this.obj).subscribe(res=>{this.obj.Id=res.Data.Id;

          this.SendNoti('Success','پیام','ساختمان با موفقیت افزوده شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.sendStatus=true;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }



    }
  }

  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
