import {Component, Input, OnInit, SimpleChanges} from '@angular/core';
import {Index} from '../../../../models';
import {IndexService} from '../../../../services/index.service';


@Component({
  selector: 'index-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  @Input() id: number;
  @Input() data: Index = new Index;
  obj: Index = new Index;
  message: string;

  constructor(private service: IndexService) { }

  ngOnInit() {
    this.obj=Object.assign({},this.data);
  }
  ngOnChanges(changes: SimpleChanges): void {

    this.obj=Object.assign({},this.data);
    // this.obj=this.obj;
    // if(this.id) this.service.getById(this.id).subscribe(data => this.obj = data);
  }

}
