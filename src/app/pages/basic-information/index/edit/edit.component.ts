import {Component, EventEmitter, Input, OnInit, Output, SimpleChanges} from '@angular/core';
import {Index} from '../../../../models';
import {IndexService} from '../../../../services/index.service';


@Component({
  selector: 'index-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Input() id: number;
  @Input() data: Index = new Index;
  @Output() oncomplete: EventEmitter<any> = new EventEmitter;
  obj: Index = new Index;

  progress: boolean = false;
  message: string;

  constructor(private service: IndexService) { }

  ngOnInit() {
    this.obj=Object.assign({},this.data);
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.obj=Object.assign({},this.data);
    // this.obj=this.obj;
    // if(this.id) this.service.getById(this.id).subscribe(data => this.obj = data);
  }
  submit(e: Event) {
    e.preventDefault();
    this.progress = true;
    this.service.update(this.obj).subscribe(op => {
      if(op) this.oncomplete.emit(op);
    }, e => this.progress = false, () => this.progress = false);
  }
  cancel() {
    this.oncomplete.emit(0);
  }
}
