import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IndexRoutingModule } from './index-routing.module';
import { IndexComponent } from './index.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { DetailsComponent } from './details/details.component';
import { DeleteComponent } from './delete/delete.component';
import {FormsModule} from '@angular/forms';
import {SigmaSoftModule} from '../../../lib/sigmasoft-ng';
import {IndexService} from '../../../services/index.service';
import {TreeviewModule} from '../../../components/tree';
import {I18n} from '../../../components/tree/file/i18n';
import {UseTypeService} from '../../../services/useType.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IndexRoutingModule,
    TreeviewModule.forRoot(),
    SigmaSoftModule
  ],
  providers:[I18n,IndexService,UseTypeService],
  declarations: [IndexComponent, CreateComponent, EditComponent, DetailsComponent, DeleteComponent]
})
export class IndexModule { }
