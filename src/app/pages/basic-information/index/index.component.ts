import {Component, Injectable, OnInit, ViewChild} from '@angular/core';
import { isNil, remove, reverse } from 'lodash';
import {TreeViewItem} from '../../../lib/sigmasoft-ng/data/treeview/models';
import {Index} from '../../../models';
import {IndexService} from '../../../services/index.service';
import {UseTypeService} from '../../../services/useType.service';
import {
  DownlineTreeviewItem,
  DropdownTreeviewComponent,
  OrderDownlineTreeviewEventParser,
  TreeviewComponent,
  TreeviewConfig,
  TreeviewEventParser,
  TreeviewItem
} from '../../../components/tree';
import {DropdownTreeviewSelectI18n} from '../../../components/tree/file/dropdown-treeview-select-i18n';
import {arrayToTree} from '../../../models/arrayToTree';





enum Modes {
  Detail = 1,
  Edit = 2,
  Create = 3,
}
@Injectable()
export class ProductTreeviewConfig extends TreeviewConfig {
  hasAllCheckBox = false;
  hasFilter = true;
  hasCollapseExpand = true;
  maxHeight = 400;
}
@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css'],
  providers:[
    { provide: TreeviewEventParser, useClass: OrderDownlineTreeviewEventParser },
    { provide: TreeviewConfig, useClass: ProductTreeviewConfig }
  ]
})
export class IndexComponent implements OnInit {
  id: number;
  mode: Modes = 1;
  obj: Index=new Index();

  sla: TreeViewItem[] = [];
  Indexes: TreeViewItem[] = [];



  items: TreeviewItem[]=[];
  rows: any[];
  @ViewChild(TreeviewComponent) treeviewComponent: TreeviewComponent;
  @ViewChild(DropdownTreeviewComponent) dropdownTreeviewComponent: DropdownTreeviewComponent;
  private dropdownTreeviewSelectI18n: DropdownTreeviewSelectI18n;
  constructor(private service:IndexService,private useTypeService:UseTypeService) { }

  ngOnInit() {
    this.loadIndexes();
  }


  loadIndexes() {
    this.service.getAll().subscribe(data => {
      for (let i = 0; i < data.Count; i++) {
        const item = data.Data[i];
        item.Parent = data.Data.find(x => x.Id == item.ParentId);
      }
      this.Indexes = [];
      this.sla = this.loadIndexItems(data.Data, this.Indexes, this.sla);
    });
  }
  loadIndexItems(data: Index[], nodes: TreeViewItem[], sla: TreeViewItem[], parent?: TreeViewItem): TreeViewItem[] {
    let items = data.filter(x => (parent && x.ParentId == parent.id) || (!parent && !x.ParentId));
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      let node = new TreeViewItem();
      node.id = item.Id;
      node.icon = 'mdi mdi-lumx color-red';
      node.text = item.Name ;
      node.value=item;
      let oldNode = sla.find(x => x.id == item.Id);
      if (oldNode) {
        node.isOpen = oldNode.isOpen;
        node.isSelected = oldNode.isSelected;
      }
      this.loadIndexItems(data, node.items, sla, node);
      nodes.push(node);
      sla.push(node);
    }
    return sla;
  }
  selectedChange(model:TreeViewItem) {
    this.obj=model.value;
    this.id = model.id;
    this.loadUseType();
  }

  edit() {
    this.mode = Modes.Edit;
  }
  editComplete(result: Index) {
    if (result) this.id = result.Id;
    this.mode = Modes.Detail;
    this.loadIndexes();
  }
  delete() {

    if (!confirm("آیا از حذف این ردیف اطمینان دارید ؟")) return;
    this.service.deleteById(this.id).subscribe(op => {
      if (op.Success) {

        this.mode = Modes.Detail;

        this.id=null;
        this.loadIndexes();
        this.obj=new Index();}
      else alert(op.Message || 'خطا در انجام عملیات');
    })
  }
  create() {
    this.mode = Modes.Create;
  }
  createA()
  {
    this.obj=new Index();
  }
  createComplete(result: Index) {
    if (result) this.id = result.Id;
    this.mode = Modes.Detail;
    this.loadIndexes();
  }

loadUseType(){
    this.useTypeService.getByIndexId(this.obj.Id).subscribe(res=>{
      let changedData = arrayToTree(res.Data as any[], { id: 'Id', parentId: 'ParentId' });

      this.items=[];
      for(const item of changedData )
      {
        item.collapsed=false;
        for(const child of item.children )
        {
          child.collapsed=false;
        }
        this.items.push(new TreeviewItem(item))

      }
    })
}


  onSelectedChange(downlineItems: DownlineTreeviewItem[]) {
    this.rows = [];
    downlineItems.forEach(downlineItem => {
      const item = downlineItem.item;
      const value = item.value;
      const texts = [item.text];
      let parent = downlineItem.parent;
      while (!isNil(parent)) {
        texts.push(parent.item.text);
        parent = parent.parent;
      }

      const row = value;
      this.rows.push(row);
    });
  }
  select(item: TreeviewItem) {
    if (item.children === undefined) {
      this.selectItem(item);
    }
  }
  private selectItem(item: TreeviewItem) {
    if (this.dropdownTreeviewSelectI18n.selectedItem !== item) {
      this.dropdownTreeviewSelectI18n.selectedItem = item;
    }
  }
  changeselect(itemG){
    for(const item of this.items){

      for(const internalChildren of item.internalChildren)
      {
        internalChildren.checked=false;
      }
    }
  }

}
