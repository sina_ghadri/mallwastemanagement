import { NgModule } from '@angular/core';
import {Routes, RouterModule, Route} from '@angular/router';
import {IndexComponent} from './index.component';


const routes: Route[] = [
  { path: '', component:IndexComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IndexRoutingModule { }
