import { BasicInformationModule } from './basic-information.module';

describe('BasicInformationModule', () => {
  let basicInformationModule: BasicInformationModule;

  beforeEach(() => {
    basicInformationModule = new BasicInformationModule();
  });

  it('should create an instance', () => {
    expect(basicInformationModule).toBeTruthy();
  });
});
