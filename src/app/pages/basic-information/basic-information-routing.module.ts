import { NgModule } from '@angular/core';
import {Route, RouterModule} from '@angular/router';
import {IndexModule} from './index/index.module';



const routes: Route[] = [
  { path: 'Building', loadChildren: './building/building.module#BuildingModule' },
  { path: 'Floors-View', loadChildren: './place/place.module#PlaceModule' },
  { path: 'Station',  loadChildren: './station/station.module#StationModule'},
  { path: 'Container', loadChildren: './container/container.module#ContainerModule' },
  { path: 'Container-Type', loadChildren: './container-type/container-type.module#ContainerTypeModule' },
  { path: 'Use-Type', loadChildren: './use-type/use-type.module#UseTypeModule' },
  { path: 'Day-Type',  loadChildren: './day-type/day-type.module#DayTypeModule'},
  { path: 'Users-Manpower', loadChildren: './users-manpower/users-manpower.module#UsersManpowerModule' },
  { path: 'Waste-Type', loadChildren: './waste-type/waste-type.module#WasteTypeModule' },
  { path: 'Map', loadChildren: './floors-view/floors-view.module#FloorsViewModule' },
  { path: 'Index', loadChildren: './index/index.module#IndexModule' },
  { path: 'Sla', loadChildren: './sla/sla.module#SlaModule' },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasicInformationRoutingModule { }
