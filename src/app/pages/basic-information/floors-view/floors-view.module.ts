import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FloorsViewRoutingModule } from './floors-view-routing.module';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import { IndexComponent } from './index/index.component';
import { MapComponent } from './map/map.component';

import { CloudinaryModule } from '@cloudinary/angular-5.x';

import * as  Cloudinary from 'cloudinary-core';
@NgModule({
  imports: [
    CommonModule,
    CloudinaryModule.forRoot(Cloudinary, { cloud_name: 'mycloudname'}),
    FloorsViewRoutingModule
  ],
  declarations: [CreateComponent, EditComponent, IndexComponent, MapComponent]
})
export class FloorsViewModule { }
