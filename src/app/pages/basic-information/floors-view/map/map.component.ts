import { Component, OnInit } from '@angular/core';
import { Cloudinary } from '@cloudinary/angular-5.x';
@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {

  constructor(private cloudinary: Cloudinary) {
    // <img src="https://res.cloudinary.com/demo/image/upload/dog.jpg" />
    console.log(cloudinary.cloudinaryInstance.image('dog'));
  }

  ngOnInit() {
  }

}
