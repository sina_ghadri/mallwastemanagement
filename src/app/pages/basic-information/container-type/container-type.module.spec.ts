import { ContainerTypeModule } from './container-type.module';

describe('ContainerTypeModule', () => {
  let containerTypeModule: ContainerTypeModule;

  beforeEach(() => {
    containerTypeModule = new ContainerTypeModule();
  });

  it('should create an instance', () => {
    expect(containerTypeModule).toBeTruthy();
  });
});
