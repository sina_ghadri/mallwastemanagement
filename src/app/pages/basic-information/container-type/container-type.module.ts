import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ContainerTypeRoutingModule } from './container-type-routing.module';
import { IndexComponent } from './index/index.component';
import { CreateComponent } from './create/create.component';
import { EditComponent } from './edit/edit.component';
import {FormsModule} from '@angular/forms';
import {SigmaSoftModule} from '../../../lib/sigmasoft-ng';
import {ContainerTypeService} from '../../../services/containerType.service';
import {NgxTreeSelectModule} from '../../../components/tree-select';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SigmaSoftModule,
    NgxTreeSelectModule,
    ContainerTypeRoutingModule
  ],
  providers:[ContainerTypeService],
  declarations: [IndexComponent, CreateComponent, EditComponent]
})
export class ContainerTypeModule { }
