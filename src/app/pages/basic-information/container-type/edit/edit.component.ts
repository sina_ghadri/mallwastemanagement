import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {DataTableRow} from '../../../../lib/sigmasoft-ng/data/table/models';
import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';

import {Global} from '../../../../lib/sigmasoft-ts/models';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {WasteTypeService} from '../../../../services/wasteType.service';
import {ContainerType} from '../../../../models/ContainerType';
import {ContainerTypeService} from '../../../../services/containerType.service';
import {WasteType} from '../../../../models/wasteType';
import {arrayToTree, TreeItemModel} from '../../../../models/arrayToTree';




@Component({
  selector: 'container-type-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  public reasontextField='text';
  public AllowParentSelection = false;
  public ShowFilter = true;
  public Disabled = false;
  public FilterPlaceholder = 'جستجو کنید...';
  public MaxDisplayed = 2;
  public wasteobj:TreeItemModel=new TreeItemModel();


  @Input() data:DataTableRow;
  progress: boolean;
  message: string;
  obj: ContainerType = new ContainerType;

  touched:boolean=false;
  wasteTypeItems : any[] = [];
  progressForm:boolean;
  global = Global;
  sendStatus:boolean;
  colorItems:any[]=[{Id:'red',Name:'قرمز'},{Id:'blue',Name:'آبی'},{Id:'yellow',Name:'زرد'},{Id:'green',Name:'سبز'},{Id:'pink',Name:'صورتی'}];
  @Output() oncomplete: EventEmitter<ContainerType> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();


  constructor(private wasteTypeService : WasteTypeService,public configService: ConfigurationService,private element: ElementRef,private service: ContainerTypeService,private  notificationService:NotificationService) {
  }
  ngOnInit() {
    this.obj=Object.assign({},this.data.value);
    this.wasteobj.value=this.obj.WasteType;
    this.wasteobj.id=this.wasteobj.value.Id;
    this.wasteobj.text=this.wasteobj.value.Name;
    this.wasteobj.checked=null;
    this.wasteobj.children=[];
    setTimeout(() => this.focus(), 100);
    this.getWasteType();
  }
  getWasteType() {
    this.wasteTypeService.getAll().subscribe(res=>{
      let changedData = arrayToTree(res.Data as any[], { id: 'Id', parentId: 'ParentId' });
      this.wasteTypeItems=changedData
    })
  }
  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  Save(form){
    this.touched = true;
    if (form.valid) {
      this.progressForm=true;
      this.service.update(this.obj).subscribe(res=>{

        this.SendNoti('Success','پیام','نوع ظرف با موفقیت ویرایش شد.');
        this.oncomplete.emit(this.obj);
        this.touched = false;
        this.progressForm=false;
        // this.SendNoti('Primary');

        // this.SendNoti('Warning');
        // this.SendNoti('Error');
      },error => {
        this.progressForm=false;
      });


    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
