import {Component, ElementRef, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {NotificationService} from '../../../../lib/sigmasoft-ng/message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../../../lib/sigmasoft-ng/message/notification/models';

import {Global} from '../../../../lib/sigmasoft-ts/models';
import {ConfigurationService} from '../../../../lib/sigmasoft-ng/misc';
import {ContainerType} from '../../../../models/ContainerType';
import {ContainerTypeService} from '../../../../services/containerType.service';
import {WasteTypeService} from '../../../../services/wasteType.service';
import {WasteType} from '../../../../models/wasteType';
import {arrayToTree} from '../../../../models/arrayToTree';
import {Station} from '../../../../models/station';





@Component({
  selector: 'container-type-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {
  public reasontextField='text';
  public AllowParentSelection = false;
  public ShowFilter = true;
  public Disabled = false;
  public FilterPlaceholder = 'جستجو کنید...';
  public MaxDisplayed = 2;
  public wasteobj:any;

  progress: boolean;
  message: string;
  obj: ContainerType = new ContainerType;

  touched:boolean=false;
  wasteTypeItems : any[] = [];
  progressForm:boolean;
  global = Global;
  sendStatus:boolean;
  colorItems:any[]=[{Id:'red',Name:'قرمز'},{Id:'blue',Name:'آبی'},{Id:'yellow',Name:'زرد'},{Id:'green',Name:'سبز'},{Id:'pink',Name:'صورتی'}];
  @Output() oncomplete: EventEmitter<ContainerType> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();

  constructor(private wasteTypeService : WasteTypeService,public configService: ConfigurationService,private element: ElementRef,private service: ContainerTypeService,private  notificationService:NotificationService) {
  }
  ngOnInit() {


    setTimeout(() => this.focus(), 100);
        this.getWasteType();
  }
  getWasteType() {
    this.wasteTypeService.getAll().subscribe(res=>{
      let changedData = arrayToTree(res.Data as any[], { id: 'Id', parentId: 'ParentId' });
      this.wasteTypeItems=changedData
    })
  }
  CreateModel(){
    this.focus()
    this.sendStatus=false;
    this.touched = false;
    this.obj=new ContainerType();
  }

  focus() { let input = this.element.nativeElement.querySelector('input'); if (input) input.focus();  }
  Save(form){
    this.touched = true;
    this.sendStatus=false;
    if (form.valid) {
      this.progressForm=true;

      if(this.obj.Id){
        this.service.update(this.obj).subscribe(res=>{

          this.SendNoti('Success','پیام','نوع ظرف با موفقیت ویرایش شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }
      else {
        this.service.insert(this.obj).subscribe(res=>{this.obj.Id=res.Data.Id;

          this.SendNoti('Success','پیام','نوع ظرف با موفقیت افزوده شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.sendStatus=true;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
        });
      }



    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }

}
