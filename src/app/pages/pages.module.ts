import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import {  HttpClientModule } from "@angular/common/http";

import { PagesRouting } from "./pages.routing";
import { PagesComponent} from './pages.component';
import { HttpConfigService } from "./../lib/sigmasoft-ng/misc/jwt/http-config.service";




import {SigmaSoftModule} from './../lib/sigmasoft-ng';
import {CssService} from './../services/css.service';
import {DataService} from './../services';
import {AccountComponent} from './account/account.component';
import {SettingsComponent} from './settings/settings.component';
import {UserService} from '../services/user.service';






@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    HttpClientModule,
    PagesRouting,
    SigmaSoftModule,



  ],
  declarations: [PagesComponent , AccountComponent, SettingsComponent],
  providers: [HttpConfigService,CssService,DataService,UserService],

})
export class PagesModule {}
