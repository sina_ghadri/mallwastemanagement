import { NgModule } from '@angular/core';
import {Routes, RouterModule, Route} from '@angular/router';
import {InspectionComponent} from './inspection.component';
import {PlaceComponent} from './place/place.component';
import {IndexesComponent} from './indexes/indexes.component';


const routes: Route[] = [
  { path: '', component:InspectionComponent},
  { path: 'Place', component:PlaceComponent},
  { path: 'Indexes', component:IndexesComponent},
];




@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InspectionRouting { }
