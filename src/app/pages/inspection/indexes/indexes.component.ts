import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {PlaceService} from '../../../services/place.service';
import {StationService} from '../../../services/station.service';
import {UseTypeService} from '../../../services/useType.service';
import {ODataPagination} from '../../../models/oDataPagination';
import {Global} from '../../../lib/sigmasoft-ts';
import {Place} from '../../../models/place';
import {Inspection} from '../../../models/inspection';

import {IndexService} from '../../../services/index.service';
import {UseType} from '../../../models/useType';
import {Station} from '../../../models/station';
import {ActivatedRoute} from '@angular/router';

import {InspectionService} from '../../../services/inspection.service';

import {NotificationItem, NotificationType} from '../../../lib/sigmasoft-ng/message/notification/models';
import {NotificationService} from '../../../lib/sigmasoft-ng/message/notification/notification.service';
import {arrayToTree} from '../../../models/arrayToTree';

@Component({
  selector: 'app-indexes',
  templateUrl: './indexes.component.html',
  styleUrls: ['./indexes.component.css']
})
export class IndexesComponent implements OnInit {

  public reasontextField='text';
  public AllowParentSelection = false;
  public ShowFilter = true;
  public Disabled = false;
  public FilterPlaceholder = 'جستجو کنید...';
  public MaxDisplayed = 2;
  public useTypeGet:any;

  oDataPagination:ODataPagination=new ODataPagination();
  global = Global;
  useTypeItems:any[]=[];
  stationItems:Station[]=[];
  useTypeObj:UseType=new UseType();
  stationObj:Station=new Station();
  obj:Inspection=new Inspection();
  sendStatus:boolean;
  @Output() oncomplete: EventEmitter<Inspection> = new EventEmitter();
  @Output() onShowList: EventEmitter<any> = new EventEmitter();
  placeId:number;
  InspectionId:number;
  touched:boolean=false;
  progressForm:boolean;
  _section: string;
  get section(): string { return this._section; }
  set section(value: string) { if(this._section == value) this._section = undefined; else this._section = value; }

  indexs:any[]=[];

  arrayChange:any[]=[];

  constructor(private placeService:PlaceService,private stationService:StationService,private useTypeService:UseTypeService,private indexService:IndexService,private service:InspectionService,private notificationService:NotificationService,private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
        this.placeId=params.placeId;
        this.InspectionId=params.InspectionId;
    });

if(this.placeId)
{
  this.getUseTypeData(this.oDataPagination);
}
    if(this.InspectionId)
    {
      this.getIndexDataByInspectionId(this.oDataPagination);
    }

  }
  getUseTypeData(oDataPagination:ODataPagination){
    this.useTypeItems=[];
    // oDataPagination.filter=`ParentId eq ${null}`;
    oDataPagination.filter=``;


    this.useTypeService.getWithPagination(oDataPagination).subscribe(res=>{

      let changedData = arrayToTree(res.Data as any[], { id: 'Id', parentId: 'ParentId' });
      this.useTypeItems=changedData

    })
  }
  getStationData(oDataPagination:ODataPagination,useTypeId){
    this.stationItems=[];

    // oDataPagination.filter=`PlaceId eq ${this.placeId} and  UseTypeId eq ${useTypeId}`;
    oDataPagination.filter=`PlaceId eq ${this.placeId}`;



    this.stationService.getByUseTypeId(oDataPagination,useTypeId).subscribe(res=>{
      this.stationItems=res.Data;
    })
  }
  getIndexData(oDataPagination:ODataPagination,useTypeId){
    this.indexs=[];

    // oDataPagination.filter=`PlaceId eq ${this.placeId} and  UseTypeId eq ${useTypeId}`;


    oDataPagination.filter=``;
    this.indexService.getByUseTypeId(oDataPagination,useTypeId).subscribe(res=>{
     this.arrayChange=[];
      let arrayParents=[];

      for(const item of res.Data){
        this.arrayChange.push(item.Index);

        if(arrayParents.length==0){

          let x=item.Index.Parent;
          x.Children=[];
          arrayParents.push(item.Index.Parent)
        }
        else {
          if(arrayParents.find(Pd=>Pd.Id==item.Index.ParentId)) {}
          else {
            let x=item.Index.Parent;
            x.Children=[];
            arrayParents.push(item.Index.Parent)
          }
        }

      }
      this.indexs=arrayParents;

      for(const indexP of  this.indexs)
      {
        for(const index  of    this.arrayChange)
        {
          if(index.ParentId==indexP.Id)
          {
            index.Grade=false;
            indexP.Children.push(index)
          }
        }
      }
      console.log(this.indexs)
    })
  }

  getIndexDataByInspectionId(oDataPagination:ODataPagination){
    this.indexs=[];

    // oDataPagination.filter=`PlaceId eq ${this.placeId} and  UseTypeId eq ${useTypeId}`;


    oDataPagination.filter=``;
    this.indexService.getByInspectionId(oDataPagination,this.InspectionId).subscribe(res=>{

      this.obj.Id= this.InspectionId
      this.obj.Station=res.Data[0].Inspection.Station;
      this.obj.StationId=res.Data[0].Inspection.StationId;
      this.obj.Time=res.Data[0].Inspection.Time;

      this.arrayChange=[];
      let arrayParents=[];

      for(const item of res.Data){
        item.Index.Grade= item.Grade;
        this.arrayChange.push(item.Index);

        if(arrayParents.length==0){

          let x=item.Index.Parent;
          x.Children=[];
          arrayParents.push(item.Index.Parent)
        }
        else {
          if(arrayParents.find(Pd=>Pd.Id==item.Index.ParentId)) {}
          else {
            let x=item.Index.Parent;
            x.Children=[];
            arrayParents.push(item.Index.Parent)
          }
        }

      }

      this.indexs=arrayParents;

      for(const indexP of  this.indexs)
      {
        for(const index  of    this.arrayChange)
        {
          if(index.ParentId==indexP.Id)
          {

            if(index.Grade==1)
            {
              index.Grade=true;
            }
            else {
              index.Grade=false;
            }
            indexP.Children.push(index)
          }
        }
      }

    })
  }

  CreateModel(){

    this.touched = false;
    this.sendStatus=false;
    this.obj=new Inspection();

  }

  Save(form,Index){

    this.obj.Indexes=[];
for(const itemCh of this.indexs) {
  for(const item of itemCh.Children)
  {
    if(item.Grade==true){
      let index={IndexId:item.Id,Grade:1}
      item.Parent=[];item.IndexId=item.Id;this.obj.Indexes.push(index)
    }
    else {
      let index={IndexId:item.Id,Grade:0}
      item.Parent=[];item.IndexId=item.Id;this.obj.Indexes.push(index)
    }

  }
}

    this.touched = true;
    this.sendStatus=false;
    if (form.valid ||    this.InspectionId) {
      this.progressForm=true;
      if(  this.obj.Id){
        this.service.update(this.obj).subscribe(res=>{

          this.SendNoti('Success','پیام','بازرسی با موفقیت ویرایش شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.progressForm=false;
          // this.SendNoti('Primary');

          // this.SendNoti('Warning');
          // this.SendNoti('Error');
        },error => {
          this.progressForm=false;
          Index.Grade=!Index.Grade;
        });
      }
      else {

        this.service.insert(this.obj).subscribe(res=>{

          this.obj.Id=res.Data.Id

          this.SendNoti('Success','پیام','بازرسی با موفقیت افزوده شد.');
          this.oncomplete.emit(this.obj);
          this.touched = false;
          this.sendStatus=true;
          this.progressForm=false;
        },error => {
          this.progressForm=false;
          Index.Grade=!Index.Grade;
        });
      }
    }
    else {

      Index.Grade=!Index.Grade;
    }
  }
  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }




    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }
}
