import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { PlaceComponent } from './place/place.component';
import { IndexesComponent } from './indexes/indexes.component';
import { InspectionComponent } from './inspection.component';
import {InspectionRouting} from './inspection.routing';
import {SigmaSoftModule} from '../../lib/sigmasoft-ng';
import {PlaceService} from '../../services/place.service';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {InspectionService} from '../../services/inspection.service';
import { ListComponent } from './list/list.component';
import {NgxTreeSelectModule} from '../../components/tree-select';
import {PlaceModule} from '../basic-information/place/place.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    InspectionRouting,
    NgxTreeSelectModule,
    SigmaSoftModule,
    PlaceModule
  ],
  providers:[PlaceService,InspectionService],
  declarations: [PlaceComponent, IndexesComponent, InspectionComponent, ListComponent]
})
export class InspectionModule { }
