import { Component, OnInit } from '@angular/core';
import {ODataPagination} from '../../../models/oDataPagination';
import {PlaceService} from '../../../services/place.service';
import {Global} from '../../../lib/sigmasoft-ts';
import {Place} from '../../../models/place';
import {ActivatedRoute, Router} from '@angular/router';


@Component({
  selector: 'app-place',
  templateUrl: './place.component.html',
  styleUrls: ['./place.component.css']
})
export class PlaceComponent implements OnInit {
  oDataPagination:ODataPagination=new ODataPagination();
  global = Global;
  buildingItems:Place[]=[];
  gradeItems:Place[]=[];
  buildingObj:Place=new Place();
  gradeObj:Place=new Place();
  placeId:number;
  touched:boolean=false;
  constructor(private service:PlaceService, private router: Router) { }
  mapEditorComplete(result) {

     if(result.placeId){
      this.placeId=result.placeId
      this.router.navigate(['/Pages/Inspection/Indexes'], { queryParams: { placeId: this.placeId } });
    }
    else {
      this.gradeObj=new Place()
    }
    // this.mode = Modes.Detail;
  }
  ngOnInit() {

    this.getBuildingData(this.oDataPagination)
  }
  getBuildingData(oDataPagination:ODataPagination){
    this.buildingItems=[];
    oDataPagination.filter=`ParentId eq ${null}`;
    this.service.getWithPagination(oDataPagination).subscribe(res=>{
    this.buildingItems=res.Data;
    })
}
  getGradeData(oDataPagination:ODataPagination,ParentId){
    this.gradeItems=[];
  oDataPagination.filter=`ParentId eq ${ParentId}`;
    this.service.getWithPagination(oDataPagination).subscribe(res=>{
      this.gradeItems=res.Data;
    })
  }
  getPlaceData(oDataPagination:ODataPagination,ParentId){
    oDataPagination.filter=`ParentId eq ${ParentId}`;
    this.service.getWithPagination(oDataPagination).subscribe(res=>{



    })
  }

  Save(form,Id){
    this.placeId=Id
    this.router.navigate(['/Pages/Inspection/Indexes'], { queryParams: { placeId: this.placeId } });

  }
}
