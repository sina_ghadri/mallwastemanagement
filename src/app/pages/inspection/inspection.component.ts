import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {DataTable, DataTableButton, DataTableColumn, DataTableMenuItem, DataTableRow} from '../../lib/sigmasoft-ng';
import {Modal} from '../../lib/sigmasoft-ng/overlay/modal/models';
import {Global} from '../../lib/sigmasoft-ts/models';

import {InspectionService} from '../../services/inspection.service';
import {DataTableColumnSearchType, DataTableMenu} from '../../lib/sigmasoft-ng/data/table/models';
import {Inspection} from '../../models/inspection';
import {ODataPagination} from '../../models/oDataPagination';
import {JDate} from '../../lib/sigmasoft-ts/datetime';
import {Router} from '@angular/router';




@Component({
  selector: 'app-inspection',
  templateUrl: './inspection.component.html',
  styleUrls: ['./inspection.component.css']
})
export class InspectionComponent implements OnInit {


  global = Global;
  @ViewChild('datepickerTemplate') datepickerTemplate: TemplateRef<any>;
  @ViewChild('phoneTemplate') phoneTemplate: TemplateRef<any>;
  @ViewChild('columnTrueFalseType') columnTrueFalseType: TemplateRef<any>;





  paginationSearch:ODataPagination=new ODataPagination();
  datatable: DataTable = new DataTable({ language: 'fa' });

  row:  DataTableRow;
  insertModal: boolean ;
  detailModal: boolean;
  updateModal: boolean;
  deleteModal: Modal = new Modal;
  listModal: boolean;



  constructor(  private router: Router,private service: InspectionService) { }

  ngOnInit(): void {
    this.datatable.lazyloading = true;
    this.datatable.onrefresh.subscribe(() => this.load());
    this.datatable.onstateChange.subscribe((state) => {
      console.log('onstateChange', state);
      this.paginationSearch=new ODataPagination();
      this.paginationSearch.skip=(state.page-1)*10;
      this.paginationSearch.top=state.limit;

      if(state.searchBy.length>0 || state.sortBy){
        if(state.searchBy.length>0){
          let i=0;
          for(const item of  state.searchBy)
          {

            if(i>0 ||  this.paginationSearch.filter)
            {this.paginationSearch.filter= this.paginationSearch.filter+' and '+` indexof(${item.name}, '${item.search.value}') gt -1`}
            else {this.paginationSearch.filter= `indexof(${item.name}, '${item.search.value}') gt -1`}
            i++;
          }
        }
        if(state.sortBy){
          if(state.sortBy.title!='وضعیت')
          {
            if(state.ascending) {this.paginationSearch.orderby=state.sortBy.name+" asc";}
            else {this.paginationSearch.orderby=state.sortBy.name+" desc";}
          }
        }
      }
      this.loadData();
    });
    this.insertModal=false;
    this.listModal=true;

    this.datatable.columns.push(new DataTableColumn('تاریخ , زمان ', row => {return ( JDate.toJDate(JDate.parseDate(row.value.Time)).format('yyyy-MM-dd hh:mm:ss'))}).setName('Time'));
    this.datatable.columns.push(new DataTableColumn('شخص بازرس ', row => {return (row.value.User.Firstname +' '+row.value.User.Lastname)}).setName('User/Firstname'));
    this.datatable.columns.push(new DataTableColumn('طبقه', row => {return row.value.Station.Place.Parent.Name}).setName('Station/Place/Parent/Name'));
    this.datatable.columns.push(new DataTableColumn('کاربری', row => {return row.value.Station.UseType.Name}).setName('Station/UseType/Name'));
    // this.datatable.columns.push(new DataTableColumn('شاخص', row => {return row.value.Index.Name}));
    this.datatable.columns.push(new DataTableColumn('نتیجه ', row => {return row.value.Grade}).setName('Grade'));

    this.datatable.buttons.push(new DataTableButton('افزودن', () => this.insert(),'mdi  mdi-plus','btn-blue-light'));

    let menu = new DataTableMenu;
    menu.items.push(new DataTableMenuItem('ویرایش', row => this.update(row), 'mdi mdi-pencil', 'color-amber'));
    menu.items.push(new DataTableMenuItem('حذف', row => this.delete(row), 'mdi mdi-delete', 'color-red'));
    this.datatable.menus.push(menu);

    this.load();
  }

  loadData() {
    this.datatable.progress = true;
    this.service.getWithPagination(this.paginationSearch).subscribe(op => {
      this.datatable.dataSource = op.Data;
      this.datatable.count=op.Count;




    }, () => this.datatable.progress = false, () => this.datatable.progress = false);
  }
  load() {
    this.datatable.progress = true;
    this.paginationSearch=new ODataPagination();
    this.paginationSearch.top=10;
    this.paginationSearch.skip=0;
    this.datatable.firstPage();

    this.service.getWithPagination(this.paginationSearch).subscribe(op => {this.datatable.dataSource = op.Data; this.datatable.count=op.Count}, () => this.datatable.progress = false, () => this.datatable.progress = false);

  }
  loadById(id: number) {
    this.datatable.progress = true;
    this.service.getById(id).subscribe(op => this.datatable.replaceRow(row => row.value.Id == id, op.Data), () => this.datatable.progress = false, () => this.datatable.progress = false);
  }

  insert() {
    this.router.navigate(['Pages/Inspection/Place']);
    // this.listModal=false;this.insertModal=true;


  }
  insertComplete(result: Inspection) {

    if(result && !result.Id) this.datatable.addRow(result);     if(result && result.Id) this.datatable.addRow(result);
    // if(result) this.load();
  }
  detail(row: DataTableRow) { this.row = row; this.detailModal=true; }
  update(row: DataTableRow) { this.row = row;
    // this.updateModal=true;this.insertModal=false;this.listModal=false;
    this.router.navigate(['/Pages/Inspection/Indexes'], { queryParams: { InspectionId: row.value.Id } });
  }
  updateComplete(result: Inspection) {
    // if(result) this.loadById(this.row.value.id);
    if(result) this.datatable.replaceRow(row => row.value.Id == this.row.value.Id, result);
    this.updateModal=false;
    this.listModal=true;
  }
  delete(row: DataTableRow) { this.row = row; this.deleteModal.open(); }
  deleteComplete(result: number) {
    this.service.deleteById(this.row.value.Id).subscribe(res=>{
      this.datatable.removeRow(row => row.value.Id == this.row.value.Id);

      if(this.datatable.rows.length==0){
        this.load();
      }
      else {
        this.loadData();
      }
      this.deleteModal.close();
    },error=>{
      this.deleteModal.close();
    })

  }

}
