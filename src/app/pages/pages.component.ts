import { Component, OnInit } from '@angular/core';
import { Panel, LoginInfo, Token, Module, User } from './../lib/sigmasoft-ng/panel/admin/models';


import { DateTime,JDate } from './../lib/sigmasoft-ts';
import { HttpConfigService } from './../lib/sigmasoft-ng/misc/jwt/http-config.service';
import { ConfigurationService } from './../lib/sigmasoft-ng';

import {SwUpdate} from '@angular/service-worker';



@Component({
  selector: 'pages',
  templateUrl: './pages.component.html',
  exportAs: 'pages',
})
export class PagesComponent implements OnInit {
  update: boolean = false;
  panel: Panel;

loginInfo  = new LoginInfo;
  public constructor(private service: ConfigurationService, private httpConfig: HttpConfigService,updates: SwUpdate) {

    updates.available.subscribe(event => {
      console.log("update");
        this.update=true;
      updates.activateUpdate().then(() => document.location.reload());
    })
  }
  ngOnInit(): void {

    this.service.loadConfiguration('config.json').subscribe(config => {


      this.panel = new Panel;

      this.panel.user=new User();
      this.panel.user.name='سینا قدری';
      this.panel.user.image='assets/user.png';



      this.loginInfo = new LoginInfo;
      this.loginInfo.token = new Token;
      // this.loginInfo.token.key = localStorage.getItem('token');
      this.loginInfo.token.key = '123';
      this.loginInfo.token.user= new User;
      this.loginInfo.token.user.name='مدیر';

      this.loginInfo.token.user.username='مدیر';




      let module = new Module;
      module.symbol = config.symbol;
      module.id=1;
      module.name="پسماند مال";
      module.icon="mdi mdi-verified";

      this.loginInfo.modules = [ module  ];

      let data = this.loginInfo;
      this.panel.menus = config.menus;
      this.panel.onlogout.subscribe(() => {this.logout()});
      this.httpConfig.on4xxUnAuthorize.subscribe(() => location.href = config.login);
      if(data) {
        this.panel.datetime = DateTime.now;
        this.panel.module = data.modules.find(x => x.symbol == config.symbol);
        this.panel.modules = data.modules;
        // this.panel.user = data.token.user;

        this.httpConfig.token = data.token.key;
        this.httpConfig.host=this.service.configuration.baseUrl;

      } else location.href = config.login;

    });










  }

  logout(){

  }
}
