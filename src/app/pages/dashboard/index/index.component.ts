import { Component, OnInit } from '@angular/core';
import {WeatherService} from '../../../services/weather.service';
import {NgOrganizationChartHelper} from '../../../components/ng-organization-chart/ng-organization-chart-helper';
import {INode} from '../../../components/org-chart-master/lib';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  dataChart:any="";
  typeChart:string="line";
  optionChart:any="";
  showChart:boolean=false;
  data = [
    {
      id: "ایران مال",
      children: [
        {
          id: "ساختمان شماره 1",
          children: [

          ]
        },
        {
          id: "ساختمان شماره 2",
          children: [

          ]
        },
        {
          id: "ساختمان شماره 3",
          children: [
            {
              id: "طبقه 1",
              children: [

              ]
            },
            {
              id: "طبقه 2",
              children: [

              ]
            },
            {
              id: "طبقه 3",
              children: [

              ]
            },
            {
              id: "طبقه 4",
              children: [

              ]
            }

          ]
        },
        {
          id: "ساختمان شماره 4",
          children: [

          ]
        }
      ]
    }
  ];


  topNode: INode = {
    name: 'ایران مال',
    designation: 'امتیاز 1 ',
    color:'#fec721 ',
    childrens: [
      {
        name: "ساختمان شماره 1",
        designation: 'امتیاز 1 ',
        color:'#fec721',
        childrens: [
        ]
      },
      {
        name: "ساختمان شماره 2",
        designation: 'امتیاز 1 ',
        color:'#f27c73',
        childrens: [
        ]
      },
      {
        name: "ساختمان شماره 3",
        designation: 'امتیاز 1 ',
        color:'#fec721 ',
        childrens: [
          {
            name: "طبقه شماره 1",
            designation: 'امتیاز 1 ',
            color:'#56e45c',
            childrens: [
            ]
          },
          {
            name: "طبقه شماره 2",
            designation: 'امتیاز 1 ',
            color:'#f27c73  ',
            childrens: [
            ]
          },
          {
            name: "طبقه شماره 3",
            designation: 'امتیاز 1 ',
            color:'#fec721 ',
            childrens: [
            ]
          },
          {
            name: "طبقه شماره4",
            designation: 'امتیاز 1 ',
            color:'#f27c73  ',
            childrens: [
            ]
          }

        ]
      },
      {
        name: "ساختمان شماره4",
        designation: 'امتیاز 1 ',
        color:'#56e45c',
        childrens: [
        ]
      }

    ]
  };
  clickNode(node) {
    alert("Node '" + node.id + "' was clicked!")
  }

  dragNode(transfer) {
    let helper = new NgOrganizationChartHelper(this.data);
    helper.moveNode(transfer.node.id, transfer.destination.id);
    let data = helper.getData();
    this.data = data
  }

  constructor(private weatherService: WeatherService) { }

  ngOnInit() {

    this.loadChartWidget();

  }
  loadChartWidget(){
    this.weatherService.dailyForecast().subscribe(res => {
      this.loadChart(res);
    });

  }
  loadChart(res) {
    let temp_max = res['list'].map(res => res.main.temp_max);
    let temp_min = res['list'].map(res => res.main.temp_min);
    let alldates = res['list'].map(res => res.dt);

    // var img = new Image();
    // img.src = 'https://www.tubefilter.com/wp-content/uploads/2018/07/true-img-1024x629.jpg';

    let weatherDates = [];
    alldates.forEach((res) => {
      let jsdate = new Date(res * 1000);
      weatherDates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }))
    });

    this.dataChart= {
      labels: weatherDates,
      datasets: [
        {
          data: temp_max,
          borderColor: "#3cba9f",
          fill: false
        },
        // {
        //   data: temp_min,
        //   borderColor: "#ffcc00",
        //   fill: false
        // },
        // {
        //   data: temp_min,
        //   borderColor: "#ffcc00",
        //   fill: false
        // },
      ]
    };


    this.optionChart ={
      legend: {
        display: false
      },
      scales: {
        yAxes: [{
          display: true
        }],
        xAxes: [{
          display: true
        }]
      },
      tooltips: {
        mode: 'dataset' //nearest //index //dataset //point
      }
    };
    this.showChart=true;
  }




}
