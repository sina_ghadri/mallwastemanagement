import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { IndexComponent } from './index/index.component';
import { ChartComponent } from './chart/chart.component';
import {WeatherService} from '../../services/weather.service';
import {ProgressBarModule} from '../../lib/sigmasoft-ng/progress/bar/bar.module';
import {NgOrganizationChartModule} from '../../components/ng-organization-chart/ng-organization-chart.module';
import {OrgChartModule} from '../../components/org-chart-master/lib';

@NgModule({
  imports: [
    CommonModule,
    ProgressBarModule,
    DashboardRoutingModule,
    OrgChartModule,
    NgOrganizationChartModule
  ],
  exports:[ChartComponent],
  providers:[WeatherService],
  declarations: [IndexComponent, ChartComponent]
})
export class DashboardModule { }
