import {Component, OnInit, ElementRef, Input} from '@angular/core';
import { Chart } from 'chart.js';
import 'chartjs-plugin-labels';
@Component({
  selector: 'dashboard-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {

  @Input() responsive: boolean = true;
  @Input() type: string =''; //bar //radar //pie //polarArea //bubble //scatter
  @Input() option: any ='';
  @Input() data: any='';


  chart: Chart;
  constructor(private element: ElementRef) { }

  ngOnInit() {
    var canvas = this.element.nativeElement.getElementsByTagName("canvas")[0];
    var ctx = canvas.getContext("2d");
    this.chart = new Chart(ctx, {
      responsive:this.responsive,
      type: this.type,
      data: this.data,
      options: this.option,
    });
  }

}
