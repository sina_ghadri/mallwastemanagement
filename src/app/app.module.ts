import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";


import { ServiceWorkerModule } from '@angular/service-worker';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpConfigService, SigmaSoftModule} from './lib/sigmasoft-ng';
import {environment} from '../environments/environment';
import {CssService} from './services/css.service';
import {DataService} from './services';
import {HttpService} from './lib/sigmasoft-ng/misc';
import {UserService} from './services/user.service';






@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    HttpClientModule,

    AppRoutingModule,
    SigmaSoftModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),


  ],
  declarations: [AppComponent],
  providers: [HttpConfigService,CssService,DataService,UserService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpService, multi: true },],
  bootstrap: [AppComponent]
})
export class AppModule {}
