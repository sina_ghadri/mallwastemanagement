# password-toggle
Angular 6+ directive to show/hide password


## Install
```

```

## Import
```
import { PasswordToggleModule } from 'password-toggle';

@NgModule({
  imports: [
    ...,
    PasswordToggleModule
  ]
})
export class MyModule(){ }
```

## Use directive
use `passwordToggle` directive


```
<!-- Password -->
<div class="field">
  <label for="">Password</label>
  <input type="password" name="password" ngModel passwordToggle required>
</div>
```

