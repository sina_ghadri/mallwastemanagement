import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PasswordToggleDirective } from './directive';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PasswordToggleDirective
  ],
  exports : [
    PasswordToggleDirective
  ]
})
export class PasswordToggleModule { }
