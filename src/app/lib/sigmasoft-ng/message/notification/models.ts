export enum NotificationType {
    Primary = 'primary',
    Success = 'success',
    Warning = 'warning',
    Error = 'error'
}
export class NotificationItem {
    public _animatedIn: boolean;
    public _animatedOut: boolean;

    public title: string;
    public message:string;
    public type: NotificationType = NotificationType.Primary;
    public innerHTML: string;

    public timeout: number = 0;
    public animationIn:string = 'fadeInDown';
    public animationOut: string = 'fadeOutDown';
    public isshow: boolean = false;

    constructor(title: string, message: string) {
        this.title = title;
        this.message = message;
    }
    setTimeout(value: number): NotificationItem { this.timeout = value; return this; }
    setType(value: NotificationType): NotificationItem { this.type = value; return this; }

    show() {
        this._animatedIn = true;
        setTimeout(() => this.isshow = true,1)
        setTimeout(() => this._animatedIn = false,1000);
    }
    hide() {
        this._animatedOut = true;
        setTimeout(() => {this.isshow = false; this._animatedOut = false},1000);
    }
    get class(): string[] {
        let list: string[] = [];
        if(this.isshow) list.push('show');
        if(this.type) list.push(this.type);
        if(this._animatedIn && this.animationIn) {
            list.push(this.animationIn);
            list.push('animated');
        } else if(this._animatedOut && this.animationOut) {
            list.push(this.animationOut);
            list.push('animated');
        }
        return list;
    }
}
