import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AuthorizationType } from './enums';
import { HttpConfigService } from './http-config.service';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/do';
import {NotificationService} from '../../message/notification/notification.service';
import {NotificationItem, NotificationType} from '../../message/notification/models';

@Injectable()
export class HttpService implements HttpInterceptor {

  constructor(private service: HttpConfigService,private notificationService:NotificationService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let url = request.url;
    // let token = this.service.token || localStorage.getItem('token');
    let token = this.service.token || "123";
    let authorizationType = this.service.authorizationType;

    if (this.service.host) url = url.replace('[host]', this.service.host);
    if (this.service.port) url = url.replace('[port]', this.service.port.toString());

    if (token && token.length > 0) {
      switch (authorizationType) {

        case AuthorizationType.Header:

          request = request.clone({
            url: url,
            // setHeaders: { Authorization: token ? token : '' ,'Content-Type': 'application/json'}
            setHeaders: { Authorization: 'Bearer ' + token ? 'Bearer ' + token : '' ,'Content-Type': 'application/json'}
            // setHeaders: { Authorization: token ? token : '' ,'Content-Type': 'application/json',"X-CSRFToken": 'csrftoken'}
          });
          break;
        case AuthorizationType.QueryString:

          url = url + (url.indexOf('?') > -1 ? '&' : '?') + 'ApiKey=' + token;
          request = request.clone({ url: url ,   setHeaders: { 'Content-Type': 'application/json'}});
          break;
      }
    } else request = request.clone({ url: url ,   setHeaders: { 'Content-Type': 'application/json'}});
    return next.handle(request).do(
      (e: HttpEvent<any>) => {
        this.service.on2xx.emit(request);
        if (e instanceof HttpResponse) {
          if (e.status < 200) {
            this.service.on1xx.emit(request);
            if (e.status == 100) this.service.on1xxContinue.emit(request);
            else if (e.status == 101) this.service.on1xxSwitchingProtocols.emit(request);
            else if (e.status == 102) this.service.on1xxProcessing.emit(request);
            else if (e.status == 103) this.service.on1xxEarlyHints.emit(request);
          } else if (e.status < 300) {
            this.service.on2xx.emit(request);
            if (e.status == 200) this.service.on2xxOk.emit(request);
            else if (e.status == 202) this.service.on2xxCreated.emit(request);
            else if (e.status == 203) this.service.on2xxAccepted.emit(request);
            else if (e.status == 204) this.service.on2xxNonAuthoritativeInformation.emit(request);
            else if (e.status == 205) this.service.on2xxNoContent.emit(request);
            else if (e.status == 206) this.service.on2xxResetContent.emit(request);
            else if (e.status == 207) this.service.on2xxPartialContent.emit(request);
            else if (e.status == 208) this.service.on2xxMultiStatus.emit(request);
            else if (e.status == 209) this.service.on2xxAlreadyReported.emit(request);
            else if (e.status == 226) this.service.on2xxImUsed.emit(request);
          } else if (e.status < 400) {
            this.service.on3xx.emit(request);
            if (e.status == 300) this.service.on3xxMultipleChoices.emit(request);
            else if (e.status == 301) this.service.on3xxMovedPermanently.emit(request);
            else if (e.status == 302) this.service.on3xxFound.emit(request);
            else if (e.status == 303) this.service.on3xxSeeOther.emit(request);
            else if (e.status == 304) this.service.on3xxNotModified.emit(request);
            else if (e.status == 305) this.service.on3xxUseProxy.emit(request);
            else if (e.status == 306) this.service.on3xxSwitchProxy.emit(request);
            else if (e.status == 307) this.service.on3xxTemporaryRedirect.emit(request);
            else if (e.status == 308) this.service.on3xxPermanentRedirect.emit(request);
          }
        }
      },
      (e: any) => {
        this.service.on4xx.emit(request);
        if (e instanceof HttpErrorResponse) {


          this.SendNoti('Error','خطا',e.error.customMsg)

          if (e.status < 500) {
            this.service.on4xx.emit(request);
            if (e.status == 400) this.service.on4xxBadRequest.emit(request);
            else if (e.status == 401) this.service.on4xxUnAuthorize.emit(request);
            else if (e.status == 402) this.service.on4xxPaymentRequire.emit(request);
            else if (e.status == 403) this.service.on4xxForbidden.emit(request);
            else if (e.status == 404) this.service.on4xxNotFound.emit(request);
            else if (e.status == 405) this.service.on4xxMethodNotAllowed.emit(request);
            else if (e.status == 406) this.service.on4xxNotAcceptable.emit(request);
            else if (e.status == 407) this.service.on4xxProxyAuthenticationRquired.emit(request);
            else if (e.status == 408) this.service.on4xxRequestTimeout.emit(request);
            else if (e.status == 409) this.service.on4xxConfilict.emit(request);
            else if (e.status == 410) this.service.on4xxGone.emit(request);
            else if (e.status == 411) this.service.on4xxLengthRequired.emit(request);
            else if (e.status == 412) this.service.on4xxPreconditionFailed.emit(request);
            else if (e.status == 413) this.service.on4xxPayloadTooLarg.emit(request);
            else if (e.status == 414) this.service.on4xxUrlTooLarg.emit(request);
            else if (e.status == 415) this.service.on4xxUnsupportedMediaType.emit(request);
            else if (e.status == 416) this.service.on4xxRangeNotSatisfiable.emit(request);
            else if (e.status == 417) this.service.on4xxExpectationFailed.emit(request);
            else if (e.status == 418) this.service.on4xxImTeapot.emit(request);
            else if (e.status == 421) this.service.on4xxMisdirectedRequest.emit(request);
            else if (e.status == 422) this.service.on4xxUnprocessableEntity.emit(request);
            else if (e.status == 423) this.service.on4xxLocked.emit(request);
            else if (e.status == 424) this.service.on4xxFailedDependency.emit(request);
            else if (e.status == 426) this.service.on4xxUpgradeRequired.emit(request);
            else if (e.status == 428) this.service.on4xxPreconditionRequired.emit(request);
            else if (e.status == 429) this.service.on4xxTooManyRequest.emit(request);
            else if (e.status == 431) this.service.on4xxRequestHeaderFieldsTooLarg.emit(request);
          } else if (e.status < 600) {
            this.service.on5xx.emit(request);
            if (e.status == 500) this.service.on5xx.emit(request);
            else if (e.status == 501) this.service.on5xxInternalServerError.emit(request);
            else if (e.status == 502) this.service.on5xxNotImplemented.emit(request);
            else if (e.status == 503) this.service.on5xxBadGateway.emit(request);
            else if (e.status == 504) this.service.on5xxServiceUnavailable.emit(request);
            else if (e.status == 505) this.service.on5xxGatewayTimeout.emit(request);
            else if (e.status == 506) this.service.on5xxHttpVersionNotSupported.emit(request);
            else if (e.status == 507) this.service.on5xxInsufficientStorage.emit(request);
            else if (e.status == 508) this.service.on5xxLoopDetected.emit(request);
            else if (e.status == 510) this.service.on5xxNotExtended.emit(request);
            else if (e.status == 511) this.service.on5xxNetworkAuthenticationRequired.emit(request);
          }
        }
      },
      () => {
        this.service.oncomplete.emit(request);
      }
    );
  }

  SendNoti(Type,Title, Message){
    let notificationItem = new NotificationItem(Type,Message);
    notificationItem.title=Title;
    if(Type=='Primary')
    {
      notificationItem.type = NotificationType.Primary;

    }

    if(Type=='Success')
    {
      notificationItem.type = NotificationType.Success;
    }

    if(Type=='Warning')
    {
      notificationItem.type = NotificationType.Warning;
    }

    if(Type=='Error')
    {
      notificationItem.type = NotificationType.Error;
    }



    notificationItem.timeout=5000;
    this.notificationService.addItem(notificationItem);
  }
}
