import { NgModule } from "@angular/core";
import { NodeModule } from "./nodes";

@NgModule({
    imports: [

    ],
    exports: [
        NodeModule
    ]
})
export class BaseChartsModule {}