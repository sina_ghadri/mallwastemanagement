import { Component, OnInit, ElementRef, HostListener, Input, TemplateRef, Output, EventEmitter } from '@angular/core';
import { Node, NodeItemIORelation, Point, NodeItemIO, NodeItemInput, NodeItemOutput, Size, NodePanel, NodeItem } from '../../models';

@Component({
  selector: 'ss-chart-panel',
  templateUrl: './panel.component.html',
  styleUrls: ['./panel.component.scss']
})
export class PanelComponent implements OnInit {
  @Input() panel: NodePanel;
  @Input() template: TemplateRef<any>;

  @Output('nodeMove') onnodeMove: EventEmitter<Node> = new EventEmitter;
  @Output('nodeDblClick') onnodeDblClick: EventEmitter<Node> = new EventEmitter;

  @Output('itemClick') onitemClick: EventEmitter<NodeItem> = new EventEmitter;
  @Output('itemDblClick') onitemDblClick: EventEmitter<NodeItem> = new EventEmitter;

  @Output('inputDblClick') oninputDblClick: EventEmitter<NodeItemInput> = new EventEmitter;
  @Output('outputDblClick') onoutputDblClick: EventEmitter<NodeItemOutput> = new EventEmitter;

  @Output('relationClick') onrelationClick: EventEmitter<NodeItemIORelation> = new EventEmitter;
  @Output('relationDblClick') onrelationDblClick: EventEmitter<NodeItemIORelation> = new EventEmitter;
  @Output('connect') onconnect: EventEmitter<NodeItemIORelation> = new EventEmitter;
  @Output('disconnect') ondisconnect: EventEmitter<NodeItemIORelation> = new EventEmitter;

  @HostListener('mousemove', ['$event']) onmousemove(e: MouseEvent) {
    // console.log(e.clientY, e.offsetY, e.pageY, e.layerY, e.y);
    if (this.dragNode) {
      this.dragNode.position.x = this.panel.scroll.x - this.panel.position.x + e.clientX - this.offsetX;
      this.dragNode.position.y = this.panel.scroll.y - this.panel.position.y + e.clientY - this.offsetY;
    }
    if (this.connectingIO) {
      let start = this.connectingIO.element.getBoundingClientRect();
      this.connectingPath = this.panel.getPath(new Point(start.left + 5, start.top + 5), new Point(e.clientX, e.clientY))
    }
  }
  dragNode: Node;
  connectingIO: NodeItemInput;
  connectingPath: string;
  private offsetX: number;
  private offsetY: number;

  constructor(private element: ElementRef) {

  }

  ngOnInit() {
    this.panel.element = this.element.nativeElement;

    this.onnodeDblClick.subscribe((e) => console.log('onnodeDblClick : ', e))
    this.onnodeMove.subscribe((e) => console.log('onnodeMove : ', e))

    this.onitemClick.subscribe((e) => console.log('onitemClick : ', e))
    this.onitemDblClick.subscribe((e) => console.log('onitemDblClick : ', e))

    this.oninputDblClick.subscribe((e) => console.log('oninputDblClick : ', e))
    this.onoutputDblClick.subscribe((e) => console.log('onoutputDblClick : ', e))

    this.onrelationClick.subscribe((e) => console.log('onrelationClick : ', e))
    this.onrelationDblClick.subscribe((e) => console.log('onrelationDblClick : ', e))
    this.onconnect.subscribe((e) => console.log('onconnect : ', e))
    this.ondisconnect.subscribe((e) => console.log('ondisconnect : ', e))

  }
  dragNodeStart(e: any) {
    let event = <MouseEvent>e.event;
    this.offsetX = event.offsetX;
    this.offsetY = event.offsetY;
    this.dragNode = e.node;
  }
  dragNodeEnd() {
    if(this.dragNode) this.onnodeMove.emit(this.dragNode);
    this.dragNode = null;
  }
  connecting(e: NodeItemIO) {
    let io: NodeItemIO;
    if (!e || this.connectingIO == e) { this.connectingIO = null; return; }
    if (e instanceof NodeItemOutput && !this.connectingIO) io = e;
    if (e instanceof NodeItemInput && this.connectingIO instanceof NodeItemOutput) {
      this.onconnect.emit(this.connectingIO.connect(e));
    } else if (e instanceof NodeItemInput && e.relations.length == 1) {
      let rel = e.relations.pop();
      this.connectingPath = null;
      io = rel.output;
      this.ondisconnect.emit(rel);
    }
    if(io) this.connectingPath = null;
    this.connectingIO = io;
  }
}
