import { Component, OnInit, Input, ElementRef, HostBinding, HostListener, Output, EventEmitter } from '@angular/core';
import { NodeItemIO } from '../../models';

@Component({
  selector: 'ss-charts-node-io',
  templateUrl: './io.component.html',
  styleUrls: ['./io.component.scss']
})
export class IOComponent implements OnInit {
  @Input() io: NodeItemIO;

  @Output('connecting') onconnecting: EventEmitter<NodeItemIO> = new EventEmitter;

  @HostBinding('class.connected') get connect(): boolean { return this.io.relations.length > 0; }
  
  @HostListener('click', ['$event']) onclick(e: MouseEvent) {
    this.onconnecting.emit(this.io);
  }
  @HostListener('mouseenter', ['$event']) onmouseenter(e: MouseEvent) {
    for(let i = 0;i < this.io.relations.length;i++) {
      this.io.relations[i].selected = true;
    }
  }
  @HostListener('mouseleave', ['$event']) onmouseleave(e: MouseEvent) {
    for(let i = 0;i < this.io.relations.length;i++) {
      this.io.relations[i].selected = false;
    }
  }

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.io.element = this.element.nativeElement;
  }
}
