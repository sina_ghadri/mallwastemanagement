import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { NodeItem, NodeItemIO, NodeItemInput, NodeItemOutput } from '../../models';

@Component({
  selector: 'ss-charts-node-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input() item: NodeItem;

  @Output('connecting') onconnecting: EventEmitter<NodeItemIO> = new EventEmitter;

  @Output('itemClick') onitemClick: EventEmitter<NodeItem> = new EventEmitter;
  @Output('itemDblClick') onitemDblClick: EventEmitter<NodeItem> = new EventEmitter;
  @Output('inputDblClick') oninputDblClick: EventEmitter<NodeItemInput> = new EventEmitter;
  @Output('outputDblClick') onoutputDblClick: EventEmitter<NodeItemOutput> = new EventEmitter;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.item.element = this.element.nativeElement;
  }
}
