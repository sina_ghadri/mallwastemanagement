import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { NextDirective } from "./next.directive";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [NextDirective],
  exports: [NextDirective]
})
export class InputNextModule {}
