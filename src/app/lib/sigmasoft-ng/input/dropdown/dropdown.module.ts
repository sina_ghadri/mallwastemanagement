import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { DropdownComponent } from "./dropdown.component";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [DropdownComponent],
  exports: [DropdownComponent]
})
export class InputDropdownModule {}
