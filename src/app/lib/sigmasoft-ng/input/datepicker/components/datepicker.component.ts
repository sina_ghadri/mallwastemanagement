import {Component, Input, OnInit, ElementRef, forwardRef, EventEmitter, Output} from '@angular/core';
import { DateTime } from '../../../../sigmasoft-ts';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import {DatePickerOptions, DatePickerType, DayType} from '../models';
import {JDate} from '../../../../sigmasoft-ts/datetime';


@Component({
    selector: 'ss-input-datepicker',
    templateUrl: './datepicker.component.html',
    styleUrls: ['./datepicker.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DatepickerComponent),
            multi: true
        }
    ]
})
export class DatepickerComponent implements OnInit, ControlValueAccessor {
    @Input() input: HTMLInputElement;
    @Input() jalali: boolean;
    @Input() format: string = 'yyyy/MM/dd';
    @Input() min: string;
    @Input() show: boolean;
    @Input() calendar: boolean = false;
    @Input() max: string;
    @Input() disables: any[];
    @Input() daysType: DayType[];
    @Input() multiple: boolean = false;
    @Input() range: boolean = false;
    @Input() date: boolean = true;
    @Input() time: boolean = false;
    @Input() options: DatePickerOptions = new DatePickerOptions;
  @Output() onchangeSend: EventEmitter<any> = new EventEmitter;

    isdisabled: boolean = false;
    onchange: (value: any) => any = value => {};
    ontouched: () => any = () => {};


    yearDialog: boolean = false;
    monthDialog: boolean = false;

    item: DateTime;
    items: DateTime[];

    get monthesName(): string[] { return this.options.getMonthesName(); }
    get weekDaysName(): string[] { return this.options.getWeekDaysName(); }
    get monthDaysCount(): number[] { return this.options.getMonthDaysCount(); }

    constructor(private element: ElementRef) { }
    ngOnInit(): void {
        if(this.jalali != undefined) this.options.type = DatePickerType.Jalali;
        if (this.input) {
            this.input.addEventListener('click', () => this.open());
            this.input.addEventListener('focus', () => this.open());
            this.input.addEventListener('blur', () => {if(this.element.nativeElement.querySelector('*:hover') == null) this.close();});
            this.input.addEventListener('keydown', (e) => {
                console.log(e.keyCode);

                if(e.keyCode == 8) return;
                if([13,27].indexOf(e.keyCode) > -1) {
                    if(e.keyCode == 13) {
                        this.setValue();
                        this.close();
                    } else {
                        this.close();
                    }
                } else this.open();
                this.writeValue(this.multiple ? this.input.value.split(',') : this.input.value);
            });
        } else this.show = true;
    }
    writeValue(value: string|string[]): void {
        if(this.multiple || this.range) this.items = [];
        if(Array.isArray(value)) {
            this.multiple = true;
            for(let i = 0,length = value.length;i < length;i++) {
                this.items.push(DateTime.parse(value[i],this.format));
            }
        } else {
            this.item = DateTime.parse(value,this.format);
            let today = this.options.globalization.today();
            if(this.item.getSecond() < 0) this.item.setSecond(today.getSecond());
            if(this.item.getMinute() < 0) this.item.setMinute(today.getMinute());
            if(this.item.getHour() < 0) this.item.setHour(today.getHour());
            if(this.item.getDay() < 1) this.item.setDay(today.getDay());
            if(this.item.getMonth() < 1) this.item.setMonth(today.getMonth());
            if(this.item.getYear() < 1) this.item.setYear(today.getYear());

            if(value) value = this.item.format(this.format);
          this.onchangeSend.emit();
        }
        //this.onchange(value);
    }
    registerOnChange(fn: any): void { this.onchange = fn; }
    registerOnTouched(fn: any): void { this.ontouched = fn; }
    setDisabledState?(isdisabled: boolean): void { this.isdisabled = isdisabled; }

    open() { this.show = true; }
    close() { this.show = false; }

    getColor(day){

      let month =this.item.getMonth();
      let year =this.item.getYear();

     let Date=new JDate(year,month,day).format('yyyy-MM-dd');

      if(this.daysType){

        if(this.daysType.find(item=> JDate.toJDate(JDate.parseDate(item.Date)).format('yyyy-MM-dd')==Date))
        {
          return this.daysType.find(item=>JDate.toJDate(JDate.parseDate(item.Date)).format('yyyy-MM-dd')==Date).Color
        }
        else {
          return '#000000'
        }
      }
      else {
        return '#000000'
      }
    }
    getTitle(day){

    let month =this.item.getMonth();
    let year =this.item.getYear();

    let Date=new JDate(year,month,day).format('yyyy-MM-dd');

    if(this.daysType){
      if(this.daysType.find(item=> JDate.toJDate(JDate.parseDate(item.Date)).format('yyyy-MM-dd')==Date))
      {
        return this.daysType.find(item=>JDate.toJDate(JDate.parseDate(item.Date)).format('yyyy-MM-dd')==Date).Name
      }
      else {
        return ''
      }
    }
    else {
      return ''
    }
  }

    getDays(): number[] {
        let start = this.options.globalization.getStartDayOfWeek(this.item.getYear(),this.item.getMonth());

        let days: number[] = [];
        for (let i = 0; i < start; i++) days.push(null);

        let month = this.item.getMonth();
        let count = this.monthDaysCount[month] + (this.item.isLeapYear() && month == 12 ? 1 : 0);
        for (let i = 1; i <= count; i++) days.push(i);
        return days;
    }
    setValue() {
        if(this.items && (this.multiple || this.range)) {
            let values: string[] = [];
            for(let i = 0,length = this.items.length;i < length;i++) values.push(this.items[i].format(this.format));
            this.onchange(values);
        } else {
            this.onchange(this.item.format(this.format));
        }
        this.focus();

        this.yearDialog = false;
        this.monthDialog = false;
      this.onchangeSend.emit();
    }
    setDay(day: number) {
        let date = this.item.setDay(day).clone();
        let flag = false;
        if(this.range) {
            if(this.items.length >= 2) {
                this.items = [];
                this.items.push(date);
            }
            else if(this.items.length == 1) {
                if(this.items[0].smallerDate(date)) this.items.push(date);
                else if(this.items[0].greaterDate(date)) this.items.unshift(date);
            }
            else this.items.push(date);
        }
        else if(this.multiple) {
            let flag = true;
            for(let i = 0,length = this.items.length;i < length;i++) if(this.items[i].equalsDate(date)) { this.items.splice(i,1); flag = false; break; }
            if(flag) this.items.push(date);
        } else flag = true;
        this.setValue();
        if(flag && !this.calendar) this.close();
    }
    setMonth(month: number) {
        if(month < 1) { this.item.addYear(-1); month = 12; }
        else if (month > 12) { this.item.addYear(1); month = 1; }
        this.item.setMonth(month);
        this.setValue();
        this.monthDialog = false;
    }
    setYear(year: number) { this.item.setYear(year); this.setValue(); }
    setHour(value: number) { this.item.setHour(value); this.setValue(); }
    setMinute(value: number) { this.item.setMinute(value); this.setValue(); }
    setSecond(value: number) { this.item.setSecond(value); this.setValue(); }

    addHours(value: number) { this.item.addHours(value); this.setValue(); }
    addMinutes(value: number) { this.item.addMinutes(value); this.setValue(); }
    addSeconds(value: number) { this.item.addSeconds(value); this.setValue(); }

    prevYear() { if(this.item.getYear() > 0) this.setYear(this.item.getYear()-1); }
    prevMonth() { this.setMonth(this.item.getMonth()-1); }
    today() { let preValue = this.item; this.item = this.options.globalization.today(); if(this.isDisabled(this.item.getDay())) this.item = preValue; else this.setValue();         this.onchangeSend.emit(); }
    nextYear() { this.setYear(this.item.getYear()+1); }
    nextMonth() { this.setMonth(this.item.getMonth()+1);  }

    focus() { if(this.input) this.input.focus(); }

    isActive(day: number) {
        let date = this.item.clone().setDay(day);
        if(this.range) {
            if(this.items.length > 0 && this.items[0].equalsDate(date)) return true;
            if(this.items.length > 1 && this.items[1].equalsDate(date)) return true;
            else if(this.items.length == 2) {
                return (this.items[0].smallerDate(date) && this.items[1].greaterDate(date));
            }
            return false;
        }
        if(this.multiple) {
            for(let i = 0,length = this.items.length;i < length;i++) if(this.items[i].equalsDate(date)) return true;
            return false;
        }
        return this.item.getDay() == day;
    }
    isDisabled(day: number,index?:number) {
        let flag = false;
        let date = this.item.clone().setDay(day);
        if(!index) index = this.options.globalization.getDayOfWeek(date);
        if(this.min || this.max) {
            if(this.min && this.min > date.format(this.format)) flag = true;
            if(this.max && this.max < date.format(this.format)) flag = true;
        }
        if(this.disables) {
            for(let i = 0,length = this.disables.length;i < length;i++) {
                if(typeof this.disables[i] == 'number') {
                    if(index%7 == this.disables[i]) { flag = true; break; }
                }
                else if(typeof this.disables[i] == 'string') {
                    date.setDay(day);
                    if(date.format(this.format) == this.disables[i]) { flag = true; break; }
                }
            }
        }
        return flag;
    }

    yearDialogModal() {
        this.yearDialog = true;
        this.element.nativeElement.querySelector('.year-dialog>input').select();
    }
    toggleMonth() {
        this.yearDialog = false;
        this.monthDialog = !this.monthDialog;
    }
    toggleYear() {
        this.monthDialog = false;
        this.yearDialog = !this.yearDialog;
        if(this.yearDialog) setTimeout(() => this.element.nativeElement.querySelector('#year').focus(),10);
    }
    clone<T>(instance: T): T {
        const copy = new (instance.constructor as { new (): T })();
        Object.assign(copy, instance);
        return copy;
    }
}
