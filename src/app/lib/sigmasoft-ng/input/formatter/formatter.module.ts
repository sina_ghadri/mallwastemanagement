import { NgModule } from "@angular/core";
import { SeperatePipe } from "./pipes/seperate.pipe";

@NgModule({
  imports: [],
  declarations: [SeperatePipe],
  exports: [SeperatePipe]
})
export class InputFormatterModule {}
