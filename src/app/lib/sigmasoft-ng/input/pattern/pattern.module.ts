import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PatternDirective } from "./pattern.directive";

@NgModule({
  imports: [CommonModule],
  declarations: [PatternDirective],
  exports: [PatternDirective]
})
export class InputPatternModule {}
