import { Directive, ElementRef, Input, OnInit, HostListener } from '@angular/core';

@Directive({
  selector: '[ss-input-pattern]'
})
export class PatternDirective implements OnInit {
  @Input('ss-input-pattern') pattern: string;
  private temp: string;

  @HostListener('keydown',['$event']) onkeydown(e:KeyboardEvent) {
    if(!this.temp) this.temp = this.element.nativeElement.value;
  }
  @HostListener('keyup',['$event']) onkeyup(e:KeyboardEvent) {
    let regex = new RegExp(this.pattern);
    let value = this.element.nativeElement.value;
    if(!regex.test(value)) this.element.nativeElement.value = this.temp;
    this.temp = '';
  }

  constructor(private element:ElementRef) { }

  ngOnInit(): void {
  }
}
