import { Component, OnInit, ElementRef, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'ss-button-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    }
  ]
})
export class SelectComponent implements OnInit {
  buttons: HTMLElement[];

  onchange: (values:number) => any = () => {};
  ontouched: () => any = () => {};
  isdisabled: boolean;

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this.buttons = this.element.nativeElement.querySelectorAll('[class^="btn-"], [class*=" btn-"]');
    for (let i = 0; i < this.buttons.length; i++) this.click(this.buttons[i]);
  }
  writeValue(value: number): void {
    for (let j = 0; j < this.buttons.length; j++) {
      if(+this.buttons[j].dataset.value == value) this.buttons[j].classList.add('active');
      else this.buttons[j].classList.remove('active');
    }
  }
  registerOnChange(fn: any): void { this.onchange = fn; }
  registerOnTouched(fn: any): void { this.ontouched = fn; }
  setDisabledState?(isdisabled: boolean): void { this.isdisabled = isdisabled; }

  click(button: HTMLElement) {
    button.addEventListener('click', e => {
      for (let j = 0; j < this.buttons.length; j++) {
        if(this.buttons[j] == button) {
          this.onchange(+button.dataset.value);
          button.classList.add('active');
        }
        else this.buttons[j].classList.remove('active');
      }
    });
  }
}
