import { NgModule } from "@angular/core";
import { ButtonButtonModule } from "./button";
import { ButtonSelectModule } from "./select";
import { ButtonToggleModule } from "./toggle";

@NgModule({
    imports: [],
    exports: [
        ButtonButtonModule,
        ButtonSelectModule,
        ButtonToggleModule,
    ]
})
export class BaseButtonModule { }