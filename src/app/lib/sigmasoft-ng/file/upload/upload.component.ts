import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FileSlice, FileSliceState, File, FileState } from './models';
import { IUploadService } from './interfaces';
import { Formatter } from '../../../sigmasoft-ts';

@Component({
  selector: 'ss-file-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent implements OnInit {
  @Input() parentId: number;
  @Input() storageId: number;
  @Input() service: IUploadService;
  @Input() sliceSize: number = 1024000;
  @Input() maxAttempt: number = 5;

  files: File[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit() {
  }
  getFormattedSize = Formatter.size;
  getStateText(file: File): string {
    switch (file.state) {
      case FileState.Ready: return 'آماده بارگزاری';
      case FileState.InProgress: return 'در حال پردازش';
      case FileState.Uploading: return 'در حال بارگزاری';
      case FileState.Pause: return 'متوقف شده';
      case FileState.Cancel: return 'منصرف شده';
      case FileState.Complete: return 'تکمیل شده';
    }
    return 'نامشخص';
  }
  onFileChange(event) {
    let input = event.target;
    let files = input.files;
    if (files) {
      for (let i = 0, length = files.length; i < length; i++) {
        let file = files[i];
        let reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
          let tmp = new File;
          tmp.name = file.name;
          tmp.type = file.type;
          tmp.size = file.size;
          this.files.push(tmp);

          let start = 0;
          let end = 0;
          for (let i = 0; end < tmp.size; i++) {
            end = start + this.sliceSize;
            if (tmp.size - end < 0) end = tmp.size;

            let slice = new FileSlice;
            slice.index = i;
            slice.start = start;
            slice.end = end;
            slice.data = this.slice(file, start, end);
            let reader2 = new FileReader;
            reader2.addEventListener('loadend', (e: any) => slice.data = e.srcElement.result);
            reader2.readAsBinaryString(slice.data);

            tmp.slices.push(slice);

            start += this.sliceSize;
          }
        };
      }
    }
    input.value = '';
  }
  slice(file, start, end) {
    var slice = file.mozSlice ? file.mozSlice :
      file.webkitSlice ? file.webkitSlice :
        file.slice ? file.slice : () => { };

    return file.slice(start, end);
  }
  uploadAll() {
    for (let i = 0; i < this.files.length; i++) {
      if(this.files[i].state == FileState.Complete) continue;
      this.uploadFile(this.files[i]);
    }
  }
  uploadFile(file: File) {
    file.parentId = this.parentId;
    file.storageId = this.storageId;
    this.service.getToken(file).subscribe(op => {
      if (op.success) this.uploadSlices(file, op.data);
      file.message = op.message;
    })
  }
  uploadSlices(file: File, key: string, index: number = 0) {
    if (file.state == FileState.Complete) {
      this.service.complete(key).subscribe(op => {

      })
      return;
    }
    if (index >= file.slices.length) index = 0;
    let slice = file.slices[index];
    slice.attempt++;
    if (slice.attempt > this.maxAttempt) {
      slice.state = FileSliceState.Failed;
      this.uploadSlices(file, key, index + 1);
      return;
    }
    slice.requestTime = new Date;
    slice.state = FileSliceState.Uploading;
    this.service.upload(slice, key).subscribe(
      op => {
        if (op.success) slice.state = FileSliceState.Complete;
        slice.message = op.message;
        slice.state = FileSliceState.Complete;
      },
      e => {
        slice.state = FileSliceState.Failed;
      },
      () => {
        slice.responseTime = new Date;
        this.uploadSlices(file, key, index + 1);
      }
    );
  }

  calculate(segment: any) {
    segment.file.progress = Math.floor(segment.segments.filter(e => e.status == 'Complete').length * 100 / segment.segments.length);
  }

  cancel(file: any) { }
  cancelAll() {
    for (let i = 0, length = this.files.length; i < length; i++) this.files[i].state = FileState.Cancel;
  }

  remove(file: any) {
    this.cancel(file);
    for (let i = 0, length = this.files.length; i < length; i++) if (file.name == this.files[i].name) { this.files.splice(i, 1); break; }
  }
  removeAll() { this.cancelAll(); this.files = []; }
  removeComplete() { }

  uploadDisabled(): boolean { return this.files.filter(e => e.state == FileState.Ready || e.state == FileState.Pause).length == 0; }
  cancelDisabled(): boolean { return this.files.filter(e => e.state == FileState.Uploading).length == 0; }
  removeDisabled(): boolean { return this.files.length == 0; }
  removeCompleteDisabled(): boolean { return this.files.filter(e => e.state == FileState.Complete).length == 0; }
}
