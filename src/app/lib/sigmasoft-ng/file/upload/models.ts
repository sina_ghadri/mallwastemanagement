import { Formatter } from "../../../sigmasoft-ts";

export class File {
    name: string;
    type: string;
    size: number;
    slices: FileSlice[] = [];
    message: string;
    storageId: number;
    parentId: number;

    get state(): FileState {
        if (this.slices.length == 0) return FileState.Ready;
        let completeCount = 0;
        for (let i = 0; i < this.slices.length; i++) {
            let slice = this.slices[i];
            if (slice.state == FileSliceState.Uploading) return FileState.Uploading;
            if (slice.state == FileSliceState.Pause) return FileState.Pause;
            if (slice.state == FileSliceState.Cancel) return FileState.Cancel;
            if (slice.state == FileSliceState.Complete) completeCount++;
        }
        if (completeCount == this.slices.length) return FileState.Complete;
        return FileState.Ready;
    }
    set state(value: FileState) {
        if (value == FileState.Cancel) this.slices.forEach(x => x.state = FileSliceState.Cancel);
        if (value == FileState.Pause) this.slices.forEach(x => {
            if (x.state != FileSliceState.Complete) x.state = FileSliceState.Pause;
        });
    }

    get progress(): number {
        let count = 0;
        for (let i = 0; i < this.slices.length; i++) if (this.slices[i].state == FileSliceState.Complete) count++;
        return (count * 100 / this.slices.length);
    }
    
    get speed(): number {
        let time = new Date();
        let size = 0;
        let sec = 5;
        time.setSeconds(time.getSeconds() - sec);
        for (let i = 0; i < this.slices.length; i++) {
            let slice = this.slices[i];
            if (slice.state != FileSliceState.Complete) continue;
            if (slice.requestTime < time) continue;
            size += slice.end - slice.start;
        }
        return size / sec;
    }
    get time() {
        let speed = this.speed;
        if (!speed || this.state == FileState.Complete) return '__ __ __';
        let uploaded = 0;
        this.slices.forEach(x => { if (x.state == FileSliceState.Complete) uploaded += x.end - x.start; })
        let time = Math.round((this.size - uploaded) / speed);
        let tmp = '';
        if (time > 3600) tmp += Math.floor(time / 3600) + ' h ';
        if (time > 60) tmp += Math.floor((time % 3600) / 60) + ' m ';
        return tmp + Math.floor(time % 60) + ' s ' + Formatter.size(speed) + '/s';
      }
}

export enum FileState {
    Ready = 0,
    InProgress = 1,
    Uploading = 2,
    Pause = 3,
    Cancel = 4,
    Complete = 5,
}

export class FileSlice {
    index: number;
    state: FileSliceState;
    start: number;
    end: number;
    attempt: number = 0;
    data: any;
    message: string;
    requestTime: Date;
    responseTime: Date;
}

export enum FileSliceState {
    Ready = 0,
    Uploading = 1,
    Pause = 2,
    Failed = 3,
    Cancel = 4,
    Complete = 5,
}