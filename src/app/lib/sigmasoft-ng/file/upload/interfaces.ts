import { Observable } from "rxjs";
import { OperationResult } from "../../../sigmasoft-ts";
import { FileSlice, File } from "./models";

export interface IUploadService {
    getToken(file: File): Observable<OperationResult<string>>;
    upload(file: FileSlice, key: string): Observable<OperationResult<any>>;
    complete(key: string): Observable<OperationResult<any>>;
}