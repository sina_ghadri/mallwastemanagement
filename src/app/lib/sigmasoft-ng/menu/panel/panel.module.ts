import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MenuPanelComponent } from "./panel.component";

@NgModule({
  imports: [CommonModule],
  declarations: [MenuPanelComponent],
  exports: [MenuPanelComponent]
})
export class MenuPanelModule {}
