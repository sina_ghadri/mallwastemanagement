import { DataTableColumn } from "./column";

export class DataTableState {
    page: number;
    limit: number;
    sortBy: DataTableColumn;
    ascending: boolean;
    searchBy: DataTableColumn[];
}