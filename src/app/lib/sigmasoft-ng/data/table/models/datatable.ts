import { DataTableMenu } from './menu';
import { DataTableButton } from './button';
import { DataTableOptions } from './options';
import { DataTableColumn } from './column';
import { DataTableRow } from './row';
import { DataTableFilter } from './filter';
import { isArray, isObject } from "util";
import { EventEmitter } from '@angular/core';
import { DataTableCollapse } from './collapse';
import { DataTableState } from './state';
import { languages } from './language';

export class DataTable {
    private _page: number;
    private _sort: number;
    private _ascending: boolean;
    private _limit: number;
    private _message: string;

    private _columns: DataTableColumn[] = [];
    private _rows: DataTableRow[] = [];
    private _rowsFilter: DataTableRow[] = [];
    private _rowsResult: DataTableRow[] = [];
    private _stateChangeInterval: any;

    private _pushEvent = function(arr,callback) {
        arr.push = function(e) {
            Array.prototype.push.call(arr, e);
            callback(e);
        }
    }

    public name: string;
    public count: number;
    public limits: number[];
    public lazyloading: boolean = false;
    public menus: DataTableMenu[] = [];
    public buttons: DataTableButton[] = [];
    public collapse: DataTableCollapse = new DataTableCollapse; 
    public class: ((row: DataTableRow) => string) | string;
    public options: DataTableOptions = new DataTableOptions;
    
    public onrefresh: EventEmitter<any> = new EventEmitter;
    public onsave: EventEmitter<any> = new EventEmitter;
    public onreset: EventEmitter<any> = new EventEmitter;
    public onexport: EventEmitter<any> = new EventEmitter;
    public onstateChange: EventEmitter<DataTableState> = new EventEmitter;
    public onpageChange: EventEmitter<number> = new EventEmitter;
    public onlimitChange: EventEmitter<number> = new EventEmitter;
    public onsortChange: EventEmitter<number> = new EventEmitter;
    public onsortTypeChange: EventEmitter<boolean> = new EventEmitter;
    public onrowClick: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowDoubleClick: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowSelectedChange: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowSelect: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowDeselect: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowCollapseChange: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowCollapse: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowExpand: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowEditingChange: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowEditStart: EventEmitter<DataTableRow> = new EventEmitter;
    public onrowEditEnd: EventEmitter<DataTableRow> = new EventEmitter;

    public progress: boolean = false;

    private loading(primary: any, secondary: any) {
        let keys = Object.keys(primary);
        for(let i = 0;i < keys.length;i++) {
            let key = keys[i];
            let value1 = primary[key];
            let value2 = secondary[key];
            if(value2) {
                if(isObject(value1)) {
                    this.loading(value1,value2);
                } else primary[key] = value2;
            }
        }
    }
    constructor(options?: DataTableOptions) {
        if(options) this.loading(this.options, options);

        this._page = this.options.page;
        this._sort = this.options.sort;
        this._ascending = this.options.ascending;
        this._limit = this.options.limit;
        this.name = (location.pathname + location.hash).replace(/\//g, '.');
        this.limits = this.options.limits;
        
        this._pushEvent(this.columns, (column: DataTableColumn) => { 
            column.datatable = this;
            column.order = this.columns.length;            
        });
        this._pushEvent(this._rows, (row: DataTableRow) => {
            row.datatable = this;
            row.subscribeOnSelectedChange((x: boolean) => {
                this.onrowSelectedChange.emit(row);
                if(x) this.onrowSelect.emit(row);
                else this.onrowDeselect.emit(row);
            }); 
            row.subscribeOnEditingChange((x:boolean) => {
                this.onrowEditingChange.emit(row);
                if(x) this.onrowEditStart.emit(row);
                else this.onrowEditEnd.emit(row);
            });
            row.subscribeOnCollapseChange((x:boolean) => {
                this.onrowCollapseChange.emit(row);
                if(x) this.onrowCollapse.emit(row);
                else this.onrowExpand.emit(row);
            });
        });
    }

    get message(): string { return this._message; }
    get language(): any { return languages[this.options.language]; }
    get columns(): DataTableColumn[] { return this._columns; }
    get columnsSorted() { return this.columns.slice().sort((x1, x2) => (x1.order == x2.order ? 0 : (x1.order > x2.order ? 1 : -1))); }
    get page(): number { const count = this.pageCount; if (count && this._page > count) this._page = count; return this._page; }
    get limit(): number { if (this.limits.indexOf(this._limit) < 0) this._limit = this.limits[0]; return this._limit; }
    get sort(): number { return this._sort; }
    get ascending(): boolean { return this._ascending; }
    get pageCount(): number { return (this.rowsFilterCount ? Math.ceil(this.rowsFilterCount / this.limit) : 0); }
    get checkedAll(): boolean { let flag = true; for (let i = 0, length = this.rowsResult.length; i < length; i++) if (!this.rowsResult[i].selected) { flag = false; break; } return flag; }
    get rowIndexFrom(): number { return (this.page - 1) * this.limit + (this.rowsFilterCount > 0 ? 1 : 0); }
    get rowIndexTo(): number { let to = this.page * this.limit, count = this.rowsFilterCount; return (count < to ? count : to); }
    get rowsResultCount(): number { return this._rowsResult.length; }
    get rowsFilterCount(): number { return this.count ? this.count : this._rowsFilter.length; }
    get rowsTotalCount(): number { return (this.count ? this.count : this.rows.length); }
    get rows(): DataTableRow[] { return this._rows; }
    get rowsFilter(): DataTableRow[] { return this._rowsFilter; }
    get rowsSelected(): DataTableRow[] { return this.getRows(row => row.selected); }
    get rowsEdited(): DataTableRow[] { return this.getRows(row => row.editing); }
    get rowsCollapsed(): DataTableRow[] { return this.getRows(row => row.collapse); }
    get rowsResult(): DataTableRow[] {
        let rows = this.rows;
        if(!this.lazyloading)
        {
            rows = DataTableFilter.search(rows, this.columns);
            rows = DataTableFilter.sort(rows, this.columnsSorted, this.sort, this.ascending);
        }
        this._rowsFilter = rows;
        if(!this.lazyloading) {
            if (this.options.paging) rows = DataTableFilter.paging(rows, this.rowIndexFrom, this.limit);
        }
        return this._rowsResult = rows;
    }
    get hasFilter(): boolean {
        for (let i = 0, length = this.columns.length; i < length; i++) if (this.columns[i].search.value) return true;
        return false;
    }
    get hasSavedOptions(): boolean { return (localStorage.getItem(this.name) != null); }
    get columnCount(): number { return this.columns.filter(c => c.visible).length + this.menus.length + (this.options.rowOptions.selectable ? 1 : 0); }


    set checkedAll(value: boolean) { for (let i = 0, length = this.rowsResult.length; i < length; i++) this.rowsResult[i].selected = value; }
    set limit(value: number) { if (this.limits.indexOf(value) > -1) { let flag = (this._limit != value); this._limit = value; if(flag) this.onlimitChange.next(value); this.stateChange(); } }
    set sortType(value: boolean) { let flag = (this._ascending != value) ; this._ascending = value; if(flag) this.onsortTypeChange.next(value); this.stateChange(); }
    set sort(value: number) {
        if (value >= 0 && value < this.columns.length) {
            let flag = (this._sort != value);
            if (this.sort == value) this._ascending = !this._ascending;
            else { this._sort = value; this._ascending = true; }
            this.columns[this.sort].onsort.emit(this.ascending);
            if(flag) this.onsortChange.next(value);
            this.stateChange();
        }
    }
    set page(value: number) { if (value >= 1 && value <= this.pageCount) { let flag = (this._page != value); this._page = value; if(flag) this.onpageChange.next(value); this.stateChange(); } }
    set dataSource(value: any[]) { this.clear(); for (let i = 0; i < value.length; i++) this.addRow(value[i]); }
    set message(value: string) { this._message = value; setTimeout(() => this._message = '', this.options.messageTime); }

    private getRows(func: (row: DataTableRow) => boolean) {
        let array: any[] = [];
        let rows = this.rowsFilter;
        for (let i = 0, length = rows.length; i < length; i++) if (func(rows[i])) array.push(rows[i]);
        return array;
    }

    subscribeOnRefresh(value: () => void): DataTable { this.onrefresh.subscribe(value); return this; }
    subscribeOnSave(value: () => void): DataTable { this.onsave.subscribe(value); return this; }
    subscribeOnExport(value: () => void): DataTable { this.onexport.subscribe(value); return this; }
    subscribeOnStateChange(value: (x: any) => void): DataTable { this.onstateChange.subscribe(value); return this; }
    subscribeOnPageChange(value: (x: number) => void): DataTable { this.onpageChange.subscribe(value); return this; }
    subscribeOnLimitChange(value: (x: number) => void): DataTable { this.onlimitChange.subscribe(value); return this; }
    subscribeOnSortChange(value: (x: number) => void): DataTable { this.onsortChange.subscribe(value); return this; }
    subscribeOnSortTypeChange(value: (x: boolean) => void): DataTable { this.onsortTypeChange.subscribe(value); return this; }
    subscribeOnRowClick(value: (x: DataTableRow) => void): DataTable { this.onrowClick.subscribe(value); return this; }
    subscribeOnRowDoubleClick(value: (x: DataTableRow) => void): DataTable { this.onrowDoubleClick.subscribe(value); return this; }
    subscribeOnRowSelectedChange(value: (x: DataTableRow) => void): DataTable { this.onrowSelectedChange.subscribe(value); return this; }

    clear(): DataTable { while(this._rows.length) this._rows.pop(); return this; }
    firstPage(): DataTable { this.page = 1; return this; }
    prevPage(): DataTable { if (this.page > 1) this.page--; return this; }
    nextPage(): DataTable { if (this.page < this.pageCount) this.page++; return this; }
    lastPage(): DataTable { this.page = this.pageCount; return this; }
    
    setCollapse(value: DataTableCollapse): DataTable { this.collapse = value; return this; }
    setOrder(column: DataTableColumn, order: number) {
        if(order< 1) order = 1;
        if(order > this.columns.length - 1) order = this.columns.length;
        let replacement = this.columns.find(x => x.order == order);
        let temp = replacement.order;
        replacement.order = column.order;
        column.order = temp;
    }

    saveOptions(): DataTable {
        let columns: any[] = [];
        for (let i = 0, length = this.columns.length; i < length; i++) {
            let column = this.columns[i];
            columns.push({
                index: i,
                visible: column.visible,
                order: column.order,
                search: column.search.value
            });
        }
        let obj = {
            limit: this.limit,
            sort: this.sort,
            ascending: this.ascending,
            columns: columns
        };
        localStorage.setItem(this.name, JSON.stringify(obj));
        this.onsave.emit();
        return this;
    }
    loadOptions(): DataTable {
        let obj = JSON.parse(localStorage.getItem(this.name));
        if (obj) {
            if (this.limits.indexOf(+obj.limit) > -1) this.limit = +obj.limit;
            this.sort = obj.sort;
            this._ascending = obj.ascending;
            if (isArray(obj.columns)) {
                for (let i = 0, length = this.columns.length; i < length; i++) {
                    let column = obj.columns[i];
                    if (column) {
                        this.columns[i].visible = (column.visible ? true : false);
                        if (column.search) this.columns[i].search.value = column.search;
                    }
                }
            }
        }
        return this;
    }
    deleteOptions(): DataTable {
        localStorage.removeItem(this.name);
        this._limit = this.options.limit;
        this._sort = this.options.sort;
        this._ascending = this.options.ascending;
        this.clearFilters();
        this.onreset.emit();
        return this;
    }
    clearFilters(): DataTable {
        let flag = false;
        for (let i = 0, length = this.columns.length; i < length; i++) {
            if (this.columns[i].search && this.columns[i].search.value) {
                this.columns[i].search.value = undefined;
                flag = true;
            }
        }
        if (flag) this.stateChange();
        return this;
    }
    addColumn(column: DataTableColumn): DataTable { this.columns.push(column); return this; }
    addRow(value: any): DataTable {
        let row = new DataTableRow;
        row.datatable = this;
        row.value = value;
        this.rows.push(row);
        return this;
    }
    replaceRow(func: (row: DataTableRow) => boolean, value: any): DataTable {
        for (let i = 0, length = this._rows.length; i < length; i++) if (func(this._rows[i])) { this._rows[i].value = value; break; }
        return this;
    }
    removeRow(func: (row: DataTableRow) => boolean): DataTable {
        for (let i = 0, length = this._rows.length; i < length; i++) if (func(this._rows[i])) { this._rows.splice(i, 1); break; }
        return this;
    }
    stateChange() {
        if (this._stateChangeInterval) clearTimeout(this._stateChangeInterval);
        this._stateChangeInterval = setTimeout(() => {
            let searchBy: DataTableColumn[] = [];
            for (var i = 0; i < this.columns.length; i++) if (this.columns[i].search && this.columns[i].search.value) searchBy.push(this.columns[i]);
            this.onstateChange.emit({
                page: this.page,
                limit: this.limit,
                sortBy: this.columnsSorted[this.sort],
                ascending: this.ascending,
                searchBy: searchBy
            });
        }, 100)
    }
}