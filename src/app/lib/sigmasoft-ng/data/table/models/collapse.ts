import { TemplateRef } from "@angular/core";

export class DataTableCollapse {
    template: TemplateRef<any>;

    constructor(template?: TemplateRef<any>) {
        this.template = template;
    }

    public setTemplate(value: TemplateRef<any>): DataTableCollapse { this.template = value;  return this; }
}