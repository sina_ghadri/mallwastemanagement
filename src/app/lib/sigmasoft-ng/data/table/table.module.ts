import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { DataTableComponent } from "./components/datatable.component";
import { HotkeyModule } from "angular2-hotkeys";
import { OverlayModalModule } from "../../overlay";
import {NgxPrintModule} from 'ngx-print';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    RouterModule,
    OverlayModalModule,
    NgxPrintModule,
    HotkeyModule.forRoot()
  ],
  declarations: [DataTableComponent],
  exports: [DataTableComponent, RouterModule]
})
export class DataTableModule { }
