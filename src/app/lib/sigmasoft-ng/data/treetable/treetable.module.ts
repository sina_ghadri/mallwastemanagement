import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { TreetableComponent } from "./treetable.component";

@NgModule({
  imports: [CommonModule],
  declarations: [TreetableComponent],
  exports: [TreetableComponent]
})
export class DataTreetableModule {}
