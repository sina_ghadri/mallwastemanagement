export class TreeViewItem {
  public id: number = null;
  public text: string;
  public isOpen: boolean;
  public src: string;
  public isSelected: boolean;
  public iconClosed: string;
  public iconOpened: string;
  public items: TreeViewItem[];
  public parent: TreeViewItem;
  public parentId: number;
  public value: any;
  public set icon(value: string) { this.iconClosed = this.iconOpened = value; }

  public constructor(item: TreeViewItem = null) {
    this.id = (item ? item.id : null);
    this.src = (item ? item.src : null);
    this.text = (item ? item.text : null);
    this.items = (item ? item.items : []);
    this.parent = (item ? item.parent : null);
    this.parentId = (item ? item.parentId : null);
    this.value = (item ? item : null);
  }
  public isFile() { return this.items.length == 0; }
  public close() { this.isOpen = false; }
  public open() {
    this.isOpen = true;
    if (this.parent) this.parent.open();
  }
}
