import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { TreeViewItem } from "../models";

@Component({
    selector: 'ss-data-treeview',
    templateUrl: './treeview.component.html',
    styleUrls: ['./treeview.component.scss']
})
export class TreeviewComponent implements OnInit {
  @Input() items: TreeViewItem[];
  @Input() parent: TreeViewItem;
  @Input() multiple: boolean;
  @Input() filter: (item: TreeViewItem) => boolean;

  @Output() onselect: EventEmitter<TreeViewItem> = new EventEmitter;


  onchange: (item: TreeViewItem | TreeViewItem[]) => any = item => { };
  ontouch: (item: TreeViewItem | TreeViewItem[]) => any = item => { };
  isdisabled: boolean;

  ngOnInit(): void {
    //this.items = this.optimize(this.items,this.parent);
  }

  optimize(items: TreeViewItem[], parent: TreeViewItem = null): TreeViewItem[] {
    let result: TreeViewItem[] = []
    if (items != null) {
      for (let i = 0; i < items.length; i++) {
        let item = new TreeViewItem(items[i]);
        item.parent = parent;
        if (this.parent == null) item.items = this.optimize(item.items, item);
        result.push(item);
      }
    }
    return result;
  }
  filtering(items: TreeViewItem[]): TreeViewItem[] {
    let result: TreeViewItem[] = [];
    for (let i = 0, length = items.length; i < length; i++) {
      let res = this.filter(items[i]);
      if (res == null || res == undefined || res == true) {
        result.push(items[i]);
        if (res == true) items[i].open();
      }
    }
    return result;
  }

  public toggle(item: TreeViewItem) { item.isOpen = (item.isOpen ? false : true); }
  public getItems(): TreeViewItem[] {
    if (!this.items) return [];
    if (this.filter) return this.filtering(this.items);
    return this.items;
  }
  public select(item: TreeViewItem) {
    if (!this.parent) this.deselectAllNodes()
    item.isSelected = true;
    if (this.onselect) this.onselect.emit(item);
  }
  public deselectAllNodes(parent?: TreeViewItem) {
    let items: TreeViewItem[];
    if (!parent) items = this.items;
    else items = parent.items;
    if (items) {
      for (let i = 0; i < items.length; i++) {
        const item = items[i];
        item.isSelected = false;
        this.deselectAllNodes(item);
      }
    }
  }
}
