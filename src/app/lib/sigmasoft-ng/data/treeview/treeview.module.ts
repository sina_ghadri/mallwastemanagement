import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";
import { TreeviewComponent } from "./components/treeview.component";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [TreeviewComponent],
  exports: [TreeviewComponent]
})
export class DataTreeviewModule {}
