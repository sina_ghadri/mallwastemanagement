import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'ss-progress-bar',
  templateUrl: './bar.component.html',
  styleUrls: ['./bar.component.css']
})
export class BarComponent implements OnInit {
  @Input() barWidth: any=0;
  @Input() color: string='#37d292';
  @Input() text: string='';

  setIntervalBar;
  constructor() { }

  ngOnInit() {
    // this.setIntervalBar = setInterval(this.frame(), 10);
  }
  frame() {
    if (this.barWidth >= 100) {
      clearInterval( this.setIntervalBar );
    } else {
      this.barWidth++;
    }
  }
}
