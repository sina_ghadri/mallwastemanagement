import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

import { BaseButtonModule } from './button/button.module';
import { BaseChartsModule } from './charts/charts.module';
import { BaseDataModule } from './data/data.module';
import { BaseDragDropModule } from './drag-drop/drag-drop.module';
import { BaseFileModule } from './file/file.module';
import { BaseInputModule } from './input/input.module';
import { BaseMenuModule } from './menu/menu.module';
import { BaseMessageModule } from './message/message.module';
import { BaseMiscModule } from './misc/misc.module';
import { BaseMultimediaModule } from './multimedia/multimedia.module';
import { BaseOverlayModule } from './overlay/overlay.module';
import { BasePageModule } from "./page";
import { BasePanelModule } from './panel/panel.module';
import { BaseProgressModule } from './progress/progress.module';
import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { HttpService } from "./misc/jwt/http.service";
import {TruefalsePipe} from '../../pipes/truefalse.pipe';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
    ],
    declarations: [TruefalsePipe],
    exports: [
        TruefalsePipe,
        BaseButtonModule,
        BaseChartsModule,
        BaseDataModule,
        BaseDragDropModule,
        BaseFileModule,
        BaseInputModule,
        BaseMenuModule,
        BaseMessageModule,
        BaseMiscModule,
        BaseMultimediaModule,
        BaseOverlayModule,
        BasePanelModule,
        BaseProgressModule,
        BasePageModule,

    ],
    providers: [{ provide: HTTP_INTERCEPTORS, useClass: HttpService, multi: true }]
})
export class SigmaSoftModule { }
