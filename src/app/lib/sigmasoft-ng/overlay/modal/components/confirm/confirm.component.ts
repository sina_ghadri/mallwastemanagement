import { Component, OnInit, OnChanges, SimpleChanges, Renderer2, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Modal } from '../../models';

@Component({
    selector: 'ss-overlay-modal-confirm',
    templateUrl: './confirm.component.html',
    styleUrls: ['./confirm.component.css']
})
export class ModalConfirmComponent implements OnInit,OnChanges {
    @Input('modal-title') title: string = 'Title';
    @Input() modal: Modal;
    @Input() value: any;
    @Input() progress: boolean = false;

    @Output() onconfirm: EventEmitter<any> = new EventEmitter;
    @Output() onclose: EventEmitter<any> = new EventEmitter;

    constructor(private element: ElementRef) { }
    ngOnInit(): void {
        if(this.modal) {
            this.modal.onchange = isopen => {
                if(isopen) {
                    let element = this.element.nativeElement.querySelector('#cancel');
                    if(element) element.focus();
                }
            }
        }
    }
    ngOnChanges(changes: SimpleChanges): void {
        
    }
    focus(btn:any) { if(this.modal.isopen) btn.focus(); }
    confirm() {
        this.onconfirm.emit(this.value);
    }
    cancel() {
        this.modal.close();
    }
}
