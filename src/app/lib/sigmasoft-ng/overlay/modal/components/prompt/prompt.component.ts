import { Component, OnInit, OnChanges, SimpleChanges, Renderer2, Input, Output, EventEmitter, ElementRef } from '@angular/core';
import { Prompt } from '../../models';

@Component({
    selector: 'ss-overlay-modal-prompt',
    templateUrl: './prompt.component.html',
    styleUrls: ['./prompt.component.css']
})
export class ModalPromptComponent implements OnInit,OnChanges {
    @Input('modal-title') title: string = 'Title';
    @Input() modal: Prompt;
    @Input() progress: boolean;
    @Input() type: string;

    @Output() onconfirm: EventEmitter<any> = new EventEmitter;
    @Output() onclose: EventEmitter<any> = new EventEmitter;

    constructor(private element: ElementRef) { }
    ngOnInit(): void {
        if(this.modal) {
            this.modal.onchange = isopen => {
                if(isopen) {
                    let element = this.element.nativeElement.querySelector('#input');
                    if(element) element.focus();
                }
            }
        }
    }
    ngOnChanges(changes: SimpleChanges): void {
        
    }
    focus(btn:any) { if(this.modal.isopen) btn.focus(); }
    confirm() {
        this.onconfirm.emit(this.modal.value);
        this.modal.value = '';
    }
    cancel() {
        this.modal.value = '';
        this.modal.close();
    }
}