import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Modal } from '../../models';

@Component({
    selector: 'ss-overlay-modal-alert',
    templateUrl: './alert.component.html',
    styleUrls: ['./alert.component.css']
})
export class ModalAlertComponent {
    @Input() modal: Modal = new Modal;
    @Input() type: string = 'info';

    @Output() onclose: EventEmitter<any> = new EventEmitter;

    close() {
        this.close();
        this.onclose.emit();
    }
}