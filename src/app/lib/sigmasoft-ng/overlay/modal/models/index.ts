export class Modal {
    private show: boolean;
    private content: boolean;

    public progress: boolean;
    public message: string;
    public onchange: (isopen:boolean) => any;

    open() { this.content = this.show = true; if(this.onchange) this.onchange(true); }
    close() { this.show = false; if(this.onchange) this.onchange(false); setTimeout(() => { if(!this.show) this.content = false; },300) }

    get isopen(): boolean { return this.show; }
    get iscontentShow(): boolean { return this.content; }
}
export class Prompt extends Modal {
    public value: any;
}