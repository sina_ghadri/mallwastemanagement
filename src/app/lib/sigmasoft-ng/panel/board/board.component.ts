import { Component, OnInit, TemplateRef, QueryList, ContentChildren, Input, ViewChild, Output, EventEmitter, ContentChild, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'ss-panel-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.scss']
})
export class BoardComponent implements OnInit, OnChanges {
  @Input() positions: number[];
  @Input() layout: number = 0;
  @ViewChild('cardTemplate') cardTemplate: TemplateRef<any>;
  @ViewChild('addTemplate') addTemplate: TemplateRef<any>;
  @ContentChild('add') add: TemplateRef<any>;
  @ContentChildren('card') templates: QueryList<TemplateRef<any>>;

  @Output() onadd: EventEmitter<any> = new EventEmitter;
  @Output() onpositionChange: EventEmitter<any> = new EventEmitter;

  initTemplates: TemplateRef<any>[];
  dragOverTemplate: TemplateRef<any>;
  dragTemplate: TemplateRef<any[]>;
  constructor() { }

  ngOnChanges(changes: SimpleChanges) {
    console.log(changes);
    this.arrange();
  }
  ngOnInit() {
    
  }
  ngAfterViewInit() {
    if(!this.initTemplates) {
      this.initTemplates = [];
      this.templates.forEach(x => this.initTemplates.push(x));
    }
    this.templates.changes.subscribe(x => {
    })
    this.arrange();
  }
  arrange() {
    if(this.templates && this.positions && Array.isArray(this.positions)) {
      let templates = [];
      for(let i = 0;i < this.positions.length;i++) {
        templates.push(this.initTemplates[this.positions[i]]);
      }
      this.templates.reset(templates);
    }
  }
  get(index: number) {
    return this.templates.find((x, i) => i == index);
  }
  getTemplateClass(template: TemplateRef<any>): string {
    if(template == this.dragOverTemplate) return 'droping';
    return '';
  }
  getResponsiveClass() {
    return 'col-sm-' + (this.templates.length > 4 ? 3 : 12 / this.templates.length);
  }
  getAfter(index: number): TemplateRef<any>[] {
    return this.templates.filter((x, i) => i > index);
  }
  allowDrop(e,template: TemplateRef<any>) {
    e.preventDefault();
    this.dragOverTemplate = template;
  }
  onDragStart(e, template) {
    this.dragTemplate = template;
  }
  onDragEnd(e, template) {
    this.dragOverTemplate = null;
    this.dragTemplate = null;
  }
  onDrop(e, template) {
    e.preventDefault();

    let templates = this.templates.toArray();
    let sindex = templates.indexOf(templates.find(x => x == this.dragTemplate));
    let dindex = templates.indexOf(templates.find(x => x == template));
    
    let temp = templates[sindex];
    templates[sindex] = templates[dindex];
    templates[dindex] = temp;

    if(sindex != dindex) {
      let changes: number[] = [];
      for(let i = 0;i < this.initTemplates.length;i++) changes.push(templates.indexOf(templates.find(x => x == this.initTemplates[i])));
      this.onpositionChange.emit(changes);
    }
    
    this.templates.reset(templates);
    this.dragOverTemplate = null;
    this.dragTemplate = null;
  }
}
