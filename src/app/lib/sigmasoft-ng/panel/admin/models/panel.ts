import { DateTime } from "../../../../sigmasoft-ts";
import { EventEmitter } from "@angular/core";
import { Module } from "./module";
import { User } from "./user";
import { Menu } from "./menu";

export class Panel {
    datetime: DateTime = DateTime.now;
    module: Module;
    menus: Menu[];
    modules: Module[];
    user: User;

    onlogout: EventEmitter<any> = new EventEmitter;
    onmoduleNotFound: EventEmitter<any> = new EventEmitter;
    
    constructor() {
    }
}