import { Token } from "./token";
import { Module } from "./module";

export class LoginInfo {
    modules: Module[];
    token: Token;
}