import { Menu } from "./menu";
import { KeyValue } from "../../../../sigmasoft-ts";

export class Module {
    id: number;
    name: string;
    link: string;
    symbol: string;
    icon: string;
    roles: string[];
    permissions: string[];
    claims: KeyValue[];
    status: KeyValue;
    optimize: boolean;

    setName(value: string): Module { this.name = value; return this; }
    setLink(value: string): Module { this.link = value; return this; }
    setSymbol(value: string): Module { this.symbol = value; return this; }
    setIcon(value: string): Module { this.icon = value; return this; }
}