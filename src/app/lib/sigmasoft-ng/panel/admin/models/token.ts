import { User } from '.'

export class Token {
    key: string;
    user: User;
}