import { Component, Output, EventEmitter, OnInit, AfterViewInit, Input } from "@angular/core";
import { Router, NavigationStart, NavigationEnd, NavigationCancel, NavigationError } from "@angular/router";
import {Menu, Module, Panel, User} from '../models';
import { Convert } from "../../../../sigmasoft-ts";

@Component({
  selector: "ss-panel-admin",
  templateUrl: "./panel.component.html",
  styleUrls: ["./panel.component.scss"],
})
export class PanelAdminComponent implements OnInit, AfterViewInit {
  @Input() panel: Panel;

  toggle: boolean = false;
  progress: boolean = false;
  modulesShow: boolean = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
    setInterval(() => { if (this.panel.datetime) this.panel.datetime.addSeconds(1); }, 1000);

    this.panel.menus = Convert.toObject(this.panel.menus, Menu);
    for (let i = 0; i < this.panel.menus.length; i++) {
      this.panel.menus[i].children = Convert.toObject(this.panel.menus[i].children, Menu);
    }
  }
  routerLink(menu){

  }
  IsOpenMenu(){

    for(const item of this.sortMenu(this.panel.menus))
    {
      if(item.isopen==true)
      {

        return true;

      }
    }

  }
  ngAfterViewInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.progress = true;
      } else if (event instanceof NavigationEnd || event instanceof NavigationCancel || event instanceof NavigationError) {
        this.progress = false;
      }
    }, e => this.progress = false, () => this.progress = false);
  }
  sortMenu(menus: Menu[]): Menu[] {
    if(!menus) return;
    return menus.sort((x1,x2) => (x1.order > x2.order ? 1 : (x1.order < x2.order ? -1 : 0)));
  }
  chooseModule() {
    if (this.panel.modules.length > 1)
      this.modulesShow = !this.modulesShow;
  }
  changeModule(obj: Module) {
    this.modulesShow = false;
    location.href = obj.link;
  }
  closeAllMenu() {
    for (let i = 0; i < this.panel.menus.length; i++) this.panel.menus[i].close();
  }
  toggleMenu(menu: Menu) {
    if (menu) {
      let isopen = menu.isopen;
      this.closeAllMenu();
      if (!isopen) menu.open();
      else menu.close();
    }
  }
  logout() {
    this.panel.onlogout.emit();
  }
}
