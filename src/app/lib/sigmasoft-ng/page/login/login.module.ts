import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import {FormsModule} from '@angular/forms';
import {PasswordToggleModule} from '../../password-toggle/PasswordToggle.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    PasswordToggleModule
  ],
  exports:[LoginComponent],
  declarations: [LoginComponent]
})
export class LoginModule { }
