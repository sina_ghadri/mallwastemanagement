import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges, } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {LoginInfo, PageLogin, Token, User} from '../../../../panel/admin/models';
import {OperationResult} from '../../../../../sigmasoft-ts/models';
import {UserService} from '../../../../../../services/user.service';


@Component({
  selector: 'ss-panel-admin-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  exportAs: "ss-panel-admin-login",
})
export class LoginComponent implements OnInit, OnChanges {
  @Input() loginInfo: PageLogin;
  // @Input() service: IAdminPanelService;
  @Input('login-block') loginBlock: boolean = true;

  @Output() onsuccess: EventEmitter<any> = new EventEmitter();
  @Output() onfailed: EventEmitter<any> = new EventEmitter();
  @Output() oncaptcha: EventEmitter<any> = new EventEmitter();
  @Output() onforget: EventEmitter<any> = new EventEmitter();

  animation: string = 'fadeInDown animated';
  islock: boolean;

  public constructor(
    private router: Router,
    private route: ActivatedRoute,
    private service: UserService,
    // private managerService: AdminPanelMangerService
  ) { }

  ngOnInit(): void {

    //
    // this.loginInfo.token = new Token;
    // this.loginInfo.key = '123';
    this.loginInfo.user= new User;
    this.loginInfo.user.name="مدیر سیستم";


    // this.login.token.user = new User();
    // let username = localStorage.getItem('username');
    // if (username) {
    //   this.login.token.user.name = username;
    //   this.islock = true;
    // }
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.loginBlock && !changes.loginBlock.firstChange && changes.loginBlock) this.animation = 'fadeIn animated';
    setTimeout(() => this.animation = '', 1200)
  }
  submit() {
    this.loginInfo.progress = true;
    // this.service.login(this.loginInfo.user).subscribe(
    //   op => {
    //
    //     if (op.access_token) this.loginSuccess(op);
    //     else this.loginFailed(op);
    //   },
    //   () => {
    //     this.loginInfo.progress = false;
    //     this.loginInfo.message = "خطایی در ارسال اطلاعات رخ داده است";
    //   },
    //   () => (this.loginInfo.progress = false)
    // );
    setTimeout(() => {
      if(this.loginInfo.user.username=="admin" &&  this.loginInfo.user.password=="123456")
      {
        this.onsuccess.emit();
      }else {
        this.loginInfo.message = "نام کاربری یا گذرواژه اشتباه است";
        let username = document.getElementById("username");
        if (username) username.focus();
        this.loginInfo.user.username = "";
        this.loginInfo.user.password = "";
        this.loginInfo.progress = false;
      }

    },1000);


  }
  loginSuccess(op) {

    if (this.onsuccess.observers.length > 0) this.onsuccess.emit(op);
    else {

      localStorage.setItem("token", op.access_token);
      localStorage.setItem("data", op);
      // this.router.navigate([this.managerService.module.link], { relativeTo: this.route });
    }
    localStorage.setItem('username', this.loginInfo.user.username);
    this.loginInfo.user = new User();
  }
  loginFailed(op) {
    if (this.onfailed.observers.length > 0) this.onfailed.emit(op);
    else {
      this.loginInfo.message = op.message && op.message.length > 0 ? op.message : "نام کاربری یا گذرواژه اشتباه است";
      let username = document.getElementById("username");
      if (username) username.focus();
      this.loginInfo.user.username = "";
      this.loginInfo.user.password = "";
    }
  }
  anotherAccount() {
    this.islock = false;
    this.loginInfo.user.username = '';
    localStorage.removeItem('username');
  }
}
