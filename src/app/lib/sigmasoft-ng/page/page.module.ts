import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Error404Module } from "./error-404/error-404.module";
import {LoginModule} from './login/login.module';

@NgModule({
  imports: [CommonModule],
  exports: [
    Error404Module,
    LoginModule
  ],
  providers: []
})
export class BasePageModule {}


// Todo Add LoginModule
