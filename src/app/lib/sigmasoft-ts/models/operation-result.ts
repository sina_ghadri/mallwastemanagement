export class OperationResult<T> {
    code: number;
    Code: number;
    success: boolean = true;
    Success: boolean = true;
    message: string;
    Message: string;
    data: T;
    Data: T;
    Count:number;
}
