export class Cache {
    private _values: any = {};
    private _timers: any = {};
    timeout: number = 1;

    constructor() {
        
    }
    get(key: any, callback: (key: any) => any): any {
        if (this._values[key]) {
            if (!this._timers[key]) {
                this._timers[key] = setTimeout(() => {
                    this._values[key] = null;
                    this._timers[key] = null;
                }, this.timeout);
            }
            return this._values[key];
        }
        return this._values[key] = callback(key);
    }
}